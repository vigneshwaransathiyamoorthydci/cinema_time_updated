package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.ExclusiveResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Gowshikan on 7/25/2018.
 */

public class ExclusiveAdapter extends RecyclerView.Adapter<ExclusiveAdapter.ViewHolder>
{
    private List<ExclusiveResponse.Results.Datum> exclu;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_label_exclusive_film_name,text_label_exclusive_category,
                text_label_exclusive_certificate;
        public ImageView image_exclusive;

        public ViewHolder(View view) {
            super(view);
//            text_label_exclusive_film_name=(TextView)view.findViewById(R.id.text_label_exclusive_film_name);
//            text_label_exclusive_category=(TextView)view.findViewById(R.id.text_label_exclusive_category);
//            text_label_exclusive_certificate=(TextView)view.findViewById(R.id. text_label_exclusive_certificate);
//            image_exclusive= view.findViewById(R.id.image_exclusive);




        }
    }
    public ExclusiveAdapter(List<ExclusiveResponse.Results.Datum>exclu, Context context){
        this.exclu=exclu;
        this.context=context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_childlayout_exclusive_fragment,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ExclusiveResponse.Results.Datum exx =exclu.get(position);
        holder.text_label_exclusive_film_name.setText(exx.getMovie_original_title());
        holder.text_label_exclusive_category.setText(exx.getMovie_language());
        holder.text_label_exclusive_certificate.setText(String.valueOf(exx.getMovie_certificate()));

//        Picasso.with(context).load(context.getString(R.string.coming_soon_image_base_url)
//                +"movies_backdrop_YI2m_1532443567.jpg").placeholder(R.mipmap.placeholder_icon)
//                .into(holder.image_exclusive);



    }

    @Override
    public int getItemCount() {
        return exclu.size();
    }


}
