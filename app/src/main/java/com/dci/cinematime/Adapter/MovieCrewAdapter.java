package com.dci.cinematime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.CaseDetailsActivity;
import com.dci.cinematime.model.CrewItem;
import com.dci.cinematime.model.MovieDetail;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by harini on 7/25/2018.
 */

public class MovieCrewAdapter extends RecyclerView.Adapter<MovieCrewAdapter.MyViewHolder> {
    ArrayList<CrewItem> moviedetailsCrew;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,role;
        public ImageView profile;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            role = (TextView) view.findViewById(R.id.role);
            profile = (ImageView) view.findViewById(R.id.profile);

        }
    }

    public MovieCrewAdapter(Context context, ArrayList<CrewItem> moviedetailsCrew) {
        this.moviedetailsCrew=moviedetailsCrew;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crew_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
       /* MovieDetail movie = circularsModels.get(position);
        holder.name.setText(movie.getCast().get(position).getCrewLname());
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + movie.getCast().get(position).getCrewImage()).placeholder(R.mipmap.cinema1).
                into(holder.profile);*/
       holder.name.setText(moviedetailsCrew.get(position).getName());
       holder.role.setText(moviedetailsCrew.get(position).getRole());
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + moviedetailsCrew.get(position).getImage()).placeholder(R.mipmap.icon_pro_image_loading_256).
                into(holder.profile);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CaseDetailsActivity.class);
                i.putExtra("castid", moviedetailsCrew.get(position).getId());
                i.putExtra("castname",moviedetailsCrew.get(position).getName());
                context.startActivity(i);
            }
        });
//        if (moviedetailsCrew.get(position).getImage().toString().length()>0) {
//            Picasso.with(context).
//                    load(context.getString(R.string.coming_soon_image_base_url) + moviedetailsCrew.get(position).getImage()
//                            + moviedetailsCrew.get(position).getImage()).placeholder(R.mipmap.icon_pro_image_loading_256).
//                    into(holder.profile);
//            holder.profile.setScaleType(ImageView.ScaleType.FIT_XY);
//        }
//        else
//        {
//            holder.profile.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            holder.profile.setImageResource(R.mipmap.icon_pro_image_loading_256);
//        }

    }

    @Override
    public int getItemCount() {
      //  return circularsModels.size();
        return moviedetailsCrew.size();
    }
}

