package com.dci.cinematime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.BaseActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.LoginActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.LikeanddislikeResponse;
import com.dci.cinematime.model.MovieDetailsWithThearteDetails;
import com.dci.cinematime.model.NowShowingResponse;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.ExclusiveDetailsResponse;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Results;
import com.dci.cinematime.utils.Theatre;
import com.dci.cinematime.utils.Util;
import com.dci.cinematime.utils.times;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gowshikan on 7/9/2018.
 */

public class ExclusiveShowingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Results> exclusiveResult;
    private Context context;
    AddTouchListen addTouchListen;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    BaseActivity baseActivity;
    LikeanddislikeResponse likeanddislikeResponse;
    HomeActivity homeActivity;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private AdView mAdView;
    AdRequest.Builder adRequestBuilder ;
    HashMap exclusivetheatreListHashmap;
    HashMap exclusivetheatreTimeListHashmap;
    Double latitude;
    Double longitude;
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView filmname, category, status, percentage, views;
        public ImageView image_now_showing,like_Imageview;



        public ViewHolder(View view) {
            super(view);
            filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
            like_Imageview=view.findViewById(R.id.image_heart);

        }
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public ExclusiveShowingAdapter(List<Results> exclusiveResult, Context context,HashMap exclusivetheatreListHashmap,HashMap exclusivetheatreTimeListHashmap,Double latitude,Double longitude) {
        this.exclusiveResult = exclusiveResult;
        this.context = context;
        this.exclusivetheatreListHashmap=exclusivetheatreListHashmap;
        this.exclusivetheatreTimeListHashmap=exclusivetheatreTimeListHashmap;
        this.latitude=latitude;
        this.longitude=longitude;



    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_childlayout_exclusive_fragment, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bannerad, parent, false);
            return new AdViewHolder(itemView);
        } else return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdViewHolder) {
            AdViewHolder footerHolder = (AdViewHolder) holder;
           //footerHolder.bannerAd.loadAd(adRequestBuilder.build());

        }
        else if (holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
//            if(position % 4 != 2) {
                final Results now = exclusiveResult.get(position);
                setAnimation(itemViewHolder.itemView, position);
                itemViewHolder.filmname.setText(now.getName());
                itemViewHolder.category.setText(now.getMovie_language());
            Picasso.with(context).
                    load(context.getString(R.string.coming_soon_image_base_url)
                            + now.getMovie_backgroundimg()).placeholder(R.drawable.default_movie).
                    into(itemViewHolder.image_now_showing);
                //itemViewHolder.status.setText(now.getMovie_certificate().getMovie_certificate());
            ArrayList<Theatre> exclusivetheatreList = (ArrayList<Theatre>) exclusivetheatreListHashmap.get(now.getId());
                //itemViewHolder.percentage.setText(exclusivetheatreList.getTheatrename());

                itemViewHolder.text_theatrename.setText(now.getTheatre().get(0).getTheatrename());
                itemViewHolder.text_theatrecity.setText(now.getTheatre().get(0).getCitylist().getCityname()+","+now.getTheatre().get(0).getCitylist().getCityname()+","+now.getTheatre().get(0).getState().getStatename());

            if (latitude > 0 && longitude >0) {
                if (now.getTheatre().get(0).getLatitude().equals("0") && now.getTheatre().get(0).getLongitude().equals("0"))
                {
                    itemViewHolder.text_theatredistance.setText(" ");
                    //holder.img.setVisibility(View.INVISIBLE);
                }
                else {
//                    itemViewHolder.text_theatredistance.setText(" " + (int) distance(Double.parseDouble(now.getTheatre().get(0).getLatitude()),
//                            Double.parseDouble( now.getTheatre().get(0).getLongitude()), latitude, longitude)+" "+" KM");
                    //holder.img.setVisibility(View.VISIBLE);
                    Location loc1 = new Location("");
                    loc1.setLatitude(latitude);
                    loc1.setLongitude(longitude);

                    Location loc2 = new Location("");
                    loc2.setLatitude(Double.parseDouble( now.getTheatre().get(0).getLatitude()));
                    loc2.setLongitude(Double.parseDouble( now.getTheatre().get(0).getLongitude()));

                    float distanceInMeters = loc1.distanceTo(loc2)/1000;
                    itemViewHolder.text_theatredistance.setText(""+distanceInMeters+" "+"KM");

                }
            }


            itemViewHolder.image_now_showing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(context, TheatreDetailsActivity.class);
                    i.putExtra("theatreid", now.getTheatre().get(0).getId());
                    i.putExtra("theatrename", now.getTheatre().get(0).getTheatrename());
                    context.startActivity(i);



                }
            });


        }


    }





    @Override
    public int getItemCount() {
        return exclusiveResult.size();
    }
    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
//        if (position % 4 == 2)
//         {
//            return TYPE_FOOTER;
//        } else {
            return TYPE_ITEM;
        //}
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        AdView bannerAd;

        public AdViewHolder(View view) {
            super(view);
            bannerAd = (AdView) view.findViewById(R.id.adView);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView filmname, category, status, percentage, views,text_theatrename,text_theatrecity,text_theatredistance;
        public ImageView image_now_showing,like_Imageview;

        public ItemViewHolder(View view) {
            super(view);

            filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
            like_Imageview=view.findViewById(R.id.image_heart);
            text_theatrename= (TextView) view.findViewById(R.id.text_theatrename);
            text_theatrecity=(TextView)view.findViewById(R.id.text_theatrecity);
            text_theatredistance=(TextView)view.findViewById(R.id.text_theatredistance);

        }
    }


    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = Math.round(dist * 100);
        dist = dist / 100;
        return dist;
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
