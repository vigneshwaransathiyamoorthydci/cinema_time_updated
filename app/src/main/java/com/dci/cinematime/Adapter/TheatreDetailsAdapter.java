package com.dci.cinematime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.dci.cinematime.model.TheatreBasedMovieList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Gowshikan on 7/13/2018.
 */

public class TheatreDetailsAdapter extends RecyclerView.Adapter<TheatreDetailsAdapter.ViewHolder>
{

    ArrayList<TheatreBasedMovieList> theatreBasedMovieList;
    private Context context;
    MovieTiminginTheaterListAdpaterTheatreList movieTiminginTheaterListAdpater;
    ArrayList<ShowTimeMovieTiminginTheater> moviedetailsCategory;
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_label_theatre_details_film_name,text_label_theatre_details_category;
        public GridView image_theatre_gridview;
        public ImageView image_theatre;

        public ViewHolder(View view) {
            super(view);
            text_label_theatre_details_film_name=(TextView)view.findViewById(R.id.text_label_theatre_details_film_name);
            text_label_theatre_details_category=(TextView)view.findViewById(R.id.text_label_theatre_details_category);
            image_theatre_gridview=(GridView)view.findViewById(R.id.girdviewtheater_movie_timing);
            image_theatre=(ImageView)view.findViewById(R.id.image_theatre_details_child);

        }
    }
    public TheatreDetailsAdapter(ArrayList<TheatreBasedMovieList> theatreBasedMovieList, Context context){
        this.theatreBasedMovieList=theatreBasedMovieList;
        this.context=context;
        moviedetailsCategory=new ArrayList<ShowTimeMovieTiminginTheater>();


    }


    @NonNull
    @Override
    public TheatreDetailsAdapter.ViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_childlayout_theatre_details,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TheatreDetailsAdapter.ViewHolder holder, final int position) {
        setAnimation(holder.itemView, position);
        holder.text_label_theatre_details_film_name.setText(theatreBasedMovieList.get(position).getName());
        holder.text_label_theatre_details_category.setText(theatreBasedMovieList.get(position).getMovie_language());
        ArrayList<ShowTimeMovieTiminginTheater> showTimeMovieTiminginTheater=new ArrayList<ShowTimeMovieTiminginTheater>();

        for (int u=0;u<theatreBasedMovieList.get(position).getShowTimeMovieTiminginTheater().size();u++)
        {
            showTimeMovieTiminginTheater= (ArrayList<ShowTimeMovieTiminginTheater>) theatreBasedMovieList.get(position).getShowTimeMovieTiminginTheater().get(position);
        }


        ViewGroup.LayoutParams layoutParams =  holder.image_theatre_gridview.getLayoutParams();
        //this is in pixels

        if (showTimeMovieTiminginTheater.size()>=9)
        {
            layoutParams.height = 450;
            holder.image_theatre_gridview.setLayoutParams(layoutParams);
        }
       else if (showTimeMovieTiminginTheater.size()>6 && showTimeMovieTiminginTheater.size()<9)
        {
            layoutParams.height = 350;
            holder.image_theatre_gridview.setLayoutParams(layoutParams);
        }

        else
        {
            layoutParams.height = 250;
            holder.image_theatre_gridview.setLayoutParams(layoutParams);
        }
        movieTiminginTheaterListAdpater=new MovieTiminginTheaterListAdpaterTheatreList(showTimeMovieTiminginTheater,context);
        holder.image_theatre_gridview.setAdapter(movieTiminginTheaterListAdpater);
//        holder.image_theatre_gridview.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//
//        });
        //movieTiminginTheaterListAdpater.notifyDataSetChanged();
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)
                        + theatreBasedMovieList.get(position).getMovie_posterimage()).placeholder(R.drawable.default_movie).
                into(holder.image_theatre);
//        holder.image_theatre_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent i = new Intent(context, MovieDetailActivity.class);
//                i.putExtra("theatreid", theatreBasedMovieList.get(position).getId());
//                i.putExtra("theatrename",theatreBasedMovieList.get(position).getName());
//                context.startActivity(i);
//            }
//        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, MovieDetailActivity.class);
                i.putExtra("id",theatreBasedMovieList.get(position).getId());
                i.putExtra("showtime",true);
                context.startActivity(i);
            }
        });







        }

    @Override
    public int getItemCount() {
        return theatreBasedMovieList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }
}
