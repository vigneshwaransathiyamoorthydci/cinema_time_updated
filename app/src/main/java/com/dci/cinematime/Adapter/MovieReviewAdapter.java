package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.fragment.ReviewFragment;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailReviewItem;

import org.joda.time.DateTimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;



public class MovieReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<MovieDetailReviewItem> movieDetailReviewList;
    Context context;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    ReviewFragment reviewFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, description, time;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.reviewer_name);
            description = (TextView) view.findViewById(R.id.description);
            time = (TextView) view.findViewById(R.id.time);

        }
    }

    public MovieReviewAdapter(Context context, List<MovieDetailReviewItem> movieDetailReviewList, ReviewFragment reviewFragment) {
        this.movieDetailReviewList = movieDetailReviewList;
        this.context = context;
        this.reviewFragment=reviewFragment;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_list_item, parent, false);
            return new RecyclerViewAdapter.ItemViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerviewfooter, parent, false);
            return new RecyclerViewAdapter.FooterViewHolder(itemView);
        } else return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof RecyclerViewAdapter.FooterViewHolder) {
            RecyclerViewAdapter.FooterViewHolder footerHolder = (RecyclerViewAdapter.FooterViewHolder) holder;
            if (movieDetailReviewList.size()>=14) {

                    footerHolder.footerText.setVisibility(View.VISIBLE);

            }
            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "You clicked at Footer View", Toast.LENGTH_SHORT).show();
                    reviewFragment.getMovieReview();

                }
            });
        } else if (holder instanceof RecyclerViewAdapter.ItemViewHolder) {
            RecyclerViewAdapter.ItemViewHolder itemViewHolder = (RecyclerViewAdapter.ItemViewHolder) holder;
            itemViewHolder.name.setText(movieDetailReviewList.get(position).getCrew_lname());
            itemViewHolder.description.setText(movieDetailReviewList.get(position).getReviews());






            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");

            try {

                Date oldDate = dateFormat.parse(movieDetailReviewList.get(position).getCreated_at());
                System.out.println(oldDate);

                Date currentDate = new Date();

                long diff = currentDate.getTime() - oldDate.getTime();
                long seconds = diff / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;

                if (oldDate.before(currentDate)) {

//                Log.e("oldDate", "is previous date");
//                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
                    if (seconds<=100)
                    {
                        itemViewHolder.time.setText("Just now");
                    }
                    else if (minutes<=10)
                    {
                        itemViewHolder.time.setText("Minute ago");
                    }
                    else if (hours<=24)
                    {
                        switch ((int) hours) {
                            case 0:
                                itemViewHolder.time.setText("1 Hour ago");
                                break;
                            case 1:
                                itemViewHolder.time.setText("2 Hour ago");
                                break;
                            case 2:
                                itemViewHolder.time.setText("3 Hour ago");
                                break;
                            case 3:
                                break;
                            case 4:
                                itemViewHolder.time.setText("4 Hour ago");
                                break;
                            case 5:
                                itemViewHolder.time.setText("5 Hour ago");
                                break;
                            case 6:
                                itemViewHolder.time.setText("6 Hour ago");
                                break;
                            case 7:
                                itemViewHolder.time.setText("7 Hour ago");
                                break;
                            case 8:
                                itemViewHolder.time.setText("8 Hour ago");
                                break;
                            case 9:
                                itemViewHolder.time.setText("9 Hour ago");
                                break;
                            case 10:
                                itemViewHolder.time.setText("10 Hour ago");
                                break;
                            case 11:
                                itemViewHolder.time.setText("11 Hour ago");
                                break;
                            case 12:
                                itemViewHolder.time.setText("12 Hour ago");
                                break;
                            case 13:
                                itemViewHolder.time.setText("13 Hour ago");
                                break;
                            case 14:
                                itemViewHolder.time.setText("14 Hour ago");
                                break;
                            case 15:
                                itemViewHolder.time.setText("15 Hour ago");
                                break;
                            case 16:
                                itemViewHolder.time.setText("16 Hour ago");
                                break;
                            case 17:
                                itemViewHolder.time.setText("17 Hour ago");
                                break;
                            case 18:
                                itemViewHolder.time.setText("18 Hour ago");
                                break;
                            case 19:
                                itemViewHolder.time.setText("19 Hour ago");
                                break;
                            case 20:
                                itemViewHolder.time.setText("20 Hour ago");
                                break;
                            case 21:
                                itemViewHolder.time.setText("21 Hour ago");
                                break;
                            case 22:
                                itemViewHolder.time.setText("22 Hour ago");
                                break;
                            case 23:
                                itemViewHolder.time.setText("23 Hour ago");
                                break;
                            case 24:
                                itemViewHolder.time.setText("24 Hour ago");
                                break;

                        }
                    }
                    else if(days<=2)
                    {
                        switch ((int) days)
                        {
                            case 0:
                                itemViewHolder.time.setText("1 Day ago");
                                break;
                            case 1:
                                itemViewHolder.time.setText("1 Day ago");
                                break;
                            case 2:
                                itemViewHolder.time.setText("2 Day ago");
                                break;
                        }

                    }
                    else
                    {
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        Date date1 = dateFormat1.parse(movieDetailReviewList.get(position).getCreated_at());
                        itemViewHolder.time.setText(dateFormat.format(date1));
                    }
                }

                // Log.e("toyBornTime", "" + toyBornTime);

            } catch (ParseException e) {

                e.printStackTrace();
            }


        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == movieDetailReviewList.size()) {

            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }
    @Override
    public int getItemCount() {
        //  return circularsModels.size();
        return movieDetailReviewList.size()+1;
    }


    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView name, description,time;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.reviewer_name);
            description = (TextView) itemView.findViewById(R.id.description);
            time = (TextView) itemView.findViewById(R.id.time);
        }
    }

}

