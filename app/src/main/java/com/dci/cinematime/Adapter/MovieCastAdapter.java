package com.dci.cinematime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.CaseDetailsActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.model.CastItem;
import com.dci.cinematime.model.MovieDetail;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by harini on 7/25/2018.
 */

public class MovieCastAdapter extends RecyclerView.Adapter<MovieCastAdapter.MyViewHolder> {
    ArrayList<CastItem> moviedetailsCast;
    Context context;
     String castBaseUrl;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,role;
        public ImageView profile;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.text_label_email_name);
            profile = (ImageView) view.findViewById(R.id.profile);
            role = (TextView) view.findViewById(R.id.role);

        }
    }

    public MovieCastAdapter(Context context ,ArrayList<CastItem> moviedetailsCast,String castBaseUrl) {
        this.moviedetailsCast =moviedetailsCast ;
        this.context = context;
        castBaseUrl=castBaseUrl;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cast_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (moviedetailsCast.get(position).getUrl().toString().length()>0) {
            holder.profile.setScaleType(ImageView.ScaleType.FIT_XY);
            Picasso.with(context).
                    load(context.getString(R.string.coming_soon_image_base_url) + moviedetailsCast.get(position).getUrl()
                            + moviedetailsCast.get(position).getCrew_image()).placeholder(R.mipmap.icon_pro_image_loading_256).
                    into(holder.profile);

        }
        else
        {
            holder.profile.setScaleType(ImageView.ScaleType.CENTER);
            holder.profile.setImageResource(R.mipmap.icon_pro_image_loading_256);
        }
        holder.name.setText(moviedetailsCast.get(position).getCrew_fname());
        holder.role.setText(moviedetailsCast.get(position).getCrew_role());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CaseDetailsActivity.class);
                i.putExtra("castid", moviedetailsCast.get(position).getId());
                i.putExtra("castname",moviedetailsCast.get(position).getCrew_fname());
                context.startActivity(i);
            }
        });




    }

    @Override
    public int getItemCount() {
       // return circularsModels.size();
        return moviedetailsCast.size();
    }
}

