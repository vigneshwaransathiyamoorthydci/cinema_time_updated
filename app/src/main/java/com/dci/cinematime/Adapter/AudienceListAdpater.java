package com.dci.cinematime.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.AudienceTypeResultItem;

import java.util.ArrayList;


public class AudienceListAdpater extends BaseAdapter   {
    public AudienceListAdpater(ArrayList<AudienceTypeResultItem> audienceTypeResultItems, Context context) {
        this.audienceTypeResultItems = audienceTypeResultItems;
        this.context = context;



    }

    ArrayList<AudienceTypeResultItem> audienceTypeResultItems;
    Context context;

    @Override
    public int getCount() {
        return audienceTypeResultItems.size();
    }

    @Override
    public AudienceTypeResultItem getItem(int position) {
        return audienceTypeResultItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.custom_spinner_item, null);
        }


        TextView textLibTitle = (TextView) convertView.findViewById(R.id.text_spinner_item_one);
        textLibTitle.setText(audienceTypeResultItems.get(position).getAudience());








        return convertView;
    }



}
