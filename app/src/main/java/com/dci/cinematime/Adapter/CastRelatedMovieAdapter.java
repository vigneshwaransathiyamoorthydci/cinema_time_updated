package com.dci.cinematime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.CaseDetailsActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.model.CastItem;
import com.dci.cinematime.model.CastRelatedMovieDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by harini on 7/25/2018.
 */

public class CastRelatedMovieAdapter extends RecyclerView.Adapter<CastRelatedMovieAdapter.MyViewHolder> {
    ArrayList<CastRelatedMovieDetails> moviedetailsCast;
    Context context;
     String castBaseUrl;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,role;
        public ImageView profile;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.text_label_email_name);
            profile = (ImageView) view.findViewById(R.id.profile);
            role = (TextView) view.findViewById(R.id.role);

        }
    }

    public CastRelatedMovieAdapter(Context context , ArrayList<CastRelatedMovieDetails> moviedetailsCast, String castBaseUrl) {
        this.moviedetailsCast =moviedetailsCast ;
        this.context = context;
        castBaseUrl=castBaseUrl;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cast_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (moviedetailsCast.get(position).getMovie_posterimage().toString().length()>0) {
            holder.profile.setScaleType(ImageView.ScaleType.FIT_XY);
            Picasso.with(context).
                    load(context.getString(R.string.coming_soon_image_base_url)+ moviedetailsCast.get(position).getMovie_posterimage()).placeholder(R.mipmap.icon_pro_image_loading_256).
                    into(holder.profile);



        }
        else
        {
            holder.profile.setScaleType(ImageView.ScaleType.CENTER);
            holder.profile.setImageResource(R.mipmap.icon_pro_image_loading_256);
        }


        holder.name.setText(moviedetailsCast.get(position).getMovie_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, MovieDetailActivity.class);
                i.putExtra("id", moviedetailsCast.get(position).getId());
                i.putExtra("releaseType", moviedetailsCast.get(position).getReleaseType());
                i.putExtra("showtime",true);
                context.startActivity(i);
            }
        });
        //holder.role.setText(moviedetailsCast.get(position).getCrew_role());





    }

    @Override
    public int getItemCount() {
       // return circularsModels.size();
        return moviedetailsCast.size();
    }
}

