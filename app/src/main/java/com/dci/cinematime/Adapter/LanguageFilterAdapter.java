package com.dci.cinematime.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.dci.cinematime.R;
import com.dci.cinematime.model.HomeFilterPopupResponse;

import java.util.List;

/**
 * Created by Gowshikan on 7/26/2018.
 */

public class LanguageFilterAdapter extends RecyclerView.Adapter<LanguageFilterAdapter.ViewHolder> {
    public List<HomeFilterPopupResponse> filter;
    public Context context;
    int i = 0;


   public int lastcheckposition = -1;
    AddTouchListen addTouchListen;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text_label_english_now_showing_filter;
        public ImageView image_now_showing_filter_tick_four;

        public ViewHolder(View view) {
            super(view);
            text_label_english_now_showing_filter = (TextView) view.findViewById(R.id.text_label_english_now_showing_filter);
            image_now_showing_filter_tick_four = view.findViewById(R.id.image_now_showing_filter_tick_four);
           /* text_label_english_now_showing_filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (i == 0) {
                        image_now_showing_filter_tick_four.setVisibility(View.VISIBLE);

                       i = 1;

                    } else if (i == 1) {
                        image_now_showing_filter_tick_four.setVisibility(View.GONE);
                       i = 0;
                    }
                }
            })*/;


        }
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);

    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;




    }

    public LanguageFilterAdapter(List<HomeFilterPopupResponse> filter, Context context) {

        this.filter = filter;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_childlayout_home_filter_popup, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        HomeFilterPopupResponse homefilter = filter.get(position);
        holder.text_label_english_now_showing_filter.setText(homefilter.getText_label_english_now_showing_filter());
        holder.image_now_showing_filter_tick_four.setVisibility(View.GONE);
        if (position==lastcheckposition){
            holder.image_now_showing_filter_tick_four.setVisibility(View.VISIBLE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               lastcheckposition = position;
              notifyDataSetChanged();

                addTouchListen.onTouchClick(position);

              //  listener.OnItemClick(position,view);

                /*if (i == 0) {
                    holder.image_now_showing_filter_tick_four.setVisibility(View.VISIBLE);


                    //position.setVisibility(View.VISIBLE);

                    i = 1;


                } else if (i == 1) {
                    holder.image_now_showing_filter_tick_four.setVisibility(View.GONE);
                   // position.setVisibility(View.GONE);
                    i = 0;
                }*/



            }
        });

      /* holder.text_label_english_now_showing_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.image_now_showing_filter_tick_four.setVisibility(View.VISIBLE);

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return filter.size();
    }


}
