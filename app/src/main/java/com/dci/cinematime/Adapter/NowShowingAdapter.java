package com.dci.cinematime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.BaseActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.LoginActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.LikeanddislikeResponse;
import com.dci.cinematime.model.NowShowingResponse;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Gowshikan on 7/9/2018.
 */

public class NowShowingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<NowShowingResponse.Result> showing;
    private Context context;
    AddTouchListen addTouchListen;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    BaseActivity baseActivity;
    LikeanddislikeResponse likeanddislikeResponse;
    HomeActivity homeActivity;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private AdView mAdView;
    AdRequest.Builder adRequestBuilder ;
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView filmname, category, status, percentage, views;
        public ImageView image_now_showing,like_Imageview;



        public ViewHolder(View view) {
            super(view);
            filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
            like_Imageview=view.findViewById(R.id.image_heart);

        }
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public NowShowingAdapter(List<NowShowingResponse.Result> showing, Context context, BaseActivity baseActivity, HomeActivity homeActivity) {
        this.showing = showing;
        this.context = context;
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        this.baseActivity=baseActivity;
        this.homeActivity=homeActivity;
//        MobileAds.initialize(context,
//                "ca-app-pub-3940256099942544~3347511713");
//        mAdView = new AdView(context);
//        mAdView.setAdSize(AdSize.LARGE_BANNER);
//        adRequestBuilder = new AdRequest.Builder();
//        mAdView.setAdUnitId(context.getString(R.string.banner_ad_unit_id));
        // Optionally populate the ad request builder.
        //adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.


        // Start loading the ad.


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_childlayout_now_showing_fragment, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bannerad, parent, false);
            return new AdViewHolder(itemView);
        } else return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdViewHolder) {
            AdViewHolder footerHolder = (AdViewHolder) holder;
           //footerHolder.bannerAd.loadAd(adRequestBuilder.build());

        }
        else if (holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
//            if(position % 4 != 2) {
                final NowShowingResponse.Result now = showing.get(position);
                setAnimation(itemViewHolder.itemView, position);
                itemViewHolder.filmname.setText(now.getName());
                itemViewHolder.category.setText(now.getMovie_language());
                itemViewHolder.status.setText(now.getMovie_certificate().getMovie_certificate());
                itemViewHolder.percentage.setText(now.getMovie_average() + " %");
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
                DecimalFormat df = (DecimalFormat) nf;
                df.applyPattern("#,###");
                int totalViews = now.getMovie_votes();
                if (LanguageHelper.getLanguage(context).toString().equals("en")) {
                    itemViewHolder.views.setText(df.format(totalViews) + " " + "views");
                } else {
                    itemViewHolder.views.setText("views" + " " + df.format(totalViews));
                }
                Picasso.with(context).
                        load(context.getString(R.string.coming_soon_image_base_url)
                                + now.getMovie_backgroundimg()).placeholder(R.drawable.default_movie).
                        into(itemViewHolder.image_now_showing);
                itemViewHolder.image_now_showing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addTouchListen.onTouchClick(position);
                    }
                });
                if (now.getMovie_liked_by_user().getLiked() == 1) {
                    itemViewHolder.like_Imageview.setImageResource(R.drawable.heart);
                } else {
                    itemViewHolder.like_Imageview.setImageResource(R.mipmap.white_heart);
                }
                itemViewHolder.like_Imageview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (sharedPreferences.getInt(Constants.Userid, 0) != 0) {
                            if (Util.isNetworkAvailable()) {
                                //addTouchListen.onNotifyTouch(position);
                                int likecount = 0;
                                if (now.getMovie_liked_by_user().getLiked() == 0) {
                                    likecount = 1;
                                } else {
                                    likecount = 0;
                                }
                                baseActivity.showProgress();
                                cinemaTimeAPI.setMovielikeandDislike(now.getId(), sharedPreferences.getInt(Constants.Userid, 0), likecount).
                                        enqueue(new Callback<LikeanddislikeResponse>() {
                                            @Override
                                            public void onResponse(Call<LikeanddislikeResponse> call, Response<LikeanddislikeResponse> response) {
                                                baseActivity.hideProgress();

                                                if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                                                    likeanddislikeResponse = response.body();
                                                    if (likeanddislikeResponse.getResults().getLikeCount().equals("1")) {
                                                        itemViewHolder.like_Imageview.setImageResource(R.drawable.heart);
                                                        now.getMovie_liked_by_user().setLiked(1);
                                                    } else {
                                                        itemViewHolder.like_Imageview.setImageResource(R.mipmap.white_heart);
                                                        now.getMovie_liked_by_user().setLiked(0);
                                                    }
                                                } else {
                                                    Toast.makeText(context, likeanddislikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<LikeanddislikeResponse> call, Throwable t) {
                                                baseActivity.hideProgress();
                                                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            } else {
                                Toast.makeText(context, "No Internet", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "Login to continue", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, LoginActivity.class);
                            context.startActivity(intent);
                            homeActivity.finish();

                        }
                    }
                });
           // }
        }

    }





    @Override
    public int getItemCount() {
        return showing.size();
    }
    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
//        if (position % 4 == 2)
//         {
//            return TYPE_FOOTER;
//        } else {
            return TYPE_ITEM;
        //}
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        AdView bannerAd;

        public AdViewHolder(View view) {
            super(view);
            bannerAd = (AdView) view.findViewById(R.id.adView);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView filmname, category, status, percentage, views;
        public ImageView image_now_showing,like_Imageview;

        public ItemViewHolder(View view) {
            super(view);

            filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
            like_Imageview=view.findViewById(R.id.image_heart);
        }
    }


}
