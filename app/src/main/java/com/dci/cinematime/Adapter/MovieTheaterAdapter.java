package com.dci.cinematime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.util.LogTime;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailsTheatresList;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.dci.cinematime.activity.MovieDetailActivity.moviedetailsCategory;

/**
 * Created by harini on 7/26/2018.
 */

public class MovieTheaterAdapter  extends RecyclerView.Adapter<MovieTheaterAdapter.MyViewHolder> {
    List<MovieDetailsTheatresList> movieDetailTheatresList;
    Context context;
    MovieTiminginTheaterListAdpaterfromMovieDetails movieTiminginTheaterListAdpater;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, address;
        ImageView profile;
        GridView theater_movie_timing;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.theater_name);
            address = (TextView) view.findViewById(R.id.address);
            profile = (ImageView) view.findViewById(R.id.profile);
            theater_movie_timing=(GridView)view.findViewById(R.id.theater_movie_timing);

        }
    }

    public MovieTheaterAdapter(Context context,   List<MovieDetailsTheatresList> movieDetailTheatresList) {
        this.movieDetailTheatresList = movieDetailTheatresList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_theater_list_item
                        , parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.name.setText(movieDetailTheatresList.get(position).getName());
        holder.address.setText(movieDetailTheatresList.get(position).getCity());
        ArrayList<ShowTimeMovieTiminginTheater> showTimeMovieTiminginTheater=new ArrayList<ShowTimeMovieTiminginTheater>();

        for (int u=0;u<movieDetailTheatresList.get(position).getShowTimeMovieTiminginTheater().size();u++)
        {
            showTimeMovieTiminginTheater= (ArrayList<ShowTimeMovieTiminginTheater>) movieDetailTheatresList.get(position).getShowTimeMovieTiminginTheater().get(position);
        }

        ViewGroup.LayoutParams layoutParams =  holder.theater_movie_timing.getLayoutParams();
        //this is in pixels
        if (showTimeMovieTiminginTheater.size()>=9)
        {
            layoutParams.height = 450;
            holder.theater_movie_timing.setLayoutParams(layoutParams);
        }
        else if (showTimeMovieTiminginTheater.size()>6 && showTimeMovieTiminginTheater.size()<9)
        {
            layoutParams.height = 350;
            holder.theater_movie_timing.setLayoutParams(layoutParams);
        }

        else
        {
            layoutParams.height = 250;
            holder.theater_movie_timing.setLayoutParams(layoutParams);
        }
        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)+ movieDetailTheatresList.get(position).getProfileImg()).placeholder(R.mipmap.cinema_default).
                into(holder.profile);
        movieTiminginTheaterListAdpater=new MovieTiminginTheaterListAdpaterfromMovieDetails(showTimeMovieTiminginTheater,context);
        holder.theater_movie_timing.setAdapter(movieTiminginTheaterListAdpater);
//        holder.theater_movie_timing.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//
//        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, TheatreDetailsActivity.class);
                i.putExtra("theatreid", movieDetailTheatresList.get(position).getId());
                i.putExtra("theatrename",movieDetailTheatresList.get(position).getName());
                context.startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {
        //  return circularsModels.size();
        return movieDetailTheatresList.size();
    }
}


