package com.dci.cinematime.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class MovieTiminginTheaterListAdpaterfromMovieDetails extends BaseAdapter   {
    public MovieTiminginTheaterListAdpaterfromMovieDetails(ArrayList<ShowTimeMovieTiminginTheater> moviedetailsCategory, Context context) {
        this.moviedetailsCategory = moviedetailsCategory;
        this.context = context;



    }

    ArrayList<ShowTimeMovieTiminginTheater> moviedetailsCategory;
    Context context;

    @Override
    public int getCount() {
        return moviedetailsCategory.size();
    }

    @Override
    public ShowTimeMovieTiminginTheater getItem(int position) {
        return moviedetailsCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_showtiminglist, null);
        }


        TextView textLibTitle = (TextView) convertView.findViewById(R.id.categoryname);
        TextView textLibType = (TextView) convertView.findViewById(R.id.categorytype);
        LinearLayout relativeLayout = (LinearLayout) convertView.findViewById(R.id.cons_lib_item);

        textLibTitle.setText(moviedetailsCategory.get(position).getTime());

            if (moviedetailsCategory.get(position).getAudienceType().equals("Male")) {
                relativeLayout.setBackgroundResource(R.drawable.shapeborder_male);
                textLibType.setText(moviedetailsCategory.get(position).getAudienceType());
            } else if (moviedetailsCategory.get(position).getAudienceType().equals("Family")) {
                relativeLayout.setBackgroundResource(R.drawable.shapeborder_family);
                textLibType.setText(moviedetailsCategory.get(position).getAudienceType());
            } else {
                relativeLayout.setBackgroundResource(R.drawable.shapeborder_male);
                textLibType.setText(moviedetailsCategory.get(position).getAudienceType());
            }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, TheatreDetailsActivity.class);
                i.putExtra("theatreid", moviedetailsCategory.get(position).getId());
                i.putExtra("theatrename",moviedetailsCategory.get(position).getTitle());
                context.startActivity(i);
            }
        });






        return convertView;
    }



}
