package com.dci.cinematime.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.CategoryItem;
import com.dci.cinematime.model.MoviesItem;

import java.util.ArrayList;


public class CategoryListAdpater extends BaseAdapter   {
    public CategoryListAdpater(ArrayList<CategoryItem> moviedetailsCategory, Context context) {
        this.moviedetailsCategory = moviedetailsCategory;
        this.context = context;



    }

    ArrayList<CategoryItem> moviedetailsCategory;
    Context context;

    @Override
    public int getCount() {
        return moviedetailsCategory.size();
    }

    @Override
    public CategoryItem getItem(int position) {
        return moviedetailsCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_categorylist, null);
        }


        TextView textLibTitle = (TextView) convertView.findViewById(R.id.categoryname);
        if (moviedetailsCategory.get(position).getCategoryName().toString().length()>=10)
        {
            textLibTitle.setText(moviedetailsCategory.get(position).getCategoryName());
            textLibTitle.setTextSize(10);
        }
        else
        {
            textLibTitle.setText(moviedetailsCategory.get(position).getCategoryName());
            textLibTitle.setTextSize(10);
        }








        return convertView;
    }



}
