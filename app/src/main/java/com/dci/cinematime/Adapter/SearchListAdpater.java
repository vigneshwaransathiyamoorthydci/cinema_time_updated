package com.dci.cinematime.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.MoviesItem;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;



public class SearchListAdpater extends BaseAdapter   {
    public SearchListAdpater(ArrayList<MoviesItem> libraryListResultsItems, Context context) {
        this.libraryListResultsItems = libraryListResultsItems;
        this.context = context;



    }

    ArrayList<MoviesItem> libraryListResultsItems;
    Context context;

    @Override
    public int getCount() {
        return libraryListResultsItems.size();
    }

    @Override
    public MoviesItem getItem(int position) {
        return libraryListResultsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_searchlist, null);
        }

        TextView imageLibrary = (TextView) convertView.findViewById(R.id.image_lib_pic);
        TextView textLibTitle = (TextView) convertView.findViewById(R.id.text_lib_title);
        TextView textLibCast= (TextView) convertView.findViewById(R.id.text_lib_cast);
        textLibTitle.setText(libraryListResultsItems.get(position).getName());
        if (libraryListResultsItems.get(position).getCategory()==1)
        {
            textLibCast.setVisibility(View.VISIBLE);
            textLibCast.setText(libraryListResultsItems.get(position).getCrew_name());
            imageLibrary.setCompoundDrawablePadding(Gravity.START);
            Drawable img = context.getResources().getDrawable( R.drawable.movies_icon);
            img.setBounds(0, 0, img.getIntrinsicWidth(), img.getIntrinsicHeight());
            imageLibrary.setCompoundDrawablesRelative(img,null,null,null);

        }
        else
        {
            //textLibCast.setVisibility(View.GONE);
            textLibCast.setVisibility(View.VISIBLE);
            textLibCast.setText(libraryListResultsItems.get(position).getCity()+","+libraryListResultsItems.get(position).getState()+","+libraryListResultsItems.get(position).getCountry());

            Drawable img = context.getResources().getDrawable( R.drawable.theatre_icon);
            img.setBounds(0, 0, img.getIntrinsicWidth(), img.getIntrinsicHeight());
            imageLibrary.setCompoundDrawablesRelative(img,null,null,null);
            imageLibrary.setCompoundDrawablePadding(Gravity.START);
        }








        return convertView;
    }



}
