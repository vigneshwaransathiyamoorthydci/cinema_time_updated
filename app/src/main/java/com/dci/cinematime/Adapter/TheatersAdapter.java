package com.dci.cinematime.Adapter;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.model.TheatersList;
import com.dci.cinematime.utils.GPSTracker;
import com.dci.cinematime.utils.Util;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;

/**
 * Created by harini on 7/27/2018.
 */

public class TheatersAdapter extends RecyclerView.Adapter<TheatersAdapter.ViewHolder> {

    private List<TheatersList.ResultsItem> theaterlist;
    private Context context;
    AddTouchListen addTouchListen;
    double latitude;
    double longitude;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView theatername, address, status, percentage, views;
        public ImageView image_now_showing,img;
        LinearLayout layout;


        public ViewHolder(View view) {
            super(view);
            theatername = (TextView) view.findViewById(R.id.theater_name);
            address = (TextView) view.findViewById(R.id.theater_area);
            status = (TextView) view.findViewById(R.id.km);
           // percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
           // views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            layout = view.findViewById(R.id.layout);
            img=(ImageView)view.findViewById(R.id.img);
            image_now_showing=(ImageView)view.findViewById(R.id.image_theatre_details_child);

        }
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public TheatersAdapter(List<TheatersList.ResultsItem> showing, Context context,Double latitude,Double longitude) {
        this.theaterlist = showing;
        this.context = context;
        this.latitude=latitude;
        this.longitude=longitude;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.theater_item_view, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TheatersList.ResultsItem now = theaterlist.get(position);
        setAnimation(holder.itemView, position);
        holder.theatername.setText(now.getTheatrename());
        holder.address.setText(now.getCitylist().getCityname() + ", " + now.getstatee().getStatename() + ", " + now.getCountry().getCountryname());
        if (latitude > 0 && longitude >0) {
            if (now.getLatitude().equals("0") && now.getLongitude().equals("0"))
            {
                holder.status.setText(" ");
                //holder.img.setVisibility(View.INVISIBLE);
            }
            else {
                //holder.status.setText(" " + (int) distance(Double.parseDouble(now.getLatitude()), Double.parseDouble(now.getLongitude()), latitude, longitude)+" "+" KM");
                Location loc1 = new Location("");
                loc1.setLatitude(latitude);
                loc1.setLongitude(longitude);

                Location loc2 = new Location("");
                loc2.setLatitude(Double.parseDouble( now.getLatitude()));
                loc2.setLongitude(Double.parseDouble( now.getLongitude()));

                float distanceInMeters = loc1.distanceTo(loc2)/1000;
                holder.status.setText(""+distanceInMeters+" "+"KM");


            }
        }

        Picasso.with(context).
                load(context.getString(R.string.coming_soon_image_base_url)+ Util.THEATREIMAGEFOLDER+ now.getPicture()).placeholder(R.mipmap.cinema_default).
                into(holder.image_now_showing);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTouchListen.onTouchClick(position);
            }
        });
        //Log.d("VIKIs","KM"+distance(Double.parseDouble(now.getLatitude()),Double.parseDouble(now.getLongitude()),9.8845,78.0524));
    }

    @Override
    public int getItemCount() {
        return theaterlist.size();
    }
    private int lastPosition = -1;
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.ABSOLUTE, 0.5f, Animation.ABSOLUTE, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }

    private float getDistance(double lat1, double lon1, double lat2, double lon2) {
        float[] distance = new float[2];
        Location.distanceBetween(lat1, lon1, lat2, lon2, distance);
        return distance[0];
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = Math.round(dist * 100);
        dist = dist / 100;
        return dist;
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }



}

