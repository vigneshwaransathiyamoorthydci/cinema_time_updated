package com.dci.cinematime.dagger;


import android.content.Context;
import android.content.SharedPreferences;


import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.retrofit.CinemaTimeAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


//provides instance or objects for a class
@Module
public class AppModule {

    public static final String CINI_PREFS = "dmk";

    private final CinemaTimeApplication ciniApp;

    public AppModule(CinemaTimeApplication app) {
        this.ciniApp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return ciniApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return ciniApp.getSharedPreferences(CINI_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public CinemaTimeAPI provideDMKApiInterface(Retrofit retrofit) {
        return retrofit.create(CinemaTimeAPI.class);
    }


}
