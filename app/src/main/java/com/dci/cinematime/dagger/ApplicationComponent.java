package com.dci.cinematime.dagger;


import android.content.SharedPreferences;


import com.dci.cinematime.Adapter.ComingSoonAdapter;
import com.dci.cinematime.Adapter.NowShowingAdapter;
import com.dci.cinematime.activity.AudienceTypeFilterActivity;
import com.dci.cinematime.activity.CaseDetailsActivity;
import com.dci.cinematime.activity.ForgotActivity;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.activity.LoginActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.activity.SignupActivity;
import com.dci.cinematime.activity.SplashActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.fragment.AboutFragment;
import com.dci.cinematime.fragment.ComingSoonFragment;
import com.dci.cinematime.fragment.DescriptionFragment;
import com.dci.cinematime.fragment.ExclusiveFragment;
import com.dci.cinematime.fragment.HelpandFeedbackFragment;
import com.dci.cinematime.fragment.LanguageFragment;
import com.dci.cinematime.fragment.NotificationFragment;
import com.dci.cinematime.fragment.NowShowingFragment;
import com.dci.cinematime.fragment.PrivacyPolicyFragment;
import com.dci.cinematime.fragment.ReviewFragment;
import com.dci.cinematime.fragment.SearchFragment;
import com.dci.cinematime.fragment.SettingFragment;
import com.dci.cinematime.fragment.ShowtimeFragment;
import com.dci.cinematime.fragment.TheatresFragment;
import com.dci.cinematime.fragment.UserProfileFragment;
import com.dci.cinematime.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;


@Singleton
// modules for perform dependency injection on below classes
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    SharedPreferences sharedPreferences();

    Retrofit retrofit();
    void inject(SplashActivity splashActivity);
    void inject(CaseDetailsActivity caseDetailsActivity);
    void inject(AudienceTypeFilterActivity audienceTypeFilterActivity);
    void inject(ForgotActivity forgotActivity);
    void inject(PrivacyPolicyFragment privacyPolicyFragment);
    void inject(LanguageFragment privacyPolicyFragment);
    void inject(HelpandFeedbackFragment helpandFeedbackFragment);
    void inject(NotificationFragment notificationFragment);
    void inject(AboutFragment aboutFragment);
    void inject(CinemaTimeApplication cinemaTimeApplication);

    void inject(LoginActivity loginActivity);
    void inject(ReviewFragment reviewFragment);

    void inject(ComingSoonAdapter customadapter_coming_soon);
    void inject(SignupActivity signupActivity);
    void inject(ShowtimeFragment showtimeFragment);
    void inject(NowShowingFragment nowShowingFragment);
    void inject(ComingSoonFragment comingSoonFragment);
    void inject(ExclusiveFragment exclusiveFragment);
    void inject(DescriptionFragment descriptionFragment);
    void inject(SearchFragment searchFragment);
    void inject(HomeFilterPopupActivity homeFilterPopupActivity);
    void inject(TheatresFragment  theatresFragment);
    void inject(MovieDetailActivity movieDetailActivity);
    void inject(TheatreDetailsActivity theatreDetailsActivity);
    void inject(SettingFragment settingFragment);
    void inject(NowShowingAdapter customadapter_now_showing);

    void inject(UserProfileFragment userProfileFragment);

}
