package com.dci.cinematime.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeActivity;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseMessaging extends com.google.firebase.messaging.FirebaseMessagingService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate icon_notification
        sendNotification(remoteMessage.getNotification());


    }


    //This method is only generating push icon_notification
    //It is same as we did in earlier posts
    private void sendNotification(RemoteMessage.Notification messageBody) {
        //Log.d("CINEMATIME","noti"+messageBody.getTitle()+" "+messageBody.getBody());
//        Intent intentNo = new Intent("notificationListener");
//        intentNo.putExtra("notify", true);
//        sendBroadcast(intentNo);
        Intent intent = new Intent(this, HomeActivity.class);
////        intent.putExtra("isFromNotification", true);
////        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bitmap smallicon = BitmapFactory.decodeResource(getResources(), R.mipmap.app_setting_iconthreex);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
////        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_item);
////        contentView.setTextViewText(R.id.text_notification_title,messageBody.getTitle());
////        contentView.setTextViewText(R.id.text_notification_subtitle,messageBody.getBody());
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(smallicon)
                .setSmallIcon(R.mipmap.app_setting_iconthreex)
                .setContentTitle(messageBody.getTitle())
                .setContentText(messageBody.getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }
}
