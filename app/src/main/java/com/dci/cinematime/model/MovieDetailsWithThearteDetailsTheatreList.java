package com.dci.cinematime.model;

public class MovieDetailsWithThearteDetailsTheatreList
{
    private int id;
    private String Theatrename;
    private String Description;
    private String picture;
    private String  Latitude;
    private String Longitude;

    public MovieDetailsWithThearteDetailsTheatreList(int id, String theatrename, String description, String picture, String latitude, String longitude) {
        this.id = id;
        Theatrename = theatrename;
        Description = description;
        this.picture = picture;
        Latitude = latitude;
        Longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTheatrename() {
        return Theatrename;
    }

    public void setTheatrename(String theatrename) {
        Theatrename = theatrename;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}
