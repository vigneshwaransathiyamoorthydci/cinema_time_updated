package com.dci.cinematime.model;

/**
 * Created by Gowshikan on 7/26/2018.
 */

public class HomeFilterPopupResponse
{
    public String text_label_english_now_showing_filter;

   /* public HomeFilterPopup_cl(String text_label_english_now_showing_filter) {

        this.text_label_english_now_showing_filter = text_label_english_now_showing_filter;
    }*/

    public String getText_label_english_now_showing_filter() {
        return text_label_english_now_showing_filter;
    }

    public void setText_label_english_now_showing_filter(String text_label_english_now_showing_filter) {
        this.text_label_english_now_showing_filter = text_label_english_now_showing_filter;
    }
}
