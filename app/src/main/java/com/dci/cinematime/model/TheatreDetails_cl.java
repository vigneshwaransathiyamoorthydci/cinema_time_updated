package com.dci.cinematime.model;

/**
 * Created by Gowshikan on 7/13/2018.
 */

public class TheatreDetails_cl
{
    private String text_label_theatre_details_film_name,text_label_theatre_details_category
            ,text_label_theatre_time,text_label_theatre_time_two;

    public TheatreDetails_cl(String text_label_theatre_details_film_name, String text_label_theatre_details_category,
                             String text_label_theatre_time, String text_label_theatre_time_two) {
        this.text_label_theatre_details_film_name = text_label_theatre_details_film_name;
        this.text_label_theatre_details_category = text_label_theatre_details_category;
        this.text_label_theatre_time = text_label_theatre_time;
        this.text_label_theatre_time_two = text_label_theatre_time_two;
    }

    public String getText_label_theatre_details_film_name() {
        return text_label_theatre_details_film_name;
    }

    public void setText_label_theatre_details_film_name(String text_label_theatre_details_film_name) {
        this.text_label_theatre_details_film_name = text_label_theatre_details_film_name;
    }

    public String getText_label_theatre_details_category() {
        return text_label_theatre_details_category;
    }

    public void setText_label_theatre_details_category(String text_label_theatre_details_category) {
        this.text_label_theatre_details_category = text_label_theatre_details_category;
    }

    public String getText_label_theatre_time() {
        return text_label_theatre_time;
    }

    public void setText_label_theatre_time(String text_label_theatre_time) {
        this.text_label_theatre_time = text_label_theatre_time;
    }

    public String getText_label_theatre_time_two() {
        return text_label_theatre_time_two;
    }

    public void setText_label_theatre_time_two(String text_label_theatre_time_two) {
        this.text_label_theatre_time_two = text_label_theatre_time_two;
    }
}
