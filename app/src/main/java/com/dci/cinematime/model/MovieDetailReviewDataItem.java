package com.dci.cinematime.model;

public class MovieDetailReviewDataItem {
	private String reviews;
	private String updated_at;
	private int user_id;
	private MovieDetailsReviewUserDetails user_details;
	private String created_at;
	private int id;
	private int review_status;
	private int movie_id;

	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public MovieDetailsReviewUserDetails getUser_details() {
		return user_details;
	}

	public void setUser_details(MovieDetailsReviewUserDetails user_details) {
		this.user_details = user_details;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getReview_status() {
		return review_status;
	}

	public void setReview_status(int review_status) {
		this.review_status = review_status;
	}

	public int getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}
}
