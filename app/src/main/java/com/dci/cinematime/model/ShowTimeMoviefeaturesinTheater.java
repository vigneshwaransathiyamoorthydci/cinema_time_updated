package com.dci.cinematime.model;

public class ShowTimeMoviefeaturesinTheater
{
    private int id;
    private String movie_feature;
    private int Status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovie_feature() {
        return movie_feature;
    }

    public void setMovie_feature(String movie_feature) {
        this.movie_feature = movie_feature;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
