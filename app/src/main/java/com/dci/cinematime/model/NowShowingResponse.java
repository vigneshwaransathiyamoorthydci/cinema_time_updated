package com.dci.cinematime.model;

import java.util.List;

import javax.xml.transform.Result;

/**
 * Created by Gowshikan on 7/9/2018.
 */

public class NowShowingResponse {

    private String Status;
    private List<Result> Results = null;
    private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public List<Result> getResults() {
        return Results;
    }

    public void setResults(List<Result> results) {
        this.Results = results;
    }
    public class features
    {
        private int id;
        private String movie_feature;
        private int status;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMovie_feature() {
            return movie_feature;
        }

        public void setMovie_feature(String movie_feature) {
            this.movie_feature = movie_feature;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

    public class Movie_certificate {

        private Integer id;
        private String movie_certificate;
        private String description;
        private Integer status;
        private String created_at;
        private String updated_at;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMovie_certificate() {
            return movie_certificate;
        }

        public void setMovie_certificate(String movie_certificate) {
            this.movie_certificate = movie_certificate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

    }
    public class Result {
        public int getViewType() {
            return viewType;
        }

        public void setViewType(int viewType) {
            this.viewType = viewType;
        }

        private int viewType;
        private Integer id;
        private String name;
        private String movie_average;
        private Integer movie_votes;
        private Integer movie_length;
        private String movie_original_title;
        private String movie_language;
        private String movie_posterimage;
        private String movie_backgroundimg;
        private String movie_trailer;
        private Movie_certificate movie_certificate;
        private MovieLikedByUser movie_liked_by_user;
        private String feature;
        private features features;

        public NowShowingResponse.features getFeatures() {
            return features;
        }

        public void setFeatures(NowShowingResponse.features features) {
            this.features = features;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }

        public MovieLikedByUser getMovie_liked_by_user() {
            return movie_liked_by_user;
        }

        public void setMovie_liked_by_user(MovieLikedByUser movie_liked_by_user) {
            this.movie_liked_by_user = movie_liked_by_user;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMovie_original_title() {
            return movie_original_title;
        }

        public void setMovie_original_title(String movie_original_title) {
            this.movie_original_title = movie_original_title;
        }

        public String getMovie_language() {
            return movie_language;
        }

        public void setMovie_language(String movie_language) {
            this.movie_language = movie_language;
        }

        public String getMovie_posterimage() {
            return movie_posterimage;
        }

        public void setMovie_posterimage(String movie_posterimage) {
            this.movie_posterimage = movie_posterimage;
        }

        public String getMovie_backgroundimg() {
            return movie_backgroundimg;
        }

        public void setMovie_backgroundimg(String movie_backgroundimg) {
            this.movie_backgroundimg = movie_backgroundimg;
        }

        public String getMovie_trailer() {
            return movie_trailer;
        }

        public void setMovie_trailer(String movie_trailer) {
            this.movie_trailer = movie_trailer;
        }

        public Movie_certificate getMovie_certificate() {
            return movie_certificate;
        }

        public void setMovie_certificate(Movie_certificate movie_certificate) {
            this.movie_certificate = movie_certificate;
        }

        public String getMovie_average() {
            return movie_average;
        }

        public void setMovie_average(String movie_average) {
            this.movie_average = movie_average;
        }

        public Integer getMovie_votes() {
            return movie_votes;
        }

        public void setMovie_votes(Integer movie_votes) {
            this.movie_votes = movie_votes;
        }

        public Integer getMovie_length() {
            return movie_length;
        }

        public void setMovie_length(Integer movie_length) {
            this.movie_length = movie_length;
        }


    }

}