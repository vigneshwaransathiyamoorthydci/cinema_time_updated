package com.dci.cinematime.model;

public class MovieDetailReviewItem {
	private String reviews;
	private String crew_lname;
	private String created_at;

	private int review_status;
	private int user_id;
	private int id;

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public MovieDetailReviewItem(String reviews, String crew_lname, int review_status, int user_id, int id,String created_at) {
		this.reviews = reviews;
		this.crew_lname = crew_lname;
		this.review_status = review_status;
		this.user_id = user_id;
		this.id = id;
		this.created_at=created_at;
	}

	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}

	public String getCrew_lname() {
		return crew_lname;
	}

	public void setCrew_lname(String crew_lname) {
		this.crew_lname = crew_lname;
	}

	public int getReview_status() {
		return review_status;
	}

	public void setReview_status(int review_status) {
		this.review_status = review_status;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
