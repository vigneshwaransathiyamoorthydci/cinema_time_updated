package com.dci.cinematime.model;

import android.os.AsyncTask;

public class AudienceTypeResultItem
{
    private int id;
    private String audience;
    private int status;
    private String created_at;
    private String updated_at;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public AudienceTypeResultItem(int id, String audience, int status) {
        this.id = id;
        this.audience = audience;
        this.status = status;
    }
}
