package com.dci.cinematime.model;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Gowshikan on 7/25/2018.
 */


public class ExclusiveResponse {

    private String Status;
    private Results Results;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public Results getResults() {
        return Results;
    }

    public void setResults(Results results) {
        this.Results = results;
    }

    public class Results {

        private Integer current_page;
        private List<Datum> data = null;
        private String first_page_url;
        private Integer from;
        private Integer last_page;
        private String last_page_url;
        private Object next_page_url;
        private String path;
        private Integer per_page;
        private Object prev_page_url;
        private Integer to;
        private Integer total;

        public Integer getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(Integer current_page) {
            this.current_page = current_page;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        public String getFirst_page_url() {
            return first_page_url;
        }

        public void setFirst_page_url(String first_page_url) {
            this.first_page_url = first_page_url;
        }

        public Integer getFrom() {
            return from;
        }

        public void setFrom(Integer from) {
            this.from = from;
        }

        public Integer getLast_page() {
            return last_page;
        }

        public void setLast_page(Integer last_page) {
            this.last_page = last_page;
        }

        public String getLast_page_url() {
            return last_page_url;
        }

        public void setLast_page_url(String last_page_url) {
            this.last_page_url = last_page_url;
        }

        public Object getNext_page_url() {
            return next_page_url;
        }

        public void setNext_page_url(Object next_page_url) {
            this.next_page_url = next_page_url;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public Integer getPer_page() {
            return per_page;
        }

        public void setPer_page(Integer per_page) {
            this.per_page = per_page;
        }

        public Object getPrev_page_url() {
            return prev_page_url;
        }

        public void setPrev_page_url(Object prev_page_url) {
            this.prev_page_url = prev_page_url;
        }

        public Integer getTo() {
            return to;
        }

        public void setTo(Integer to) {
            this.to = to;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }
        public class Datum {

            private Integer id;
            private Integer exclusive;
            private String movie_imdb_id;
            private Integer movie_tmdb_id;
            private String movie_name;
            private String movie_original_title;
            private String movie_language;
            private String movie_category;
            private String movie_tagline;
            private String movie_description;
            private String movie_posterimage;
            private String movie_backgroundimg;
            private String movie_trailer;
            private Integer movie_length;
            private Integer movie_type;
            private Integer movie_view_count;
            private String movie_vote_average;
            private Integer movie_vote_count;
            private String movie_crews;
            private String movie_casts;
            private Integer movie_status;
            private String movie_released_date;
            private Integer movie_certificate;
            private String movie_feature;
            private String created_at;
            private String updated_at;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getExclusive() {
                return exclusive;
            }

            public void setExclusive(Integer exclusive) {
                this.exclusive = exclusive;
            }

            public String getMovie_imdb_id() {
                return movie_imdb_id;
            }

            public void setMovie_imdb_id(String movie_imdb_id) {
                this.movie_imdb_id = movie_imdb_id;
            }

            public Integer getMovie_tmdb_id() {
                return movie_tmdb_id;
            }

            public void setMovie_tmdb_id(Integer movie_tmdb_id) {
                this.movie_tmdb_id = movie_tmdb_id;
            }

            public String getMovie_name() {
                return movie_name;
            }

            public void setMovie_name(String movie_name) {
                this.movie_name = movie_name;
            }

            public String getMovie_original_title() {
                return movie_original_title;
            }

            public void setMovie_original_title(String movie_original_title) {
                this.movie_original_title = movie_original_title;
            }

            public String getMovie_language() {
                return movie_language;
            }

            public void setMovie_language(String movie_language) {
                this.movie_language = movie_language;
            }

            public String getMovie_category() {
                return movie_category;
            }

            public void setMovie_category(String movie_category) {
                this.movie_category = movie_category;
            }

            public String getMovie_tagline() {
                return movie_tagline;
            }

            public void setMovie_tagline(String movie_tagline) {
                this.movie_tagline = movie_tagline;
            }

            public String getMovie_description() {
                return movie_description;
            }

            public void setMovie_description(String movie_description) {
                this.movie_description = movie_description;
            }

            public String getMovie_posterimage() {
                return movie_posterimage;
            }

            public void setMovie_posterimage(String movie_posterimage) {
                this.movie_posterimage = movie_posterimage;
            }

            public String getMovie_backgroundimg() {
                return movie_backgroundimg;
            }

            public void setMovie_backgroundimg(String movie_backgroundimg) {
                this.movie_backgroundimg = movie_backgroundimg;
            }

            public String getMovie_trailer() {
                return movie_trailer;
            }

            public void setMovie_trailer(String movie_trailer) {
                this.movie_trailer = movie_trailer;
            }

            public Integer getMovie_length() {
                return movie_length;
            }

            public void setMovie_length(Integer movie_length) {
                this.movie_length = movie_length;
            }

            public Integer getMovie_type() {
                return movie_type;
            }

            public void setMovie_type(Integer movie_type) {
                this.movie_type = movie_type;
            }

            public Integer getMovie_view_count() {
                return movie_view_count;
            }

            public void setMovie_view_count(Integer movie_view_count) {
                this.movie_view_count = movie_view_count;
            }

            public String getMovie_vote_average() {
                return movie_vote_average;
            }

            public void setMovie_vote_average(String movie_vote_average) {
                this.movie_vote_average = movie_vote_average;
            }

            public Integer getMovie_vote_count() {
                return movie_vote_count;
            }

            public void setMovie_vote_count(Integer movie_vote_count) {
                this.movie_vote_count = movie_vote_count;
            }

            public String getMovie_crews() {
                return movie_crews;
            }

            public void setMovie_crews(String movie_crews) {
                this.movie_crews = movie_crews;
            }

            public String getMovie_casts() {
                return movie_casts;
            }

            public void setMovie_casts(String movie_casts) {
                this.movie_casts = movie_casts;
            }

            public Integer getMovie_status() {
                return movie_status;
            }

            public void setMovie_status(Integer movie_status) {
                this.movie_status = movie_status;
            }

            public String getMovie_released_date() {
                return movie_released_date;
            }

            public void setMovie_released_date(String movie_released_date) {
                this.movie_released_date = movie_released_date;
            }

            public Integer getMovie_certificate() {
                return movie_certificate;
            }

            public void setMovie_certificate(Integer movie_certificate) {
                this.movie_certificate = movie_certificate;
            }

            public String getMovie_feature() {
                return movie_feature;
            }

            public void setMovie_feature(String movie_feature) {
                this.movie_feature = movie_feature;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

        }

    }


}