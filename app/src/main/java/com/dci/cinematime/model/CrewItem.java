package com.dci.cinematime.model;

public class CrewItem{
	private String role;
	private String name;
	private int id;
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//	public CrewItem(String role, String name, String id) {
//		this.role = role;
//		this.name = name;
//		this.id = id;
//	}
}
