package com.dci.cinematime.model;

import java.io.Serializable;
import java.util.List;

public class TheatersList implements Serializable {
    private String Status;
    private String Message;


    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getStatus() {
        return Status;
    }

    private List<ResultsItem> Results;

    public List<ResultsItem> getResults() {
        return Results;
    }

    public void setResults(List<ResultsItem> results) {
        Results = results;
    }

    public class ResultsItem {
        private int Status;
        private Country country;
        private String Email;
        private String Description;
        private String Address;
        private String createdAt;
        private String Latitude;
        private int City;
        private String Mobile;
        private String Longitude;
        private String Theatrename;
        private String updatedAt;
        private String Phone;
        private int State;
        private int Country;
        private Citylist citylist;
        private String NoofScreens;
        private int id;
        private State state;
        private String Pincode;
        private String picture;

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }
        public int getStatus() {
            return Status;
        }

        public void setStatus(int status) {
            Status = status;
        }

        public Country getCountry() {
            return country;
        }

        public void setCountry(int country) {
            Country = country;
        }

        public Citylist getCitylist() {
            return citylist;
        }

        public void setCitylist(Citylist citylist) {
            this.citylist = citylist;
        }

        public String getNoofScreens() {
            return NoofScreens;
        }

        public void setNoofScreens(String noofScreens) {
            NoofScreens = noofScreens;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setCountry(Country country) {
            this.country = country;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public int getCity() {
            return City;
        }

        public void setCity(int city) {
            City = city;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String mobile) {
            Mobile = mobile;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getTheatrename() {
            return Theatrename;
        }

        public void setTheatrename(String theatrename) {
            Theatrename = theatrename;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public State getstatee() {
            return state;
        }

        public void setState(State state) {
            this.state = state;
        }

        public String getPincode() {
            return Pincode;
        }

        public void setPincode(String pincode) {
            Pincode = pincode;
        }

        public void setState(int state) {
            State = state;
        }

        public int getState() {
            return State;
        }


    }

    public class Country {
        private int Status;
        private String Countryname;
        private String updatedAt;
        private String createdAt;
        private int id;

        public int getStatus() {
            return Status;
        }

        public void setStatus(int status) {
            Status = status;
        }

        public String getCountryname() {
            return Countryname;
        }

        public void setCountryname(String countryname) {
            Countryname = countryname;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }


    }

    public class State {
        private int Countryid;
        private int Status;
        private String updatedAt;
        private String Statename;
        private String createdAt;
        private int id;


        public int getCountryid() {
            return Countryid;
        }

        public void setCountryid(int countryid) {
            Countryid = countryid;
        }

        public int getStatus() {
            return Status;
        }

        public void setStatus(int status) {
            Status = status;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getStatename() {
            return Statename;
        }

        public void setStatename(String statename) {
            Statename = statename;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }


    }

    public class Citylist {
        private int Countryid;
        private int Status;
        private String updatedAt;
        private int Stateid;
        private String createdAt;
        private int id;
        private String Cityname;


        public int getCountryid() {
            return Countryid;
        }

        public void setCountryid(int countryid) {
            Countryid = countryid;
        }

        public int getStatus() {
            return Status;
        }

        public void setStatus(int status) {
            Status = status;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getStateid() {
            return Stateid;
        }

        public void setStateid(int stateid) {
            Stateid = stateid;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCityname() {
            return Cityname;
        }

        public void setCityname(String cityname) {
            Cityname = cityname;
        }

    }


/*@Override
     public String toString(){
		return
			"TheatersList{" +
			"Status = '" + status + '\'' +
			",SearchResults = '" + results + '\'' +
			"}";
		}*/
}