package com.dci.cinematime.model;

public class CastItem{
	private String crew_fname;
	private String crew_lname;
	private String crew_image;
	private int id;
	private String crew_role;
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCrew_role() {
		return crew_role;
	}

	public void setCrew_role(String crew_role) {
		this.crew_role = crew_role;
	}

	public String getCrew_fname() {
		return crew_fname;
	}

	public void setCrew_fname(String crew_fname) {
		this.crew_fname = crew_fname;
	}

	public String getCrew_lname() {
		return crew_lname;
	}

	public void setCrew_lname(String crew_lname) {
		this.crew_lname = crew_lname;
	}

	public String getCrew_image() {
		return crew_image;
	}

	public void setCrew_image(String crew_image) {
		this.crew_image = crew_image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

//	public CastItem(int id, String crew_fname,String crew_lname,String crew_image) {
//		this.id=id;
//		this.crew_fname=crew_fname;
//		this.crew_lname=crew_lname;
//		this.crew_image=crew_image;
//
//	}
}
