package com.dci.cinematime.model;

public class MovieDetailsReviewResponse{
	private String Status;
	private MovieDetailsReviewResults Results;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public MovieDetailsReviewResults getResults() {
		return Results;
	}

	public void setResults(MovieDetailsReviewResults results) {
		Results = results;
	}
}
