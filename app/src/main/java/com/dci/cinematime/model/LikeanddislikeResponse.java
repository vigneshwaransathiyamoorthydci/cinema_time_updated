package com.dci.cinematime.model;

public class LikeanddislikeResponse{
	private String Status;
	private likeandDislikeResults Results;

	public String getStatus() {
		return Status;
	}
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public likeandDislikeResults getResults() {
		return Results;
	}

	public void setResults(likeandDislikeResults results) {
		Results = results;
	}
}
