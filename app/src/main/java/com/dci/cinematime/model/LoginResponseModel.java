package com.dci.cinematime.model;

/**
 * Created by Gowshikan on 7/19/2018.
 */


    public class LoginResponseModel {

        private String Status;
        private SignupResponseModel.Results Results;
        private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            this.Status = status;
        }

        public SignupResponseModel.Results getResults() {
            return Results;
        }

        public void setResults(SignupResponseModel.Results results) {
            this.Results = results;
        }


    public class Results {

        private Integer id;
        private String user_fname;
        private Object user_lname;
        private String user_profile_img;
        private String gender;
        private String dob;
        private String device_name;
        private String password;
        private Object fb_id;
        private Object google_id;
        private String phone;
        private String email;
        private String device_id;
        private String device_imei;
        private String device_os;
        private String app_version;
        private Integer user_status;
        private String created_at;
        private String updated_at;

        public String getUser_profile_img() {
            return user_profile_img;
        }

        public void setUser_profile_img(String user_profile_img) {
            this.user_profile_img = user_profile_img;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUser_fname() {
            return user_fname;
        }

        public void setUser_fname(String user_fname) {
            this.user_fname = user_fname;
        }

        public Object getUser_lname() {
            return user_lname;
        }

        public void setUser_lname(Object user_lname) {
            this.user_lname = user_lname;
        }





        public String getDevice_name() {
            return device_name;
        }

        public void setDevice_name(String device_name) {
            this.device_name = device_name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Object getFb_id() {
            return fb_id;
        }

        public void setFb_id(Object fb_id) {
            this.fb_id = fb_id;
        }

        public Object getGoogle_id() {
            return google_id;
        }

        public void setGoogle_id(Object google_id) {
            this.google_id = google_id;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getDevice_imei() {
            return device_imei;
        }

        public void setDevice_imei(String device_imei) {
            this.device_imei = device_imei;
        }

        public String getDevice_os() {
            return device_os;
        }

        public void setDevice_os(String device_os) {
            this.device_os = device_os;
        }

        public String getApp_version() {
            return app_version;
        }

        public void setApp_version(String app_version) {
            this.app_version = app_version;
        }

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

    }

    }

