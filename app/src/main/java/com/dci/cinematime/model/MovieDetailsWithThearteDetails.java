package com.dci.cinematime.model;

import java.util.ArrayList;
import java.util.HashMap;

public class MovieDetailsWithThearteDetails
{
    private int id;
    private String name;
    private String movie_original_title;
    private String movie_language;
    private String movie_posterimage;
    private String movie_backgroundimg;
    private String movie_trailer;
    private ArrayList<MovieDetailsWithThearteDetailsTheatreList> movieDetailsWithThearteDetailsTheatreList;
    private HashMap showTimeMovieTiminginTheater;

    public ArrayList<MovieDetailsWithThearteDetailsTheatreList> getMovieDetailsWithThearteDetailsTheatreList() {
        return movieDetailsWithThearteDetailsTheatreList;
    }

    public void setMovieDetailsWithThearteDetailsTheatreList(ArrayList<MovieDetailsWithThearteDetailsTheatreList> movieDetailsWithThearteDetailsTheatreList) {
        this.movieDetailsWithThearteDetailsTheatreList = movieDetailsWithThearteDetailsTheatreList;
    }


    public HashMap getShowTimeMovieTiminginTheater() {
        return showTimeMovieTiminginTheater;
    }

    public void setShowTimeMovieTiminginTheater(HashMap showTimeMovieTiminginTheater) {
        this.showTimeMovieTiminginTheater = showTimeMovieTiminginTheater;
    }

    public MovieDetailsWithThearteDetails(int id, String name, String movie_original_title, String movie_language, String movie_posterimage, String movie_backgroundimg, String movie_trailer, ArrayList<MovieDetailsWithThearteDetailsTheatreList> movieDetailsWithThearteDetailsTheatreList
    , HashMap showTimeMovieTiminginTheater) {
        this.id = id;
        this.name = name;
        this.movie_original_title = movie_original_title;
        this.movie_language = movie_language;
        this.movie_posterimage = movie_posterimage;
        this.movie_backgroundimg = movie_backgroundimg;
        this.movie_trailer = movie_trailer;
        this.movieDetailsWithThearteDetailsTheatreList=movieDetailsWithThearteDetailsTheatreList;
        this.showTimeMovieTiminginTheater=showTimeMovieTiminginTheater;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMovie_original_title() {
        return movie_original_title;
    }

    public void setMovie_original_title(String movie_original_title) {
        this.movie_original_title = movie_original_title;
    }

    public String getMovie_language() {
        return movie_language;
    }

    public void setMovie_language(String movie_language) {
        this.movie_language = movie_language;
    }

    public String getMovie_posterimage() {
        return movie_posterimage;
    }

    public void setMovie_posterimage(String movie_posterimage) {
        this.movie_posterimage = movie_posterimage;
    }

    public String getMovie_backgroundimg() {
        return movie_backgroundimg;
    }

    public void setMovie_backgroundimg(String movie_backgroundimg) {
        this.movie_backgroundimg = movie_backgroundimg;
    }

    public String getMovie_trailer() {
        return movie_trailer;
    }

    public void setMovie_trailer(String movie_trailer) {
        this.movie_trailer = movie_trailer;
    }
}
