package com.dci.cinematime.model;

public class CastRelatedMovieDetails
{

        private int id;
        private String movie_name;
        private String movie_original_title;
        private String movie_posterimage;
        private String movie_backgroundimg;
        private String moviePosterImageBaseUrl;
        private int releaseType;

    public int getReleaseType() {
        return releaseType;
    }

    public void setReleaseType(int releaseType) {
        this.releaseType = releaseType;
    }

    public String getMoviePosterImageBaseUrl() {
        return moviePosterImageBaseUrl;
    }

    public void setMoviePosterImageBaseUrl(String moviePosterImageBaseUrl) {
        this.moviePosterImageBaseUrl = moviePosterImageBaseUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }

    public String getMovie_original_title() {
        return movie_original_title;
    }

    public void setMovie_original_title(String movie_original_title) {
        this.movie_original_title = movie_original_title;
    }

    public String getMovie_posterimage() {
        return movie_posterimage;
    }

    public void setMovie_posterimage(String movie_posterimage) {
        this.movie_posterimage = movie_posterimage;
    }

    public String getMovie_backgroundimg() {
        return movie_backgroundimg;
    }

    public void setMovie_backgroundimg(String movie_backgroundimg) {
        this.movie_backgroundimg = movie_backgroundimg;
    }
}
