package com.dci.cinematime.model;

import java.util.ArrayList;

public class AudienceType
{
    private String Status;
    private ArrayList<AudienceTypeResultItem> Results;
    private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ArrayList<AudienceTypeResultItem> getResults() {
        return Results;
    }

    public void setResults(ArrayList<AudienceTypeResultItem> results) {
        Results = results;
    }
}
