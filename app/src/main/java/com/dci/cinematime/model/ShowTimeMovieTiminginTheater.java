package com.dci.cinematime.model;

public class ShowTimeMovieTiminginTheater
{
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    int id;
    String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ShowTimeMovieTiminginTheater(String time, String audienceType,int id,String title) {
        this.time = time;
        this.audienceType = audienceType;
        this.id=id;
        this.title=title;

    }

    private String audienceType;

    public String getAudienceType() {
        return audienceType;
    }

    public void setAudienceType(String audienceType) {
        this.audienceType = audienceType;
    }
}
