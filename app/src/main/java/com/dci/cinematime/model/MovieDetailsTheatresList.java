package com.dci.cinematime.model;

import java.util.ArrayList;
import java.util.HashMap;

public class MovieDetailsTheatresList
{
    private String name;
    private String Latitude;
    private String Longitude;
    private String Email;
    private String NoofScreens;
    private HashMap showTimeMovieTiminginTheater;
    private String city;
    private int id;
    private String profileImg;

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public MovieDetailsTheatresList(String name, String latitude, String longitude, String email, String noofScreens, HashMap showTimeMovieTiminginTheater,String city,int id,
                                    String profileImg) {
        this.name = name;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.Email = email;
        this.NoofScreens = noofScreens;
        this.showTimeMovieTiminginTheater=showTimeMovieTiminginTheater;
        this.city=city;
        this.id=id;
        this.profileImg=profileImg;
    }

    public HashMap getShowTimeMovieTiminginTheater() {
        return showTimeMovieTiminginTheater;
    }

    public void setShowTimeMovieTiminginTheater(HashMap showTimeMovieTiminginTheater) {
        this.showTimeMovieTiminginTheater = showTimeMovieTiminginTheater;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNoofScreens() {
        return NoofScreens;
    }

    public void setNoofScreens(String noofScreens) {
        NoofScreens = noofScreens;
    }
}
