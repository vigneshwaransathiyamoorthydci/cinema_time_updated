package com.dci.cinematime.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.transform.Result;

/**
 * Created by Gowshikan on 7/10/2018.
 */

public class ComingSoonResponse {

    private String Status;
    private List<Result> Results = null;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public List<Result> getResults() {
        return Results;
    }

    public void setResults(List<Result> results) {
        this.Results = results;
    }


    public static class Result  {

        private Integer id;
        private String name;
        private String movie_original_title;
        private String movie_language;
        private String movie_posterimage;
        private String movie_backgroundimg;
        private String movie_average;
        private Integer movie_votes;
        private String movie_trailer;
        private Integer notify_status;
        private String movie_certi;

        public String getMovie_certi() {
            return movie_certi;
        }

        public void setMovie_certi(String movie_certi) {
            this.movie_certi = movie_certi;
        }

        public Movie_certificate getMovie_certificate() {
            return movie_certificate;
        }

        public void setMovie_certificate(Movie_certificate movie_certificate) {
            this.movie_certificate = movie_certificate;
        }

        private Movie_certificate movie_certificate;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMovie_original_title() {
            return movie_original_title;
        }

        public void setMovie_original_title(String movie_original_title) {
            this.movie_original_title = movie_original_title;
        }

        public String getMovie_language() {
            return movie_language;
        }

        public void setMovie_language(String movie_language) {
            this.movie_language = movie_language;
        }

        public String getMovie_posterimage() {
            return movie_posterimage;
        }

        public void setMovie_posterimage(String movie_posterimage) {
            this.movie_posterimage = movie_posterimage;
        }

        public String getMovie_backgroundimg() {
            return movie_backgroundimg;
        }

        public void setMovie_backgroundimg(String movie_backgroundimg) {
            this.movie_backgroundimg = movie_backgroundimg;
        }

        public String getMovie_trailer() {
            return movie_trailer;
        }

        public void setMovie_trailer(String movie_trailer) {
            this.movie_trailer = movie_trailer;
        }

        public Integer getNotify_status() {
            return notify_status;
        }

        public void setNotify_status(Integer notify_status) {
            this.notify_status = notify_status;
        }




    }
    public class Movie_certificate {

        private Integer id;
        private String movie_certificate;
        private String description;
        private Integer status;
        private String created_at;
        private String updated_at;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMovie_certificate() {
            return movie_certificate;
        }

        public void setMovie_certificate(String movie_certificate) {
            this.movie_certificate = movie_certificate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

    }

}
