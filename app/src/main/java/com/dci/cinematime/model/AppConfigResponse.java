package com.dci.cinematime.model;

public class AppConfigResponse {
	private String Status;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}
	private Results Results;

	public AppConfigResponse.Results getResults() {
		return Results;
	}

	public void setResults(AppConfigResponse.Results results) {
		Results = results;
	}

	public class  Results
	{
		private String movieImgFolder;
		private String crewProfileImg;
		private String theatreImgFolder;
		private String userProfileImg;

		public String getUserProfileImg() {
			return userProfileImg;
		}

		public void setUserProfileImg(String userProfileImg) {
			this.userProfileImg = userProfileImg;
		}

		public String getMovieImgFolder() {
			return movieImgFolder;
		}

		public void setMovieImgFolder(String movieImgFolder) {
			this.movieImgFolder = movieImgFolder;
		}

		public String getCrewProfileImg() {
			return crewProfileImg;
		}

		public void setCrewProfileImg(String crewProfileImg) {
			this.crewProfileImg = crewProfileImg;
		}

		public String getTheatreImgFolder() {
			return theatreImgFolder;
		}

		public void setTheatreImgFolder(String theatreImgFolder) {
			this.theatreImgFolder = theatreImgFolder;
		}
	}


}
