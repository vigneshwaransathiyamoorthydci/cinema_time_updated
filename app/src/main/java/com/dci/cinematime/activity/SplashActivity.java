package com.dci.cinematime.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.AppConfigResponse;
import com.dci.cinematime.model.SignupResponseModel;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;

import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    String language = "en";
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        String languageToLoad  = "ar";
//        Locale locale = new Locale(languageToLoad);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,
//                getBaseContext().getResources().getDisplayMetrics());
        CinemaTimeApplication.getContext().getComponent().inject(this);
            editor = sharedPreferences.edit();
        if (sharedPreferences.getInt(Constants.SELECTEDLANGUAGES, 0) == 1) {
            language = "ar";
            LanguageHelper.setLanguage(this, language);
            editor.putInt(Constants.SELECTEDLANGUAGES, 1).commit();


        } else {
            language = "en";
            LanguageHelper.setLanguage(this, language);
            editor.putInt(Constants.SELECTEDLANGUAGES, 0).commit();


        }
        getAppConfig();


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    private void getAppConfig() {

        if (Util.isNetworkAvailable()) {
            cinemaTimeAPI.getAppConfig().enqueue(new Callback<AppConfigResponse>() {
                @Override
                public void onResponse(Call<AppConfigResponse> call, Response<AppConfigResponse> response) {
                    if (response.body()!=null && response.isSuccessful())
                    {
                        AppConfigResponse appConfigResponse = response.body();
                        if (appConfigResponse.getStatus().equals("Success"))
                        {

                            Util.MOVIEIMGFOLDER=appConfigResponse.getResults().getMovieImgFolder();
                            Util.CREWPROFILEIMAGE=appConfigResponse.getResults().getCrewProfileImg();
                            Util.THEATREIMAGEFOLDER=appConfigResponse.getResults().getTheatreImgFolder();
                            Util.USERPROFILEIMAGE=appConfigResponse.getResults().getUserProfileImg();


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (AppPreference.isLoggedIn(SplashActivity.this)) {
                                        AppPreference.setSkip(SplashActivity.this, true);
                                        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                        finish();
                                    } else {
                                        AppPreference.setSkip(SplashActivity.this, false);
                                        LanguageHelper.setLanguage(getApplicationContext(), language);
                                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                        finish();
                                    }

                                }
                            }, 1000);
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AppConfigResponse> call, Throwable t) {

                }
            });

        }
        else
        {
            Toast.makeText(this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }
}
