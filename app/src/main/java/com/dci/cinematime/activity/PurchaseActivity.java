package com.dci.cinematime.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.dci.cinematime.R;

public class PurchaseActivity extends Activity  implements BillingProcessor.IBillingHandler{
    BillingProcessor bp;
    ImageView image_pop_corn;
    Button button_buy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);

        bp = new BillingProcessor(this, null, this);
        bp.initialize();
        button_buy=(Button)findViewById(R.id.button_buy);
        image_pop_corn=(ImageView)findViewById(R.id.image_pop_corn);
        image_pop_corn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //bp.purchase(PurchaseActivity.this,"android.test.purchased");

            }
        });
        button_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bp.purchase(PurchaseActivity.this,"android.test.purchased");
            }
        });
        // or bp = BillingProcessor.newBillingProcessor(this, "YOUR LICENSE KEY FROM GOOGLE PLAY CONSOLE HERE", this);
        // See below on why this is a useful alternative
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
                Toast.makeText(getApplicationContext(),details.purchaseInfo.purchaseData.purchaseState.toString(),Toast.LENGTH_SHORT).show();
                Intent intent_logout = new Intent(this, HomeActivity.class);
                startActivity(intent_logout);
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                finish();
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();
        Intent intent_logout = new Intent(this, HomeActivity.class);
        startActivity(intent_logout);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();

    }

    @Override
    public void onBillingInitialized() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }
}
