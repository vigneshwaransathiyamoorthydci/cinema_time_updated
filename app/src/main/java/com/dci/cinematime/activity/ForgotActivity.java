package com.dci.cinematime.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.LoginResponseModel;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity extends BaseActivity {

    Button button_forgot;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    EditText Edit_text_label_forgot_email;
    ImageView image_forgot_back_arrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        button_forgot=findViewById(R.id.button_forgot);
        Edit_text_label_forgot_email=(EditText)findViewById(R.id.Edit_text_label_forgot_email);
        image_forgot_back_arrow=(ImageView)findViewById(R.id.image_forgot_back_arrow);
        image_forgot_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        button_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Util.isValidEmailAddress(Edit_text_label_forgot_email.getText().toString()))
                {
                    Edit_text_label_forgot_email.setError(getString(R.string.enter_validemail));
                }
                else
                {
                    forgetPassword();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    private void forgetPassword() {

        if (Util.isNetworkAvailable()) {
            showProgress("Please Wait.....");


            cinemaTimeAPI.getforgetPassword(Edit_text_label_forgot_email.getText().toString()).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();

                    if (response.body() != null) {
                        Log.e("login response", "" + response.toString());
                        try {
                            JSONObject object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("Status").equals("Success"))
                            {
                                JSONArray jsonArray = object.getJSONArray("Results");
                                if (object.getString("Status").equals("Success"))
                                {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        String value=jsonArray.getString(i);

                                        Toast.makeText(ForgotActivity.this, value, Toast.LENGTH_SHORT).show();
                                    }
                                    finish();
                                    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                                }
                            }

                            else
                            {

                                    Toast.makeText(ForgotActivity.this, object.getString("Message"), Toast.LENGTH_SHORT).show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ForgotActivity.this, "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            hideProgress();
            Toast.makeText(this, "no internet", Toast.LENGTH_SHORT).show();

        }

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
