package com.dci.cinematime.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.dci.cinematime.R;

import com.dci.cinematime.fragment.HomeFragment;
import com.dci.cinematime.fragment.SearchFragment;
import com.dci.cinematime.fragment.SettingFragment;
import com.dci.cinematime.fragment.TheatresFragment;
import com.dci.cinematime.utils.LanguageHelper;

public class HomeActivity extends BaseActivity implements AHBottomNavigation.OnTabSelectedListener {


    AHBottomNavigation bottomNavigationView;
    android.support.v4.app.Fragment fragment;
    FrameLayout constrain_home_tool_bar;
    public   ImageView image_filter;
    public static int filteroption=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        image_filter = findViewById(R.id.image_filter);
        bottomNavigationView = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnTabSelectedListener(this);
        bottomNavigationView.setAccentColor(Color.parseColor("#DA3D31"));

        bottomNavigationView.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigationView.setTitleTextSize(40,35);
       // bottomNavigationView.setBackground(getDrawable(R.drawable.background_notifycolour));
        createitems();
        constrain_home_tool_bar = findViewById(R.id.constrain_home_tool_bar);
        fragment = new HomeFragment();
        setFragment(fragment);

        image_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filteroption==0)
                {
                    startActivity(new Intent(HomeActivity.this, HomeFilterPopupActivity.class));
                }
                else
                {
                    startActivity(new Intent(HomeActivity.this, AudienceTypeFilterActivity.class));
                }

                HomeActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

    }



   /* private void showBottomSheetDialog() {

        View view = getLayoutInflater().inflate(R.layout.nowshowing_bottom_sheet_dialog_filter, null);
        NowshowingBottomSheetFragment nowshowingBottomSheetFragment=new NowshowingBottomSheetFragment();
        nowshowingBottomSheetFragment.show(getSupportFragmentManager(),nowshowingBottomSheetFragment.getTag());


    }
*/
    public void hidetoolbar() {
        constrain_home_tool_bar.setVisibility(View.GONE);
    }

    public void showtoolbar() {
        constrain_home_tool_bar.setVisibility(View.VISIBLE);


    }


    private void createitems() {


        AHBottomNavigationItem homeitem = new AHBottomNavigationItem(getString(R.string.home), R.mipmap.show_type_iconthreex);
        AHBottomNavigationItem theatreitem = new AHBottomNavigationItem(getString(R.string.theatres), R.mipmap.theatre_iconthreex);
        AHBottomNavigationItem searchitem = new AHBottomNavigationItem(getString(R.string.search), R.mipmap.search_iconthreex);
        AHBottomNavigationItem settingitem = new AHBottomNavigationItem(getString(R.string.settings), R.mipmap.setings_iconthreex);

        //      AHBottomNavigationItem dashItem=new AHBottomNavigationItem(getString(R.string.dashboard),R.drawable.dashboard_icon);
        //     AHBottomNavigationItem contactItem=new
        bottomNavigationView.addItem(homeitem);
        bottomNavigationView.addItem(theatreitem);
        bottomNavigationView.addItem(searchitem);
        bottomNavigationView.addItem(settingitem);
        bottomNavigationView.setCurrentItem(0);


    }


    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {

        if (position == 0 && !wasSelected) {
            fragment = new HomeFragment();
            setFragment(fragment);
        } else if (position == 1 && !wasSelected) {
            fragment = new TheatresFragment();
            setFragment(fragment);
        } else if (position == 2 && !wasSelected) {
            fragment = new SearchFragment();
            setFragment(fragment);
        } else if (position == 3 && !wasSelected) {
            fragment = new SettingFragment();
            setFragment(fragment);


        }
        return true;
    }


    public void setFragment(android.support.v4.app.Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.constraint_tap_layout_home, fragment, "About Us");
            fragmentTransaction.commit();
        }
    }
    @Override
    public void onBackPressed() {

        if (bottomNavigationView.getCurrentItem()!=0)
        {
            bottomNavigationView.setCurrentItem(0);
            fragment = new HomeFragment();
            setFragment(fragment);
        }
        else
        {
            finish();
        }
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
