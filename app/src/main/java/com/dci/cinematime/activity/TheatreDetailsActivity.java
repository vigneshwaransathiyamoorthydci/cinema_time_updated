package com.dci.cinematime.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.TheatreDetailsAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.MovieCertificate;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.dci.cinematime.model.ShowTimeMoviefeaturesinTheater;
import com.dci.cinematime.model.TheatreBasedMovieList;
import com.dci.cinematime.model.TheatreDetails_cl;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.inject.Inject;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TheatreDetailsActivity extends BaseActivity {

    HorizontalCalendar horizontalCalendar;

    private List<TheatreDetails_cl> theatre = new ArrayList<>();
    private RecyclerView recyclerView;
    private TheatreDetailsAdapter mAdapter;
    ImageView image_theatre_settings_back_arrow,image_theatre_settings_filter_icon;
    String selectedDateStr;
    TextView text_label_theatre_setting_heading_date_years;
    int theatreId;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    String formattedDate;
    ArrayList<TheatreBasedMovieList> theatreBasedMovieList;
    public  ArrayList<ShowTimeMovieTiminginTheater> showTimeMovieTiminginTheater;
    private ArrayList<ShowTimeMoviefeaturesinTheater> showTimeMoviefeaturesinTheater;
    private ArrayList<MovieCertificate> movieCertificate;
    TextView theatreTextview;
    String theatreName;
    SwipeRefreshLayout movieListswipelayout;
    HashMap Showtimingmap = new HashMap();
    ImageView imageNoList;
    TextView textNoList;
    boolean exclusive=false;
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theatre_details);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        text_label_theatre_setting_heading_date_years=findViewById(R.id.text_label_theatre_setting_heading_date_years);
        theatreTextview=(TextView)findViewById(R.id.text_label_theatre_setting_heading_name);
        image_theatre_settings_filter_icon=findViewById(R.id.image_theatre_settings_filter_icon);
        image_theatre_settings_filter_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(TheatreDetailsActivity.this,AudienceTypeFilterActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                //Toast.makeText(getApplicationContext(),"Under Development",Toast.LENGTH_SHORT).show();

            }
        });

        movieListswipelayout = findViewById(R.id.swip_lay);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        formattedDate = df.format(c);
        theatreBasedMovieList=new ArrayList<TheatreBasedMovieList>();
        showTimeMovieTiminginTheater=new ArrayList<ShowTimeMovieTiminginTheater>();
        showTimeMoviefeaturesinTheater=new ArrayList<ShowTimeMoviefeaturesinTheater>();
        movieCertificate=new ArrayList<MovieCertificate>();
        recyclerView = (RecyclerView) findViewById(R.id.recycle_theatre_details);
        image_theatre_settings_back_arrow = findViewById(R.id.image_theatre_settings_back_arrow);
        imageNoList=(ImageView)findViewById(R.id.image_no_comments);
        textNoList=(TextView)findViewById(R.id.text_no_comm);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        Intent bundle = getIntent();
        if (getIntent() != null) {
            theatreId = bundle.getIntExtra("theatreid", 0);
            theatreName = bundle.getStringExtra("theatrename");
            exclusive=bundle.getBooleanExtra("exclusive",false);

        }
        mAdapter = new TheatreDetailsAdapter(theatreBasedMovieList, TheatreDetailsActivity.this);
        recyclerView.setAdapter(mAdapter);
        theatreTextview.setText(theatreName);
        //theatredetails(theatreId,formattedDate);
        selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", Calendar.getInstance()).toString();
        text_label_theatre_setting_heading_date_years.setText(selectedDateStr);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 1);


        /* end after 2 months from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        // Default Date set to Today.
        final Calendar defaultSelectedDate = Calendar.getInstance();
       // horizontalCalendar.getSelectedDatePosition()


        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.horizondalcalendarview)

                .range(Calendar.getInstance(), endDate)
                .datesNumberOnScreen(5)
                .configure()
                .formatTopText("MMM")
                .formatMiddleText("EEE")
                .formatBottomText("dd")
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.LTGRAY, Color.WHITE)
                .colorTextMiddle(Color.LTGRAY, Color.parseColor("#ffd54f"))
                .end()
                .defaultSelectedDate(defaultSelectedDate)
                .addEvents(new CalendarEventsPredicate() {

                    Random rnd = new Random();

                    @Override
                    public List<CalendarEvent> events(Calendar date) {
                        List<CalendarEvent> events = new ArrayList<>();
                        //int count = rnd.nextInt(6);

                       /* for (int i = 0; i <= count; i++){
                            events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
                        }*/

                        return events;
                    }
                })
                .build();


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                Date selecteddate = null;
                selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                text_label_theatre_setting_heading_date_years.setText(selectedDateStr);
              // Date c = Calendar.getInstance().getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
                try {
                     selecteddate = dateFormat.parse(selectedDateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
                formattedDate = dfs.format(selecteddate);
                //Toast.makeText(TheatreDetailsActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
                Log.i("onDateSelected", selectedDateStr + " - Position = " + position);
                theatreBasedMovieList.clear();
                theatredetails(theatreId,formattedDate);
            }

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView, int dx, int dy) {
                super.onCalendarScroll(calendarView, dx, dy);
                //Toast.makeText(TheatreDetailsActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean onDateLongClicked(Calendar date, int position) {
                Toast.makeText(TheatreDetailsActivity.this, selectedDateStr + " selected!", Toast.LENGTH_SHORT).show();
                return super.onDateLongClicked(date, position);

            }
        });


        image_theatre_settings_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
        movieListswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                movieListswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        theatreBasedMovieList.clear();
                        theatredetails(theatreId,formattedDate);
                        movieListswipelayout.setRefreshing(false);

                    }

                }, 000);
            }
        });


    }

    private void theatredetails(int theatreId,String date) {

        if (Util.isNetworkAvailable())
        {
            imageNoList.setVisibility(View.GONE);
            //imageNoList.setImageResource();
            textNoList.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            showProgress();
            cinemaTimeAPI.getTheatreMovieList(theatreId,date,AudienceTypeFilterActivity.Audiencetype).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    if (response.body()!=null ) {
                        JSONObject object = null;
                        try {
                            object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("Status").equals("Success")) {
                                JSONArray ResultsjsonArray = object.getJSONArray("Results");

                                for (int i = 0; i < ResultsjsonArray.length(); i++) {
                                    showTimeMovieTiminginTheater=new ArrayList<>();
                                    JSONObject theatresjsonArrayObject = ResultsjsonArray.getJSONObject(i);
                                    if (theatresjsonArrayObject.getString("showtime").toString().length()>0) {
                                        JSONArray timesjsonArray = theatresjsonArrayObject.getJSONArray("times");


                                        for (int j = 0; j < timesjsonArray.length(); j++) {
                                            JSONObject theatrestimejsonArrayObject = timesjsonArray.getJSONObject(j);
                                            if (AudienceTypeFilterActivity.Audiencetype==0)
                                            {
                                                showTimeMovieTiminginTheater.add
                                                        (new ShowTimeMovieTiminginTheater(theatrestimejsonArrayObject.getString("time"),theatrestimejsonArrayObject.getString("audienceType")
                                                                ,theatresjsonArrayObject.getInt("id"),   theatresjsonArrayObject.getString("name")));
                                            }
                                            else if (AudienceTypeFilterActivity.Audiencetype==1)
                                            {
                                                if (theatrestimejsonArrayObject.getString("audienceType").equals("Male"))
                                                {
                                                    showTimeMovieTiminginTheater.add
                                                            (new ShowTimeMovieTiminginTheater(theatrestimejsonArrayObject.getString("time"),theatrestimejsonArrayObject.getString("audienceType")
                                                                    ,theatresjsonArrayObject.getInt("id"),   theatresjsonArrayObject.getString("name")));
                                                }
                                            }
                                            else if (AudienceTypeFilterActivity.Audiencetype==2)
                                            {
                                                if (theatrestimejsonArrayObject.getString("audienceType").equals("Family"))
                                                {
                                                    showTimeMovieTiminginTheater.add
                                                            (new ShowTimeMovieTiminginTheater(theatrestimejsonArrayObject.getString("time"),theatrestimejsonArrayObject.getString("audienceType")
                                                                    ,theatresjsonArrayObject.getInt("id"),   theatresjsonArrayObject.getString("name")));
                                                }
                                            }


                                        }
                                        Showtimingmap.put(i,showTimeMovieTiminginTheater);

                                    }
                                    theatreBasedMovieList.add(new TheatreBasedMovieList(theatresjsonArrayObject.getInt("id"),
                                            theatresjsonArrayObject.getString("name"),
                                            theatresjsonArrayObject.getString("movie_original_title"), theatresjsonArrayObject.getString("movie_language"),
                                            theatresjsonArrayObject.getString("movie_posterimage"),
                                            theatresjsonArrayObject.getString("movie_backgroundimg"), theatresjsonArrayObject.getString("movie_trailer"),
                                            theatresjsonArrayObject.getString("theatre_id"),
                                            movieCertificate, Showtimingmap,
                                            showTimeMoviefeaturesinTheater));


                                }
                                mAdapter.notifyDataSetChanged();

                            }
                            else
                            {
                                imageNoList.setVisibility(View.VISIBLE);
                                //imageNoList.setImageResource();
                                textNoList.setVisibility(View.VISIBLE);
                                textNoList.setText(object.getString("Message"));
                                recyclerView.setVisibility(View.GONE);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        imageNoList.setVisibility(View.VISIBLE);
                        //imageNoList.setImageResource();
                        textNoList.setVisibility(View.VISIBLE);
                        textNoList.setText(R.string.No_movie);
                        recyclerView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    imageNoList.setVisibility(View.VISIBLE);
                    //imageNoList.setImageResource();
                    textNoList.setVisibility(View.VISIBLE);
                    textNoList.setText(R.string.No_movie);
                    recyclerView.setVisibility(View.GONE);
                }
            });
        }
        else
        {
            imageNoList.setVisibility(View.VISIBLE);
            //imageNoList.setImageResource();
            textNoList.setVisibility(View.VISIBLE);
            textNoList.setText(R.string.no_network);
            recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        theatreBasedMovieList.clear();
        theatredetails(theatreId,formattedDate);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
