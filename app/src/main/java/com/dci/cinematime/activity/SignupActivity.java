package com.dci.cinematime.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.utils.LanguageHelper;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.SignupResponseModel;
import com.dci.cinematime.model.SocialLoginParam;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.JsonElement;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener  {

    TextView text_label_log_in, text_label_sign_skip;
    EditText edit_text_label_your_name, edit_text_label_email, edit_text_label_mobile_number, edit_text_label_password,edit_text_label_confirmpassword;
    Button button_sign_up;
    ProgressDialog progressDialog;
    String username, emailid, mobilenum, passwrd,confirm_password;
    boolean isrefresh;
    TelephonyManager telephonyManager;
    String device_id;
    String device_name;
    String version_name;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    CallbackManager callbackManager;
    Button button_facebook;
    Button sample_gmail;
    GoogleApiClient mGoogleApiClient;
    protected static final int RC_SIGN_IN = 007;
    CountryCodePicker ccp;
    int phoneumberLimit=10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        device_id = telephonyManager.getDeviceId();
        device_name = Build.MODEL;
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version_name = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        button_facebook=(Button)findViewById(R.id.sample_facebook);
        sample_gmail=(Button) findViewById(R.id.sample_gmail);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.enableHint(true);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country country) {
                edit_text_label_mobile_number.setText("");
                ccp.registerPhoneNumberTextView(edit_text_label_mobile_number);
                String result = edit_text_label_mobile_number.getHint().toString().replaceAll("[\\-\\+\\.\\^:,]","");
                phoneumberLimit=result.toString().length();
                edit_text_label_mobile_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(phoneumberLimit)});

            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        //sample_gmail.setSize(SignInButton.SIZE_STANDARD);
        //sample_gmail.setScopes(gso.getScopeArray());
        callbackManager = CallbackManager.Factory.create();
        sample_gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {

                                        try {

                                            signupSocialUser(response.getJSONObject().getString("name"),
                                                    response.getJSONObject().getString("email"),"Facebook",response.getJSONObject().getString("id"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        // Application code
                                        try {
                                            String email = object.getString("email");
                                           // String birthday = object.getString("birthday"); // 01/31/1980 format
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {

                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code

                    }
                });
        initializeView();
        button_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(SignupActivity.this,Arrays.asList(
                        "public_profile", "email"));
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void initializeView() {
        text_label_log_in = findViewById(R.id.text_label_log_in);
        edit_text_label_your_name = findViewById(R.id.edit_text_label_your_name);
        edit_text_label_email = findViewById(R.id.edit_text_label_email);
        text_label_sign_skip = findViewById(R.id.text_label_sign_skip);
        edit_text_label_mobile_number = findViewById(R.id.edit_text_label_mobile_number);
        edit_text_label_password = findViewById(R.id.edit_text_label_password);
        edit_text_label_confirmpassword=findViewById(R.id.edit_text_label_confirmpassword);
        button_sign_up = findViewById(R.id.button_sign_up);
        initializeListener();
        progressDialog = new ProgressDialog(this);
        ccp.registerPhoneNumberTextView(edit_text_label_mobile_number);
        edit_text_label_mobile_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(phoneumberLimit)});


    }

    private void initializeListener() {
        text_label_log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
        text_label_sign_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreference.setSkip(SignupActivity.this, true);
                Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupNullcheck();
            }
        });
    }


    private void signupNullcheck()
    {
        username = edit_text_label_your_name.getText().toString().trim();
        emailid = edit_text_label_email.getText().toString().trim();
        mobilenum = edit_text_label_mobile_number.getText().toString().trim();
        passwrd = edit_text_label_password.getText().toString().trim();
        confirm_password=edit_text_label_confirmpassword.getText().toString().trim();
        if (username.length()<=0) {
            edit_text_label_your_name.setError(getString(R.string.username));

        } else if (!Util.isValidEmailAddress(edit_text_label_email.getText().toString())) {
            edit_text_label_email.setError(getString(R.string.enter_validemail));
            //Toast.makeText(SignupActivity.this, "Enter vaild email", Toast.LENGTH_SHORT).show();
        }  else if (mobilenum.length() < phoneumberLimit) {
            edit_text_label_mobile_number.setError(getString(R.string.enter_mobilenumber));
            //Toast.makeText(SignupActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
        }  else if (passwrd.length() != 6) {
            edit_text_label_password.setError(getString(R.string.enter_valid_password));
            //Toast.makeText(SignupActivity.this, "Please enter your correct password", Toast.LENGTH_SHORT).show();
        }

        else if (!passwrd.equals(confirm_password))
        {

            edit_text_label_confirmpassword.setError(getString(R.string.enter_password_Notmatching));
        }
        else {

            signupUser();
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            //Log.d(TAG, "Got cached sign-in");
           // GoogleSignInResult result = opr.get();
           // handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
           // showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    //hideProgressDialog();
                    //handleSignInResult(googleSignInResult);
                }
            });
        }
    }
    private void signupUser() {
        if (Util.isNetworkAvailable()) {
            showProgress("Please Wait...");

            cinemaTimeAPI.signup(username, emailid, mobilenum, passwrd,Settings.Secure.ANDROID_ID, device_name, device_id, Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName(),version_name).enqueue(new Callback<SignupResponseModel>() {
                @Override
                public void onResponse(Call<SignupResponseModel> call, Response<SignupResponseModel> response) {
                    hideProgress();
                    if (response.body() != null) {
                        SignupResponseModel model = response.body();
                       // UtilsDefault.updateSharedPreference(Constants.Userid,String.valueOf(model.getMovieDetailsReviewResults().getId()));
                        if (model.getStatus().equals("Success")) {
                            Toast.makeText(SignupActivity.this, model.getResults().getActivationMsg(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                        }else{
                            // hideProgress();
                            Toast.makeText(SignupActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }


                @Override
                public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(SignupActivity.this, "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            hideProgress();
            Toast.makeText(this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }




    private void signupSocialUser(String fname,String emailid,String socialtype,String socialid) {
        if (Util.isNetworkAvailable()) {
            showProgress("Please Wait...");
            SocialLoginParam socialLoginParam=new SocialLoginParam();
            socialLoginParam.setUser_fname(fname);
            socialLoginParam.setEmail(emailid);
            socialLoginParam.setApp_version(version_name);
            socialLoginParam.setDevice_id(device_id);
            socialLoginParam.setDevice_imei(Settings.Secure.ANDROID_ID);
            socialLoginParam.setDevice_os( Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName());
            socialLoginParam.setDevice_name(device_name);
            socialLoginParam.setLogintype(socialtype);
            cinemaTimeAPI.loginSocilNetwork(fname,emailid,socialid,socialtype,device_id,device_name,
                    Settings.Secure.ANDROID_ID,Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName(),
                    version_name).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    Log.d("Vigneshwaran","onResponse"+response.body());
                    JSONObject object = null;

                    try {
                        object = new JSONObject(String.valueOf(response.body()));
                        if (object.getString("Status").equals("Success")) {
                            JSONObject resultObject=object.getJSONObject("Results");
                            editor.putInt(Constants.LOGINSTATUS, 1);
                            editor.putInt(Constants.Userid,resultObject.getInt("id"));
                            editor.putString(Constants.USERNAME,resultObject.getString("user_fname"));
                            editor.putString(Constants.EMAIL,resultObject.getString("email"));
                            editor.putString(Constants.PROFILEPHOTO,resultObject.getString("user_profile_img"));
                            editor.putString(Constants.DOB,resultObject.getString("dob"));
                            editor.putString(Constants.GENDER,resultObject.getString("gender"));
                            editor.putString(Constants.MOBILENUMBER,resultObject.getString("phone"));
                            editor.commit();
                            AppPreference.setLoggedIn(SignupActivity.this, true);
                            AppPreference.setSkip(SignupActivity.this, true);
                            Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            Toast.makeText(SignupActivity.this, "success", Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            Toast.makeText(SignupActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Log.d("Vigneshwaran","onFailure");

                }
            });

        } else {
            hideProgress();
            Toast.makeText(this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }




    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        //updateUI(false);
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                       // updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
       // Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            signupSocialUser(acct.getDisplayName(),acct.getEmail(),"Google",acct.getId());




        } else {
            // Signed out, show unauthenticated UI.
           // updateUI(false);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}




