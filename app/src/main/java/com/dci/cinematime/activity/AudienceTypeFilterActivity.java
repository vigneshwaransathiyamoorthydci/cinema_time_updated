package com.dci.cinematime.activity;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.AudienceFilterAdapter;
import com.dci.cinematime.Adapter.LanguageFilterAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.AudienceType;
import com.dci.cinematime.model.AudienceTypeResultItem;
import com.dci.cinematime.model.HomeFilterPopupResponse;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class AudienceTypeFilterActivity extends BaseActivity {

    private List<HomeFilterPopupResponse> filter = new ArrayList<>();
    private List<String> language;
    private RecyclerView recyclerView;
    boolean isrefresh;
    String value,lan;
    int postion;
    ProgressDialog progressDialog;
    private LanguageFilterAdapter mAdapter;
    TextView text_label_popularity, text_label_release_date, text_label_albhabetically, text_label_reset_nowshowing_bottom;
    ImageView image_now_showing_filter_tick, image_now_showing_filter_tick_second, image_now_showing_filter_tick_three;
    ImageView image_now_showing_filter_back_arrow;
    Button button_filter_one_cancel, button_filter_two_apply;
    public static int type=0;
    public ArrayList<Integer> audienceTypeList;
    AudienceType audienceTypeResponse;
    ArrayList<AudienceTypeResultItem> audienceTypeResultItems;
    AudienceFilterAdapter audienceFilterAdapter;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    public static  int Audiencetype=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audiencttype_filter_popup);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        image_now_showing_filter_back_arrow = findViewById(R.id.image_now_showing_filter_back_arrow);
        button_filter_one_cancel = findViewById(R.id.button_filter_one_cancel);
        button_filter_two_apply = findViewById(R.id.button_filter_two_apply);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_now_showing_filter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //popup_language();
        audienceTypeResultItems=new ArrayList<AudienceTypeResultItem>();
        getAudienceType();
        audienceFilterAdapter=new AudienceFilterAdapter(audienceTypeResultItems,getApplicationContext(),this);
        recyclerView.setAdapter(audienceFilterAdapter);

        image_now_showing_filter_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type=0;
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
        button_filter_one_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
        button_filter_two_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(HomeFilterPopupActivity.this, NowShowingFragment.class);
                intent.putExtra("nowshowing",1);

                startActivity(intent);*/

                Fragment fragment = new Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("nowshowing",value);
                bundle.putString("nowshowinglan",lan);
                fragment.setArguments(bundle);

                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);


            }
        });
        text_label_reset_nowshowing_bottom = findViewById(R.id.text_label_reset_nowshowing_bottom);
        text_label_reset_nowshowing_bottom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                View view = recyclerView.getChildAt(postion);
                ImageView imgTick = view.findViewById(R.id.image_now_showing_filter_tick_four);
                imgTick.setVisibility(View.GONE);
                type=0;
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);

               // mAdapter.notifyDataSetChanged();

            }
        });
        text_label_popularity = findViewById(R.id.text_label_popularity);
        image_now_showing_filter_tick = findViewById(R.id.image_now_showing_filter_tick);
        image_now_showing_filter_tick_second = findViewById(R.id.image_now_showing_filter_tick_second);
        image_now_showing_filter_tick_three = findViewById(R.id.image_now_showing_filter_tick_three);

        text_label_popularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="1";

            }
        });
        text_label_release_date = findViewById(R.id.text_label_release_date);
        text_label_release_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="2";

            }
        });
        text_label_albhabetically = findViewById(R.id.text_label_albhabetically);
        text_label_albhabetically.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.VISIBLE);
                value="3";


            }
        });


    }

    private void popup_language() {
        if (Util.isNetworkAvailable()) {
           /* if (!isrefresh){
                progressDialog.setMessage("Please Wait......");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }*/
           showProgress();

            cinemaTimeAPI.getLang().enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    try {
                        language = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(response.body()));
                        JSONArray jsonArray = object.getJSONArray("Results");

// get category JSONObject from mainJSONObj
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject object1 = jsonArray.getJSONObject(i);
                            Iterator<String> iterator = object1.keys();
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                Log.i("TAG", "key:" + key + "--Value::" + object1.optString(key));
                                HomeFilterPopupResponse cl = new HomeFilterPopupResponse();
                                cl.setText_label_english_now_showing_filter(object1.optString(key));
                                filter.add(cl);
                                language.add(key);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

// get all keys from categoryJSONObj

                    if (response.body() != null) {
                        // String model_lang = response.body();

                        if (filter.size() != 0) {
                            mAdapter = new LanguageFilterAdapter(filter, getApplicationContext());
                            recyclerView.setAdapter(mAdapter);
                            mAdapter.setOnClickListen(new LanguageFilterAdapter.AddTouchListen() {
                                @Override
                                public void onTouchClick(int position) {
                                    postion=position;
                                    Log.d(TAG, "onTouchClick: " + position);

                                    //selectedlanguage=language.get(position);
                                        Log.e("filter", "language" + language.get(position));
                                        //startActivity(new Intent(HomeFilterPopupActivity.this, TheatreDetailsActivity.class));

                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(AudienceTypeFilterActivity.this, "failure", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            hideProgress();
            Toast.makeText(AudienceTypeFilterActivity.this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void getAudienceType()
    {
        if (Util.isNetworkAvailable())
        {
            //audienceTypeList.clear();
            showProgress();
            audienceTypeResultItems.clear();
            audienceTypeResultItems.add(new AudienceTypeResultItem(0,getString(R.string.all_audience),1));
            cinemaTimeAPI.getAudiencetype().enqueue(new Callback<AudienceType>() {
                @Override
                public void onResponse(Call<AudienceType> call, Response<AudienceType> response) {
                    hideProgress();
                    audienceTypeResponse=response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        for (int u=0;u<audienceTypeResponse.getResults().size();u++)
                        {
                            audienceTypeResultItems.add(new AudienceTypeResultItem(audienceTypeResponse.getResults().get(u).getId(),
                                    audienceTypeResponse.getResults().get(u).getAudience(),
                                    audienceTypeResponse.getResults().get(u).getStatus()));
                            //audienceTypeList.add(audienceTypeResponse.getResults().get(u).getId());

                        }
                        audienceFilterAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(AudienceTypeFilterActivity.this, "On error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AudienceType> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(AudienceTypeFilterActivity.this, "On FAilure", Toast.LENGTH_SHORT).show();

                }
            });
        }
        else
        {
            Toast.makeText(AudienceTypeFilterActivity.this, "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
