package com.dci.cinematime.activity;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.CastRelatedMovieAdapter;
import com.dci.cinematime.Adapter.LanguageFilterAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.CastItem;
import com.dci.cinematime.model.CastRelatedMovieDetails;
import com.dci.cinematime.model.HomeFilterPopupResponse;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class CaseDetailsActivity extends BaseActivity {

    private List<HomeFilterPopupResponse> filter = new ArrayList<>();
    private List<String> language;
    private RecyclerView recyclerView;
    boolean isrefresh;
    String value,lan;
    int postion;
    ProgressDialog progressDialog;
    private LanguageFilterAdapter mAdapter;
    TextView text_label_popularity, text_label_release_date, text_label_albhabetically, text_label_reset_nowshowing_bottom;
    TextView textActorTitle,textActorName,textActorRole,textActorGender,textActorDob,textActorBio;
    ImageView image_now_showing_filter_back_arrow,imageActorProfile;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    int castid;
    String castName;
    public static ArrayList<CastRelatedMovieDetails> Castmoviedetails;
    CastRelatedMovieAdapter castRelatedMovieAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cast_details);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        Castmoviedetails=new ArrayList<CastRelatedMovieDetails>();
        image_now_showing_filter_back_arrow = findViewById(R.id.image_now_showing_filter_back_arrow);
        imageActorProfile=(ImageView)findViewById(R.id.profile);
        textActorTitle=(TextView)findViewById(R.id.text_label_actorname);
        textActorName=(TextView)findViewById(R.id.text_actorname);
        textActorRole=(TextView)findViewById(R.id.text_actorrole);
        textActorGender=(TextView)findViewById(R.id.text_actorgender);
        textActorDob=(TextView)findViewById(R.id.text_actordob);
        textActorBio=(TextView)findViewById(R.id.text_label_actorbio);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_related_movie);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        castRelatedMovieAdapter=new CastRelatedMovieAdapter(getApplicationContext(),Castmoviedetails,"");
        recyclerView.setAdapter(castRelatedMovieAdapter);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            castid = bundle.getIntExtra("castid", 0);
            castName=bundle.getStringExtra("castname");

        }
        textActorTitle.setText(castName);
        getcastDetails(castid);

        image_now_showing_filter_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });



    }

    private void getcastDetails(int id) {
        if (Util.isNetworkAvailable()) {

           showProgress();

            cinemaTimeAPI.getCastDetails(id).enqueue(new Callback<JsonElement>() {

                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    if (response.body() != null) {
                        try {
                            JSONObject object = new JSONObject(String.valueOf(response.body()));
                            JSONObject resultsObject = object.getJSONObject("Results");
                            textActorName.setText(resultsObject.getString("first_name")+" "+resultsObject.getString("last_name"));
                            textActorRole.setText(resultsObject.getString("role"));
                            textActorGender.setText(resultsObject.getString("gender"));
                            boolean  validDobb = resultsObject.getString("dob").startsWith("1000");
                            if (!validDobb) {
                                textActorDob.setText(parseDateToddMMyyyy(resultsObject.getString("dob")));
                            }
                            if (resultsObject.getString("bio").equals("Details not available")) {
                                textActorBio.setVisibility(View.GONE);
                            }
                            else
                            {
                                textActorBio.setText(resultsObject.getString("bio"));

                            }
                            makeTextViewResizable(textActorBio, 2, "View More", true);
                            Picasso.with(getApplicationContext()).
                                    load(getString(R.string.coming_soon_image_base_url)
                                            +resultsObject.getString("image")).placeholder(R.mipmap.icon_pro_image_loading_256).
                                    into(imageActorProfile);
                            JSONArray relatedMoviesArray=resultsObject.getJSONArray("relatedMovies");
                            for (int y=0;y<relatedMoviesArray.length();y++)
                            {
                                JSONObject relatedMoviesObject = relatedMoviesArray.getJSONObject(y);
                                CastRelatedMovieDetails castRelatedMovieDetails=new CastRelatedMovieDetails();
                                castRelatedMovieDetails.setId(relatedMoviesObject.getInt("id"));
                                castRelatedMovieDetails.setReleaseType(relatedMoviesObject.getInt("releaseType"));
                                castRelatedMovieDetails.setMovie_name(relatedMoviesObject.getString("movie_name"));
                                castRelatedMovieDetails.setMovie_original_title(relatedMoviesObject.getString("movie_original_title"));
                                castRelatedMovieDetails.setMovie_backgroundimg(relatedMoviesObject.getString("movie_backgroundimg"));
                                castRelatedMovieDetails.setMovie_posterimage(relatedMoviesObject.getString("movie_posterimage"));
                                castRelatedMovieDetails.setMoviePosterImageBaseUrl(resultsObject.getString("moviePosterImageBaseUrl"));
                                Castmoviedetails.add(castRelatedMovieDetails);

                            }

                            castRelatedMovieAdapter.notifyDataSetChanged();



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(CaseDetailsActivity.this, "failure", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(CaseDetailsActivity.this, "failure", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            hideProgress();
            Toast.makeText(CaseDetailsActivity.this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }
    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "MMM-dd-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(
                        addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE);
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        makeTextViewResizable(tv, 3, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

}
