package com.dci.cinematime.activity;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.dci.cinematime.Adapter.LanguageFilterAdapter;

import com.dci.cinematime.R;

import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.HomeFilterPopupResponse;

import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class HomeFilterPopupActivity extends BaseActivity {

    private List<HomeFilterPopupResponse> filter = new ArrayList<>();
    private List<String> language;
    private RecyclerView recyclerView;
    boolean isrefresh;
    String value,lan;
    int postion;
    ProgressDialog progressDialog;
    private LanguageFilterAdapter mAdapter;
    TextView text_label_popularity, text_label_release_date, text_label_albhabetically, text_label_reset_nowshowing_bottom;
    ImageView image_now_showing_filter_tick, image_now_showing_filter_tick_second, image_now_showing_filter_tick_three;
    ImageView image_now_showing_filter_back_arrow;
    Button button_filter_one_cancel, button_filter_two_apply;
    public static int sortby=1;
    public static boolean nowshowingFilter=false;
    public static String selectedlanguage=" ";
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    LinearLayoutManager linearLayoutManager;
    FrameLayout framepopulaty,frameRelease,framealbha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_filter_popuptest);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        image_now_showing_filter_back_arrow = findViewById(R.id.image_now_showing_filter_back_arrow);
        //button_filter_one_cancel = findViewById(R.id.button_filter_one_cancel);
        button_filter_two_apply = findViewById(R.id.button_filter_two_apply);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_now_showing_filter);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        popup_language();
        framepopulaty=(FrameLayout)findViewById(R.id.framepopulaty);
        frameRelease=(FrameLayout)findViewById(R.id.frameRelease);
        framealbha=(FrameLayout)findViewById(R.id.framealbha);

        image_now_showing_filter_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortby=1;
                selectedlanguage="";
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
        });
//        button_filter_one_cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                nowshowingFilter=false;
//                onBackPressed();
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//
//            }
//        });
        button_filter_two_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(HomeFilterPopupActivity.this, NowShowingFragment.class);
                intent.putExtra("nowshowing",1);

                startActivity(intent);*/
                nowshowingFilter=true;
                Fragment fragment = new Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("nowshowing",value);
                bundle.putString("nowshowinglan",lan);
                fragment.setArguments(bundle);

                onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);


            }
        });
        text_label_reset_nowshowing_bottom = findViewById(R.id.text_label_reset_nowshowing_bottom);
        text_label_reset_nowshowing_bottom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                View view = recyclerView.getChildAt(postion);
                ImageView imgTick = view.findViewById(R.id.image_now_showing_filter_tick_four);
                imgTick.setVisibility(View.GONE);
                sortby=1;
                selectedlanguage="";
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);

               // mAdapter.notifyDataSetChanged();

            }
        });
        text_label_popularity = findViewById(R.id.text_label_popularity);
        image_now_showing_filter_tick = findViewById(R.id.image_now_showing_filter_tick);
        image_now_showing_filter_tick_second = findViewById(R.id.image_now_showing_filter_tick_second);
        image_now_showing_filter_tick_three = findViewById(R.id.image_now_showing_filter_tick_three);

        text_label_popularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="1";
                sortby=1;
            }
        });
        text_label_release_date = findViewById(R.id.text_label_release_date);
        text_label_release_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="2";
                sortby=2;
            }
        });
        text_label_albhabetically = findViewById(R.id.text_label_albhabetically);
        text_label_albhabetically.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.VISIBLE);
                value="3";
                sortby=3;

            }
        });

        framepopulaty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="1";
                sortby=1;
            }
        });
        frameRelease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.VISIBLE);
                image_now_showing_filter_tick_three.setVisibility(View.GONE);
                value="2";
                sortby=2;
            }
        });
        framealbha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_now_showing_filter_tick.setVisibility(View.GONE);
                image_now_showing_filter_tick_second.setVisibility(View.GONE);
                image_now_showing_filter_tick_three.setVisibility(View.VISIBLE);
                value="3";
                sortby=3;
            }
        });


    }

    private void popup_language() {
        if (Util.isNetworkAvailable()) {
           /* if (!isrefresh){
                progressDialog.setMessage("Please Wait......");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }*/
           showProgress();

            cinemaTimeAPI.getLang().enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    try {
                        language = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(response.body()));
                        if (object.getString("Status").equals("Success")) {
                            JSONArray jsonArray = object.getJSONArray("Results");

// get category JSONObject from mainJSONObj
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object1 = jsonArray.getJSONObject(i);
                                Iterator<String> iterator = object1.keys();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    Log.i("TAG", "key:" + key + "--Value::" + object1.optString(key));
                                    HomeFilterPopupResponse cl = new HomeFilterPopupResponse();
                                    cl.setText_label_english_now_showing_filter(object1.optString(key));
                                    filter.add(cl);
                                    language.add(key);


                                }
                            }
                        }
                        else
                        {
                            Toast.makeText(HomeFilterPopupActivity.this, object.getString("Message"), Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

// get all keys from categoryJSONObj

                    if (response.body() != null) {
                        // String model_lang = response.body();

                        if (filter.size() != 0) {
                            mAdapter = new LanguageFilterAdapter(filter, getApplicationContext());
                            recyclerView.setAdapter(mAdapter);
                            mAdapter.setOnClickListen(new LanguageFilterAdapter.AddTouchListen() {
                                @Override
                                public void onTouchClick(int position) {
                                    postion=position;
                                    Log.d(TAG, "onTouchClick: " + position);

                                    selectedlanguage=language.get(position);
                                        Log.e("filter", "language" + language.get(position));
                                        //startActivity(new Intent(HomeFilterPopupActivity.this, TheatreDetailsActivity.class));

                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(HomeFilterPopupActivity.this, "failure", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            hideProgress();
            Toast.makeText(HomeFilterPopupActivity.this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
