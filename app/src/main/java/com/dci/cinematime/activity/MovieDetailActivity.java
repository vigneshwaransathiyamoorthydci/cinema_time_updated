package com.dci.cinematime.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.CategoryListAdpater;
import com.dci.cinematime.Adapter.TheatersAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.fragment.DescriptionFragment;
import com.dci.cinematime.fragment.ReviewFragment;
import com.dci.cinematime.fragment.ShowtimeFragment;
import com.dci.cinematime.model.CastItem;
import com.dci.cinematime.model.CategoryItem;
import com.dci.cinematime.model.CrewItem;
import com.dci.cinematime.model.LikeanddislikeResponse;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailsResponse;
import com.dci.cinematime.model.MoviesItem;
import com.dci.cinematime.model.TheatersList;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.ShoppingApplication;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by harini on 7/25/2018.
 */

public class MovieDetailActivity extends BaseActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView playicon;
    RelativeLayout back,calendar_view;
    GridView categoryGridview;
    MovieDetailsResponse movieDetail;
    public static  int movieId;
    int releaseType;
    TextView moviename;
    TextView percentage;
    TextView movieduration;
    TextView views;
    TextView language;
    ImageView posterImage;
    public static String description="test";
    public static ArrayList<CastItem> moviedetailsCast;
    public static ArrayList<CrewItem> moviedetailsCrew;
    public static ArrayList<CategoryItem> moviedetailsCategory;
    CategoryListAdpater categoryListAdpater;
    String videoID;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    ImageView like_imageview;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    LikeanddislikeResponse likeanddislikeResponse;
    int likecount;
    public static  String castBaseUrl;
    NestedScrollView scrollView;
    private int dragThreshold = 10;
    int downX = 0;
    int downY = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detailtest);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        scrollView=(NestedScrollView)findViewById(R.id.scrolview);
        moviename=(TextView)findViewById(R.id.moviename);
        percentage=(TextView)findViewById(R.id.percentage);
        movieduration=(TextView)findViewById(R.id.movieduration);
        posterImage=(ImageView)findViewById(R.id.movie_image);
        views=(TextView)findViewById(R.id.views);
        language=(TextView)findViewById(R.id.language);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        categoryGridview=(GridView)findViewById(R.id.categoryGridview);
        like_imageview=(ImageView)findViewById(R.id.like_imageview);
        moviedetailsCast=new ArrayList<CastItem>();
        moviedetailsCrew=new ArrayList<CrewItem>();
        moviedetailsCategory=new ArrayList<CategoryItem>();
        playicon = (ImageView) findViewById(R.id.playicon);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        back = findViewById(R.id.back);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            movieId = bundle.getIntExtra("id", 0);
            releaseType=bundle.getIntExtra("releaseType",0);

        }
        playicon.setVisibility(View.GONE);
        getMoviedetail();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        playicon.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent i = new Intent(MovieDetailActivity.this, YoutubePlayerActivity.class);
                i.putExtra("videoid", videoID);
                startActivity(i);



            }
        });
        like_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getInt(Constants.Userid,0)!=0) {
                    if (Util.isNetworkAvailable()) {
                        //addTouchListen.onNotifyTouch(position);
                        showProgress();
                        cinemaTimeAPI.setMovielikeandDislike(movieId,sharedPreferences.getInt(Constants.Userid,0),likecount).
                                enqueue(new Callback<LikeanddislikeResponse>() {
                                    @Override
                                    public void onResponse(Call<LikeanddislikeResponse> call, Response<LikeanddislikeResponse> response) {
                                        hideProgress();

                                        if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                                            likeanddislikeResponse=response.body();
                                            if (likeanddislikeResponse.getResults().getLikeCount().equals("1"))
                                            {
                                             like_imageview.setImageResource(R.drawable.heart);
                                                like_imageview.setBackgroundResource(R.color.white);
                                                likecount=0;

                                            }
                                            else
                                            {
                                                like_imageview.setImageResource(R.mipmap.white_heartcopy);
                                                //like_imageview.setBackgroundResource(R.color.light_grey);
                                                likecount=1;

                                            }
                                        }
                                        else
                                        {
                                            Toast.makeText(MovieDetailActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<LikeanddislikeResponse> call, Throwable t) {
                                        hideProgress();
                                        Toast.makeText(MovieDetailActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                                    }
                                });

                    } else {
                        Toast.makeText(MovieDetailActivity.this, "No Internet", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(MovieDetailActivity.this, "Login to continue", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(MovieDetailActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (releaseType==2) {
            adapter.addFragment(new DescriptionFragment(), "Description");

        }
        else
        {
            adapter.addFragment(new ShowtimeFragment(), "Show Times");
            adapter.addFragment(new DescriptionFragment(), "Description");
            adapter.addFragment(new ReviewFragment(), "  Review  ");
        }

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            title.toLowerCase();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getMoviedetail() {
        if (Util.isNetworkAvailable()) {

            //  swip_lay.setRefreshing(true);
            showProgress();

            cinemaTimeAPI.getmoviesDetail(movieId,sharedPreferences.getInt(Constants.Userid,0)).enqueue(new Callback<JsonElement>() {
               @Override
               public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                   hideProgress();


                   if (response.body() != null) {
                       try {
                           JSONObject object = new JSONObject(String.valueOf(response.body()));
                           if (object.getString("Status").equals("Success"))
                           {
                           JSONObject jsonArray = object.getJSONObject("Results");
                           for (int i = 0; i < 1; i++) {
                               moviename.setText(jsonArray.getString("name"));
                               percentage.setText(jsonArray.getString("movie_vote_average") + " " + "%");
                               castBaseUrl = jsonArray.getString("castBaseUrl");
                               int duration = jsonArray.getInt("movie_length");
                               if (duration > 0) {
                                   int hours = duration / 60; //since both are ints, you get an int
                                   int minutes = duration % 60;
                                   movieduration.setText("Duration: " + hours + " " + "hr" + " " + minutes + "mins");
                               }


                               description = jsonArray.getString("movie_description");
                               NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
                               DecimalFormat df = (DecimalFormat) nf;
                               df.applyPattern("#,###");
                               int totalViews = jsonArray.getInt("movie_vote_count");
                               if (LanguageHelper.getLanguage(getApplicationContext()).toString().equals("en")) {
                                   views.setText(df.format(totalViews) + " " + "Views");
                               } else {
                                   views.setText("Views" + " " + df.format(totalViews));
                               }


                               videoID = jsonArray.getString("movie_trailer");
                               if (videoID.length() > 0) {
                                   playicon.setVisibility(View.VISIBLE);
                               }


                                    Picasso.with(getApplicationContext()).
                                            load(getApplicationContext().getString(R.string.coming_soon_image_base_url)
                                                    + jsonArray.getString("movie_backgroundimg")).placeholder(R.drawable.default_movie).
                                            into(posterImage);

//                                else if (videoID.length()>0)
//                                {
//                                    String splitted[] =videoID.split(",",2);
//                                    Picasso.with(getApplicationContext()).
//                                            load("http://img.youtube.com/vi/"+splitted+"/0.jpg").placeholder(R.drawable.default_movie).
//                                            into(posterImage);
//                                }

                               if (jsonArray.getString("movie_casts").toString().length() > 0) {
                                   JSONArray castarray = jsonArray.getJSONArray("cast");


                                   for (int j = 0; j < castarray.length(); j++) {
                                       JSONObject moviesListcastObject = castarray.getJSONObject(j);
                                       CastItem castItem = new CastItem();
                                       castItem.setId(moviesListcastObject.getInt("id"));
                                       castItem.setCrew_fname(moviesListcastObject.getString("crew_fname"));
                                       castItem.setCrew_lname(moviesListcastObject.getString("crew_lname"));
                                       castItem.setCrew_image(moviesListcastObject.getString("crew_image"));
                                       castItem.setCrew_role(moviesListcastObject.getString("crew_role"));
                                       castItem.setUrl(castBaseUrl);
                                       moviedetailsCast.add(castItem);
                                   }
                               }


                               if (jsonArray.getString("movie_crews").toString().length() > 0) {
                                   JSONArray crewarray = jsonArray.getJSONArray("crew");
                                   for (int j = 0; j < crewarray.length(); j++) {
                                       JSONObject moviesListCrewObject = crewarray.getJSONObject(j);
                                       CrewItem crewItem = new CrewItem();
                                       crewItem.setId(moviesListCrewObject.getInt("id"));
                                       crewItem.setName(moviesListCrewObject.getString("name"));
                                       crewItem.setRole(moviesListCrewObject.getString("role"));
                                       crewItem.setImage(moviesListCrewObject.getString("image"));
                                       moviedetailsCrew.add(crewItem);
                                   }
                               }
                               JSONObject movie_certificateobject = jsonArray.getJSONObject("movie_certificate");

                               if (movie_certificateobject.has("movie_certificate")) {
                                   language.setText(jsonArray.getString("movie_language") + " " + movie_certificateobject.getString("movie_certificate"));
                               } else {
                                   language.setText(jsonArray.getString("movie_language"));
                               }


                               int liked = jsonArray.getInt("movie_liked_by_user");
                               ;
                               if (liked == 1) {
                                   like_imageview.setImageResource(R.drawable.heart);
                                   likecount = 0;
                               } else {
                                   //like_imageview.setBackgroundResource(R.color.light_grey);
                                   like_imageview.setImageResource(R.mipmap.white_heartcopy);
                                   likecount = 1;
                               }

                               if (jsonArray.getString("movie_category").toString().length() > 0) {
                                   JSONArray categoryarray = jsonArray.getJSONArray("category");

                                   for (int j = 0; j < categoryarray.length(); j++) {
                                       JSONObject moviesListcategoryObject = categoryarray.getJSONObject(j);
                                       CategoryItem categoryItem = new CategoryItem();
                                       categoryItem.setId(moviesListcategoryObject.getInt("id"));
                                       categoryItem.setStatus(moviesListcategoryObject.getInt("status"));
                                       categoryItem.setCategoryName(moviesListcategoryObject.getString("category_name"));
                                       categoryItem.setCategoryTmdbId(moviesListcategoryObject.getInt("category_tmdb_id"));
                                       moviedetailsCategory.add(categoryItem);

                                   }
                                   // categoryListAdpater=new CategoryListAdpater(moviedetailsCategory,getApplicationContext());
                                   //categoryGridview.setAdapter(categoryListAdpater);
                               } else {
                                   //categoryGridview.setVisibility(View.GONE);
                                   CategoryItem categoryItem = new CategoryItem();
                                   categoryItem.setId(0);
                                   categoryItem.setStatus(0);
                                   categoryItem.setCategoryName("N/A");
                                   categoryItem.setCategoryTmdbId(0);
                                   moviedetailsCategory.add(categoryItem);
                               }

                               categoryListAdpater = new CategoryListAdpater(moviedetailsCategory, getApplicationContext());
                               categoryGridview.setAdapter(categoryListAdpater);
                           }
                           }
                           else
                           {
                               Toast.makeText(getApplicationContext(),object.getString("Message"),Toast.LENGTH_SHORT).show();
                           }
                       } catch (JSONException e) {
                           e.printStackTrace();
                           Toast.makeText(getApplicationContext(),"No Data Found",Toast.LENGTH_SHORT).show();
                           onBackPressed();
                       }
//                       moviename.setText(movieDetail.getMovieDetailsReviewResults().getName());
//                        percentage.setText(movieDetail.getMovieDetailsReviewResults().getMovie_vote_average());
//                        movieduration.setText("Duration: "+movieDetail.getMovieDetailsReviewResults().getMovie_length()+" Min");
//                        language.setText(movieDetail.getMovieDetailsReviewResults().getMovie_language());
//                        description=movieDetail.getMovieDetailsReviewResults().getMovie_description();
//                        for (int i=0;i<movieDetail.getMovieDetailsReviewResults().getCast().size();i++)
//                        {
//                            moviedetailsCast.add(new CastItem(movieDetail.getMovieDetailsReviewResults().getCast().get(i).getId(),movieDetail.getMovieDetailsReviewResults().getCast().get(i).getCrew_fname(),
//                                    movieDetail.getMovieDetailsReviewResults().getCast().get(i).getCrew_lname(),movieDetail.getMovieDetailsReviewResults().getCast().get(i).getCrew_image()));
//                        }
//                       for (int i=0;i<movieDetail.getMovieDetailsReviewResults().getCrew().size();i++)
//                       {
//                           moviedetailsCrew.add(new CrewItem(movieDetail.getMovieDetailsReviewResults().getCrew().get(i).getRole(),movieDetail.getMovieDetailsReviewResults().getCrew().get(i).getName(),
//                                   movieDetail.getMovieDetailsReviewResults().getCrew().get(i).getId()));
//                       }
                        setupViewPager(viewPager);
                   }
                   else
                   {
                       Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                       onBackPressed();
                   }
               }

               @Override
               public void onFailure(Call<JsonElement> call, Throwable t) {
                hideProgress();
                   Toast.makeText(getApplicationContext(), "onFailure", Toast.LENGTH_SHORT).show();
                   onBackPressed();
               }
           });
        } else {
            Toast.makeText(this, "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

}
