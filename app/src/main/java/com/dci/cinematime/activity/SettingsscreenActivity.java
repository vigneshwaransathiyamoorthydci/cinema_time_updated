package com.dci.cinematime.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.dci.cinematime.R;
import com.dci.cinematime.fragment.AboutFragment;
import com.dci.cinematime.fragment.HelpandFeedbackFragment;
import com.dci.cinematime.fragment.LanguageFragment;
import com.dci.cinematime.fragment.NotificationFragment;
import com.dci.cinematime.fragment.PrivacyPolicyFragment;
import com.dci.cinematime.fragment.SearchFragment;
import com.dci.cinematime.fragment.UserProfileFragment;
import com.dci.cinematime.utils.LanguageHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsscreenActivity extends BaseActivity
{
    Bundle bundle;
    String Bundlename;
    FrameLayout frameLoginContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settingscreen);
        //ButterKnife.bind(this,contentView);
        frameLoginContainer=(FrameLayout)findViewById(R.id.setting_frame);

        bundle = new Bundle();
        bundle = getIntent().getExtras();
        Bundlename = bundle.getString("name");
        if (Bundlename.equals("userprofile"))
        {

            setFragment(new UserProfileFragment());
        }
        else if (Bundlename.equals("policy"))
        {
            setFragment(new PrivacyPolicyFragment());
        }
        else if (Bundlename.equals("about"))
        {
            setFragment(new AboutFragment());
        }
        else if (Bundlename.equals("notification"))
        {
            setFragment(new NotificationFragment());
        }

        else if (Bundlename.equals("help"))
        {
            setFragment(new HelpandFeedbackFragment());
        }
        else if (Bundlename.equals("language"))
        {
            setFragment(new LanguageFragment());
        }

    }

    public void setFragment(android.support.v4.app.Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.setting_frame, fragment, "About Us");
            fragmentTransaction.commit();
        }
    }

    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.setting_frame, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.setting_frame, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.setting_frame, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.setting_frame, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
