package com.dci.cinematime.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.LoginResponseModel;
import com.dci.cinematime.model.SocialLoginParam;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Locale;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    /*@BindView(R.id.image_login_pop_corn)
    ImageView imageLoginPopCorn;
    @BindView(R.id.text_label_login_skip)
    TextView textLabelLoginSkip;
    @BindView(R.id.text_label_glad_to_see_you)
    TextView textLabelGladToSeeYou;
    @BindView(R.id.edit_text_label_login_user_name)
    EditText editTextLabelLoginUserName;
    @BindView(R.id.edit_text_label_login_password)
    EditText editTextLabelLoginPassword;
    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.text_label_forgot_user_name_password)
    TextView textLabelForgotUserNamePassword;
    @BindView(R.id.edit_text_login_or_connect_with)
    TextView editTextLoginOrConnectWith;
    @BindView(R.id.button_login_facebook)
    Button buttonLoginFacebook;
    @BindView(R.id.button_login_gmail)
    Button buttonLoginGmail;
    @BindView(R.id.text_label_login_dont_have_an_account)
    TextView textLabelLoginDontHaveAnAccount;
    @BindView(R.id.text_label_sign_up)*/
    TextView textLabelSignUp, textLabelLoginSkip;
    TextView text_label_forgot_user_name_password, text_label_sign_up;
    AlertDialog alertDialog;
    Button buttonLogin, buttonLoginFacebook;
    EditText editTextLabelLoginUserName, editTextLabelLoginPassword;
    String username_login, username_paswd;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    TelephonyManager telephonyManager;
    String device_id="1000";
    String device_name;
    String version_name;
    CallbackManager callbackManager;
    Button button_login_gmail;
    GoogleApiClient mGoogleApiClient;
    protected static final int RC_SIGN_IN = 007;
    public  static boolean active = false;
    //String language = "en";
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //ButterKnife.bind(this);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
       // LanguageHelper.setLanguage(getApplicationContext(), language);

        Log.d("VIKIS","locale"+Locale.getDefault().getDisplayLanguage());
        FacebookSdk.sdkInitialize(getApplicationContext());

        AppEventsLogger.activateApp(this,getString(R.string.facebook_app_id));
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {

        }
        catch (NoSuchAlgorithmException e) {

        }
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        editTextLabelLoginUserName = findViewById(R.id.edit_text_label_login_user_name);
        editTextLabelLoginPassword = findViewById(R.id.edit_text_label_login_password);
        //editTextLabelLoginPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }

//        device_id = telephonyManager.getDeviceId();
        device_name = Build.MODEL;
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version_name = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        textLabelLoginSkip = findViewById(R.id.text_label_login_skip);
        textLabelLoginSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreference.setSkip(LoginActivity.this, true);
                Intent intent_skip = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent_skip);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
        text_label_forgot_user_name_password = findViewById(R.id.text_label_forgot_user_name_password);
        text_label_forgot_user_name_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        button_login_gmail=findViewById(R.id.button_login_gmail);
        button_login_gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        buttonLogin = findViewById(R.id.button_login);
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {

                                        try {

                                            signupSocialUser(response.getJSONObject().getString("name"),
                                                    response.getJSONObject().getString("email"),"Facebook",response.getJSONObject().getString("id"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        // Application code
                                        try {
                                            String email = object.getString("email");
//                                            String birthday = object.getString("birthday"); // 01/31/1980 format
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {

                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code

                    }
                });
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginNullcheck();
            }
        });
        buttonLoginFacebook = findViewById(R.id.button_login_facebook);

        buttonLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList(
                        "public_profile", "email"));

            }
        });


        text_label_sign_up = findViewById(R.id.text_label_sign_up);
        text_label_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
            }
        });

    }

    private void loginNullcheck()
    {
        String username_login = editTextLabelLoginUserName.getText().toString().trim();
        String username_paswd = editTextLabelLoginPassword.getText().toString().trim();
        if (username_login.equals("")) {
            editTextLabelLoginUserName.setError(getString(R.string.enter_username));
            //Toast.makeText(LoginActivity.this, "Please enter your username/email", Toast.LENGTH_SHORT).show();
        } else if (username_paswd.equals("")) {
            editTextLabelLoginPassword.setError(getString(R.string.enter_password));
            //Toast.makeText(LoginActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
        } else if (!Util.isValidEmailAddress(editTextLabelLoginUserName.getText().toString()))
        {
            editTextLabelLoginUserName.setError(getString(R.string.enter_validemail));
        } else if (username_paswd.length() !=6) {
            editTextLabelLoginPassword.setError(getString(R.string.enter_valid_password));
            //Toast.makeText(LoginActivity.this, "Please enter your correct password", Toast.LENGTH_SHORT).show();
        } else {

            userLogin();
        }
    }



    public void Dialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.threwnttousefa);
        alertDialogBuilder.setPositiveButton((getString(R.string.continu)),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("text/plain");
                        shareAppLinkViaFacebook("https://www.facebook.com");
                        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        share.putExtra(Intent.EXTRA_SUBJECT, "Cinima Time");
                        share.putExtra(Intent.EXTRA_TEXT, "Invite your friends to CinemaTime via http://www.dotcominfoway.com");
                        startActivity(Intent.createChooser(share, "Invite Via"));
                    }
                });
        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                //finish();
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void shareAppLinkViaFacebook(String urlToShare) {
        try {
            Intent intent1 = new Intent();
            intent1.setClassName("com.facebook.katana", "com.facebook.katana.activity.composer.ImplicitShareIntentHandler");
            intent1.setAction("android.intent.action.SEND");
            intent1.setType("text/plain");
            intent1.putExtra("android.intent.extra.TEXT", urlToShare);
            startActivity(intent1);
        } catch (Exception e) {
            // If we failed (not native FB app installed), try share through SEND
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
            startActivity(intent);

        }
    }

    private void userLogin() {

        if (Util.isNetworkAvailable()) {
            showProgress("Please Wait.....");
            username_login = editTextLabelLoginUserName.getText().toString();
            username_paswd = editTextLabelLoginPassword.getText().toString();

             cinemaTimeAPI.login(username_login, username_paswd, Settings.Secure.ANDROID_ID, device_name, device_id, Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName(),version_name).enqueue(new Callback<LoginResponseModel>() {
                @Override
                public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                    hideProgress();

                    if (response.body() != null) {
                        LoginResponseModel model = response.body();
                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(model);
                        if (model.getStatus().equals("Success")) {
                            editor.putInt(Constants.LOGINSTATUS, 1);
                            editor.putInt(Constants.Userid,model.getResults().getId());
                            editor.putString(Constants.USERNAME,model.getResults().getUser_fname());
                            editor.putString(Constants.MOBILENUMBER,model.getResults().getPhone());
                            editor.putString(Constants.EMAIL,model.getResults().getEmail());
                            editor.putString(Constants.PASSWORD,model.getResults().getPassword());
                            editor.putString(Constants.PROFILEPHOTO,model.getResults().getUser_profile_img());
                            editor.putString(Constants.DOB,model.getResults().getDob());
                            editor.putString(Constants.GENDER,model.getResults().getGender());
                            editor.commit();
                            AppPreference.setLoggedIn(LoginActivity.this, true);
                            AppPreference.setSkip(LoginActivity.this, true);
                            Intent intent_new = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent_new);
                            // UtilsDefault.updateSharedPreference(Constants.Userid,String.valueOf(model.getMovieDetailsReviewResults().getId()));
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            finish();
                           // Toast.makeText(LoginActivity.this, "success", Toast.LENGTH_SHORT).show();
                        }else{
                           // hideProgress();
                            Toast.makeText(LoginActivity.this, model.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(LoginActivity.this, "Email id does not exist", Toast.LENGTH_SHORT).show();
                }
            });
//            cinemaTimeAPI.userlogin(username_login, username_paswd, Settings.Secure.ANDROID_ID, device_name, device_id, Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName(),version_name).enqueue(new Callback<JsonElement>() {
//                @Override
//                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
//                    hideProgress();
//                    if (response.body() != null) {
//                        try {
//                            JSONObject object = new JSONObject(String.valueOf(response.body()));
//                            JSONArray jsonArray = object.getJSONArray("Results");
//                            if (object.getString("Status").equals("Success"))
//                            {
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject userobject = jsonArray.getJSONObject(i);
//
//                                    editor.putInt(Constants.LOGINSTATUS, 1);
//                            editor.putInt(Constants.Userid,userobject.getInt("id"));
//                            editor.putString(Constants.USERNAME,userobject.getString("user_fname"));
//                            editor.putString(Constants.MOBILENUMBER,userobject.getString("phone"));
//                            editor.putString(Constants.EMAIL,userobject.getString("email"));
//                            //editor.putString(Constants.PASSWORD,model.getResults().getPassword());
//                            editor.commit();
//                            AppPreference.setLoggedIn(LoginActivity.this, true);
//                            Intent intent_new = new Intent(LoginActivity.this, HomeActivity.class);
//                            startActivity(intent_new);
//                            // UtilsDefault.updateSharedPreference(Constants.Userid,String.valueOf(model.getMovieDetailsReviewResults().getId()));
//                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                            finish();
//
//                                }
//                            }
//                            else
//                            {
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    String value=jsonArray.getString(i);
//                                    Toast.makeText(LoginActivity.this, value, Toast.LENGTH_SHORT).show();
//
//                                }
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                    else
//                    {
//                        Toast.makeText(LoginActivity.this, "failed", Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<JsonElement> call, Throwable t) {
//                    hideProgress();
//                    Toast.makeText(LoginActivity.this, "failed", Toast.LENGTH_SHORT).show();
//                }
//            });
        } else {
            hideProgress();
            Toast.makeText(this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();

        }

    }

    private void signupSocialUser(String fname,String emailid,String socialtype,String socialid) {
        if (Util.isNetworkAvailable()) {
            showProgress("Please Wait...");
            SocialLoginParam socialLoginParam=new SocialLoginParam();
            socialLoginParam.setUser_fname(fname);
            socialLoginParam.setEmail(emailid);
            socialLoginParam.setApp_version(version_name);
            socialLoginParam.setDevice_id(device_id);
            socialLoginParam.setDevice_imei(Settings.Secure.ANDROID_ID);
            socialLoginParam.setDevice_os( Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName());
            socialLoginParam.setDevice_name(device_name);
            socialLoginParam.setLogintype(socialtype);
            cinemaTimeAPI.loginSocilNetwork(fname,emailid,socialid,socialtype,device_id,device_name,
                    Settings.Secure.ANDROID_ID,Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName(),
                    version_name).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();

                    JSONObject object = null;

                    try {
                        object = new JSONObject(String.valueOf(response.body()));
                        if (object.getString("Status").equals("Success")) {
                            JSONObject resultObject=object.getJSONObject("Results");
                            editor.putInt(Constants.LOGINSTATUS, 1);
                            editor.putInt(Constants.Userid,resultObject.getInt("id"));
                            editor.putString(Constants.USERNAME,resultObject.getString("user_fname"));
                            editor.putString(Constants.EMAIL,resultObject.getString("email"));
                            editor.putString(Constants.MOBILENUMBER,resultObject.getString("phone"));
                            editor.putString(Constants.PROFILEPHOTO,resultObject.getString("user_profile_img"));
                            editor.putString(Constants.DOB,resultObject.getString("dob"));
                            editor.putString(Constants.GENDER,resultObject.getString("gender"));
                            //editor.putString(Constants.MOBILENUMBER,model.getResults().getPhone());
                            editor.commit();
                            AppPreference.setLoggedIn(LoginActivity.this, true);
                            AppPreference.setSkip(LoginActivity.this, true);
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            Toast.makeText(LoginActivity.this, "success", Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Log.d("Vigneshwaran","onFailure");

                }
            });

        } else {
            hideProgress();
            Toast.makeText(this, "no internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        //updateUI(false);
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        // Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            signupSocialUser(acct.getDisplayName(),acct.getEmail(),"Google",acct.getId());




        } else {
            // Signed out, show unauthenticated UI.
            // updateUI(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            //Log.d(TAG, "Got cached sign-in");
            // GoogleSignInResult result = opr.get();
            // handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            // showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    //hideProgressDialog();
                    //handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    protected void attachBaseContext(Context base) {

        super.attachBaseContext(LanguageHelper.onAttach(base,LanguageHelper.getLanguage(base)));
    }
}

