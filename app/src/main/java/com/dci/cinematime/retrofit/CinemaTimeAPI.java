package com.dci.cinematime.retrofit;


import com.dci.cinematime.model.AppConfigResponse;
import com.dci.cinematime.model.AudienceType;
import com.dci.cinematime.model.ComingSoonResponse;
import com.dci.cinematime.model.ExclusiveResponse;
import com.dci.cinematime.model.LikeanddislikeResponse;
import com.dci.cinematime.model.LoginResponseModel;
import com.dci.cinematime.model.MovieDetailsReviewResponse;
import com.dci.cinematime.model.NotifyMe_cl;
import com.dci.cinematime.model.NowShowingResponse;
import com.dci.cinematime.model.SignupResponseModel;
import com.dci.cinematime.model.SocialLoginParam;
import com.dci.cinematime.model.TheatersList;
import com.dci.cinematime.utils.ExclusiveDetailsResponse;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CinemaTimeAPI {



    @FormUrlEncoded
    @POST("api/registration")
    Call<SignupResponseModel> signup(@Field("user_fname") String user_fname,
                                     @Field("email") String email,
                                     @Field("phone") String phone,
                                     @Field("password") String password,
                                     @Field("device_id") String device_id,
                                     @Field("device_name") String device_name,
                                     @Field("device_imei") String device_imei,
                                     @Field("device_os") String device_os,
                                     @Field("app_version") String app_version);



    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponseModel> login(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("device_id") String device_id,
                                   @Field("device_name") String device_name,
                                   @Field("device_imei") String device_imei,
                                   @Field("device_os") String device_os,
                                   @Field("app_version") String app_version);

    @FormUrlEncoded
    @POST("api/login")
    Call<JsonElement> userlogin(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("device_id") String device_id,
                                   @Field("device_name") String device_name,
                                   @Field("device_imei") String device_imei,
                                   @Field("device_os") String device_os,
                                   @Field("app_version") String app_version);





    @FormUrlEncoded
    @POST("api/comingsoon")
    Call<ComingSoonResponse> coming(@Field("user_id") int user_id,
                                    //@Field("setuser_id") String setuser_id,
                                    @Field("sortBy") String sortBy,
                                    @Field("language") String language);

    @FormUrlEncoded
    @POST("api/comingsoon")
    Call<JsonElement> getcomingSoonMovie(@Field("user_id") int user_id,
                                      @Field("sortBy") String sortBy,
                                    @Field("language") String language);



    @FormUrlEncoded
    @POST("api/nowShowing")
    Call<NowShowingResponse> nowshow(@Field("sortBy") String sortBy,
                                     @Field("language") String language, @Field("user_id")int user_id);



    @FormUrlEncoded
    @POST("api/exclusive")
//remaining url
    Call<ExclusiveResponse> execlu(@Field("sortBy") String sortBy,
                                   @Field("language") String language);



    @GET("api/movieLanguages")
    Call<JsonElement> getLang();

    @GET("api/appconfigs")
    Call<AppConfigResponse> getAppConfig();



    @GET("api/theatres")
    Call<TheatersList> getTheaterList();


    @GET("api/allmoviedetails")
    Call<JsonElement> getAllmovieandTheatrelist();

    @GET("api/allmoviedetails")
    Call<ExclusiveDetailsResponse> getAllmovieandTheatrelistbyModel();


    @FormUrlEncoded
    @POST("api/notifyMe")
    Call<NotifyMe_cl>noti(@Field("movie_id")int movie_id,
                          @Field("user_id")int user_id);


    @FormUrlEncoded
    @POST("api/search")
    Call<JsonElement>getmoviesearch(@Field("keyword")String keyword);



    @FormUrlEncoded
    @POST("api/movieDetails")
    Call<JsonElement>getmoviesDetail(@Field("movie_id")int movie_id,@Field("user_id")int user_id);

    @FormUrlEncoded
    @POST("api/movieReviews")
    Call<MovieDetailsReviewResponse>getMovieReview(@Field("movie_id")int movie_id,
                          @Field("page[number]")int page);


    @FormUrlEncoded
    @POST("api/addReview")
    Call<JsonElement>addmovieReview(@Field("movie_id")int movie_id, @Field("user_id")int user_id,@Field("reviews")String reviews);



    @FormUrlEncoded
    @POST("api/movieInTheatres")
    Call<JsonElement>getMovieThreate(@Field("movie_id")int movie_id, @Field("date")String date,@Field("time")String time,@Field("audienceType")int audienceType);

    @FormUrlEncoded
    @POST("api/showTimes")
    Call<JsonElement>getMovieShowtime(@Field("movie_id")int movie_id);


    @FormUrlEncoded
    @POST("api/theatreMovies")
    Call<JsonElement>getTheatreMovieList(@Field("theatre_id")int theatre_id,
                          @Field("date")String date,@Field("audienceType") int audienceType);
    @FormUrlEncoded
    @POST("api/addLike")
    Call<LikeanddislikeResponse>setMovielikeandDislike(@Field("movie_id")int movie_id,
                                                       @Field("user_id")int user_id, @Field("likeCount")int likeCount);



    @GET("api/audienceType")
    Call<AudienceType>getAudiencetype();

    @FormUrlEncoded
    @POST("api/socialLogin")
    Call<JsonElement>loginSocilNetwork(@Field("user_fname") String user_fname ,
                                       @Field("email") String email,
                                       @Field("socialId") String socialId,
                                       @Field("LoginType") String LoginType,
                                       @Field("device_id") String device_id,
                                       @Field("device_name") String device_name,
                                       @Field("device_imei") String device_imei,
                                       @Field("device_os") String device_os,
                                       @Field("app_version") String app_version);

    @FormUrlEncoded
    @POST("api/forgotpassword")
    Call<JsonElement> getforgetPassword(@Field("email_id") String emailid);


    @FormUrlEncoded
    @POST("api/updateProfile")
    Call<JsonElement> updateProfile(@Field("user_id") int user_id,@Field("dob") String dob,@Field("gender") String gender,
                                    @Field("phone") String phone,@Field("profileImg") String profileImg);


    @FormUrlEncoded
    @POST("api/artistdetails")
    Call<JsonElement> getCastDetails(@Field("id") int id);

}





