package com.dci.cinematime.utils;

public class Theatre{
	private int Status;
	private String Email;
	private String Description;
	private String created_at;
	private String Latitude;
	private String Mobile;
	private String Longitude;
	private String picture;
	private String Theatrename;
	private String updated_at;
	private String Phone;
	private String NoofScreens;
	private int id;
	private String Pincode;
	private citylist citylist;
	private state state;
	private country country;

	public Theatre.citylist getCitylist() {
		return citylist;
	}

	public void setCitylist(Theatre.citylist citylist) {
		this.citylist = citylist;
	}

	public Theatre.state getState() {
		return state;
	}

	public void setState(Theatre.state state) {
		this.state = state;
	}

	public Theatre.country getCountry() {
		return country;
	}

	public void setCountry(Theatre.country country) {
		this.country = country;
	}

	public class citylist
	{
		private int id;
		private String Cityname;
		private int Stateid;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCityname() {
			return Cityname;
		}

		public void setCityname(String cityname) {
			Cityname = cityname;
		}

		public int getStateid() {
			return Stateid;
		}

		public void setStateid(int stateid) {
			Stateid = stateid;
		}
	}
	public class state
	{
		private int id;
		private String Statename;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getStatename() {
			return Statename;
		}

		public void setStatename(String statename) {
			Statename = statename;
		}
	}
	public class country
	{
		private int id;
		private String Countryname;
		private int Stateid;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCountryname() {
			return Countryname;
		}

		public void setCountryname(String countryname) {
			Countryname = countryname;
		}

		public int getStateid() {
			return Stateid;
		}

		public void setStateid(int stateid) {
			Stateid = stateid;
		}
	}


	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}


	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}


	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getTheatrename() {
		return Theatrename;
	}

	public void setTheatrename(String theatrename) {
		Theatrename = theatrename;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}


	public String getNoofScreens() {
		return NoofScreens;
	}

	public void setNoofScreens(String noofScreens) {
		NoofScreens = noofScreens;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPincode() {
		return Pincode;
	}

	public void setPincode(String pincode) {
		Pincode = pincode;
	}
}
