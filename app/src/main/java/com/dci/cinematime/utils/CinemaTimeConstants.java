package com.dci.cinematime.utils;

/**
 * Created by vijayaganesh on 10/23/2017.
 */

public class CinemaTimeConstants {
    //Common Constants
    public static final String FCMTOKEN = "FcmToken";
    public static final String FCMKEYSHAREDPERFRENCES = "FCMKEYSHAREDPERFRENCES";
    public static final String DEVICEID = "DeviceID";
    public static final String APPID = "APPID";
    public static final String APPVERSION = "APPVERSION";
    public static final String SELECTEDLANGUAGES = "SELECTEDLANGUAGES";
    public static final String LOGINSTATUS = "LOGINSTATUS";
    public static final String IMEIID = "ImeiID";
    public static final String PHONENUMBER = "PHONENUMBER";


}
