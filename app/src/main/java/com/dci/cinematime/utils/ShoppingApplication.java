
package com.dci.cinematime.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;


public class ShoppingApplication extends MultiDexApplication {

    private static Context context;

    public static final String TAG = ShoppingApplication.class
            .getSimpleName();

    private static ShoppingApplication mInstance;
  //  AppEnvironment appEnvironment;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = null;
        context = getApplicationContext();
      //  intializeTwitter();
        MultiDex.install(this);

      //  appEnvironment = AppEnvironment.SANDBOX;
//        TwitterConfig config = new TwitterConfig.Builder(this)
//                .logger(new DefaultLogger(Log.DEBUG))
//                .twitterAuthConfig(new TwitterAuthConfig("Qv17c7O34xPWkvvAPOXUiDDYH", "YQi9mD1bsWj1V323sH7mTPw0pEGdgU7Rau76dam8KCX35aKjtW"))
//                .debug(true)
//                .build();
//        Twitter.initialize(config);
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }

//    public AppEnvironment getAppEnvironment() {
//        return appEnvironment;
//    }
//
//    public void setAppEnvironment(AppEnvironment appEnvironment) {
//        this.appEnvironment = appEnvironment;
//    }

    public static synchronized ShoppingApplication getInstance() {
        return mInstance;
    }


    public static Context globalContext() {
        return context;
    }

    public static Resources getAppResources() {
        return context.getResources();
    }

    public static String getAppString(int resourceId, Object... formatArgs) {
        return getAppResources().getString(resourceId, formatArgs);
    }

    public static String getAppString(int resourceId) {
        return getAppResources().getString(resourceId);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public static Context getGlobalContext() {
        // TODO Auto-generated method stub
        return context;
    }

}
