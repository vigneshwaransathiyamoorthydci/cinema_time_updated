package com.dci.cinematime.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by harini on 7/25/2018.
 */

public class AppPreference {

    public static void setversioncode(Context context, String value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(Constants.VERSION_CODE, String.valueOf(value));
        ed.commit();
    }

    public static String getversioncode(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Constants.VERSION_CODE, "");

    }

    public static boolean isLoggedIn(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(Constants.LOGIN_STATUS, false);
    }

    public static void setLoggedIn(Context context, boolean value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean(Constants.LOGIN_STATUS, value);
        ed.commit();
    }
    public static void setSkip(Context context, boolean value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean(Constants.SKIPSTATUS, value);
        ed.commit();
    }
    public static boolean isSkipIn(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(Constants.SKIPSTATUS, false);
    }

}
