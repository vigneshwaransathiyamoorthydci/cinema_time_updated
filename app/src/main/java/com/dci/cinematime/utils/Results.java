package com.dci.cinematime.utils;

import java.util.HashMap;
import java.util.List;

public class Results {
	private List<Theatre>  theatre;
	private List<com.dci.cinematime.utils.times> times;
	private String movie_original_title;
	private String showtime;
	private String name;
	private String movie_language;
	private String movie_trailer;
	private int id;
	private String movie_backgroundimg;
	private int theatre_id;
	private String movie_posterimage;
	HashMap exclusivetheatreListHashmap = new HashMap();
	HashMap exclusivetheatreTimeListHashmap = new HashMap();

	public HashMap getExclusivetheatreListHashmap() {
		return exclusivetheatreListHashmap;
	}

	public void setExclusivetheatreListHashmap(HashMap exclusivetheatreListHashmap) {
		this.exclusivetheatreListHashmap = exclusivetheatreListHashmap;
	}

	public HashMap getExclusivetheatreTimeListHashmap() {
		return exclusivetheatreTimeListHashmap;
	}

	public void setExclusivetheatreTimeListHashmap(HashMap exclusivetheatreTimeListHashmap) {
		this.exclusivetheatreTimeListHashmap = exclusivetheatreTimeListHashmap;
	}

	public List<Theatre> getTheatre() {
		return theatre;
	}

	public void setTheatre(List<Theatre> theatre) {
		this.theatre = theatre;
	}

	public List<com.dci.cinematime.utils.times> getTimes() {
		return times;
	}

	public void setTimes(List<com.dci.cinematime.utils.times> times) {
		this.times = times;
	}

	public String getMovie_original_title() {
		return movie_original_title;
	}

	public void setMovie_original_title(String movie_original_title) {
		this.movie_original_title = movie_original_title;
	}

	public String getShowtime() {
		return showtime;
	}

	public void setShowtime(String showtime) {
		this.showtime = showtime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMovie_language() {
		return movie_language;
	}

	public void setMovie_language(String movie_language) {
		this.movie_language = movie_language;
	}

	public String getMovie_trailer() {
		return movie_trailer;
	}

	public void setMovie_trailer(String movie_trailer) {
		this.movie_trailer = movie_trailer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMovie_backgroundimg() {
		return movie_backgroundimg;
	}

	public void setMovie_backgroundimg(String movie_backgroundimg) {
		this.movie_backgroundimg = movie_backgroundimg;
	}

	public int getTheatre_id() {
		return theatre_id;
	}

	public void setTheatre_id(int theatre_id) {
		this.theatre_id = theatre_id;
	}

	public String getMovie_posterimage() {
		return movie_posterimage;
	}

	public void setMovie_posterimage(String movie_posterimage) {
		this.movie_posterimage = movie_posterimage;
	}
}