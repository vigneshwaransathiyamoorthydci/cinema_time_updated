package com.dci.cinematime.utils;

import java.util.List;

public class ExclusiveDetailsResponse{
	private String Status;
	private List<Results> Results;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<com.dci.cinematime.utils.Results> getResults() {
		return Results;
	}

	public void setResults(List<com.dci.cinematime.utils.Results> results) {
		Results = results;
	}
}