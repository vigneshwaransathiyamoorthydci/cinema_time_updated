package com.dci.cinematime.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.SignupActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.zip.GZIPOutputStream;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import lib.kingja.switchbutton.SwitchMultiButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class UserProfileFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {


    TextView text_label_log_in;
    EditText edit_text_label_your_name, edit_text_label_email, edit_text_label_mobile_number, edit_text_label_password, edit_text_label_confirmpassword;
    Button button_sign_up;
    ImageView imagebackarrow;
    String username, emailid, mobilenum, passwrd, confirm_password;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    SwitchMultiButton switchGender;
    private int gender = 2;
    TextView text_label_gender_error;
    private Calendar currentD;
    private Calendar calendardate;
    private SimpleDateFormat simpleDateFormat;
    private String dob;
    private String genderstring;
    CircularImageView profileImage;
    private int SELECT_FILE = 2;
    private static final int MY_REQUEST_CODE_GALLERY = 6;
    private Uri galleryUri;
    private String selectedImage;
    Bitmap bundleImageBitmap;
    private int SELECT_IMAGE_FROM_GALLERY = 6;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_userprofile, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        //text_label_log_in = view.findViewById(R.id.text_label_log_in);
        //edit_text_label_your_name = (EditText) view.findViewById(R.id.edit_text_label_your_name);

        profileImage = (CircularImageView) view.findViewById(R.id.image_setting);
        edit_text_label_email = (EditText) view.findViewById(R.id.edit_text_label_email);
        edit_text_label_mobile_number = (EditText) view.findViewById(R.id.edit_text_label_mobile_number);
        //edit_text_label_password =(EditText) view.findViewById(R.id.edit_text_label_password);
        //edit_text_label_confirmpassword=view.findViewById(R.id.edit_text_label_confirmpassword);
        button_sign_up = (Button) view.findViewById(R.id.button_sign_up);
        switchGender = (SwitchMultiButton) view.findViewById(R.id.switch_gender);
        text_label_gender_error = (TextView) view.findViewById(R.id.text_label_gender_error);
        dob = sharedPreferences.getString(Constants.DOB, null);
        edit_text_label_email.setText(dob);
        mobilenum = sharedPreferences.getString(Constants.MOBILENUMBER, null);
        edit_text_label_mobile_number.setText(mobilenum);
        edit_text_label_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text_label_email.setError(null);
                datePickerDialog();
            }
        });


        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFiles();
            }
        });
        if (sharedPreferences.getString(Constants.PROFILEPHOTO,null)!=null) {

//            byte[] decodedString = Base64.decode(sharedPreferences.getString(Constants.PROFILEPHOTO, null), Base64.DEFAULT);
//            bundleImageBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            if (bundleImageBitmap != null) {
//                profileImage.setImageBitmap(bundleImageBitmap);
//            }
            Picasso.with(getContext()).
                    load(getContext().getString(R.string.coming_soon_image_base_url)+Util.USERPROFILEIMAGE
                            + sharedPreferences.getString(Constants.PROFILEPHOTO,null)).placeholder(R.mipmap.placeholder_icon).
                    into(profileImage);
        }
        switchGender.setOnSwitchListener

                (new SwitchMultiButton.OnSwitchListener() {
                    @Override
                    public void onSwitch(int position, String tabText) {
                        text_label_gender_error.setError(null);
                        gender = position;
                    }
                });

        genderstring = sharedPreferences.getString(Constants.GENDER, "not");
        if (genderstring.equals("male")) {
            switchGender.setSelectedTab(0);
            gender = 0;
        } else if (genderstring.equals("female")) {
            switchGender.setSelectedTab(1);
            gender = 1;
        } else {
            gender = 2;
        }
        imagebackarrow = (ImageView) view.findViewById(R.id.userprofile_arrow);
        imagebackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileNullcheck();

            }
        });

        return view;
    }


    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        maxDate.add(Calendar.YEAR, -18);
        datePickerDialog.setMaxDate(maxDate);
        minDate.add(Calendar.YEAR, -60);
        datePickerDialog.setMinDate(minDate);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.red));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        dob = simpleDateFormat.format(calendardate.getTime());
        edit_text_label_email.setText(dob);
        //age = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));


    }


    private void updateProfileNullcheck() {
        // username = edit_text_label_your_name.getText().toString().trim();
        emailid = edit_text_label_email.getText().toString().trim();
        mobilenum = edit_text_label_mobile_number.getText().toString().trim();
        //passwrd = edit_text_label_password.getText().toString().trim();
        //  confirm_password=edit_text_label_confirmpassword.getText().toString().trim();
        if (edit_text_label_email.getText().toString() != null && edit_text_label_email.getText().toString().length() <= 0) {
            edit_text_label_email.setError(getString(R.string.enter_dob));
            //Toast.makeText(SignupActivity.this, "Enter vaild email", Toast.LENGTH_SHORT).show();
        } else if (mobilenum.length() < 10) {
            edit_text_label_mobile_number.setError(getString(R.string.enter_mobilenumber));
            //Toast.makeText(SignupActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
        } else if (gender == 2) {
            text_label_gender_error.setError(getString(R.string.select_gender));
            //Toast.makeText(SignupActivity.this, "Please enter your correct password", Toast.LENGTH_SHORT).show();
        } else {

            //signupUser();
            updateUserProfile();
        }
    }

    public static byte[] compress(String data) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length());
        GZIPOutputStream gzip = new GZIPOutputStream(bos);
        gzip.write(data.getBytes());
        gzip.close();
        byte[] compressed = bos.toByteArray();
        bos.close();
        return compressed;
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, baos);
        byte [] b=baos.toByteArray();
        String temp=null;
        try{
            System.gc();
            temp=Base64.encodeToString(b, Base64.DEFAULT);
        }catch(Exception e){
            e.printStackTrace();
        }catch(OutOfMemoryError e){
            baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,50, baos);
            b=baos.toByteArray();
            temp=Base64.encodeToString(b, Base64.DEFAULT);
            Log.e("EWN", "Out of memory error catched");
        }
        return temp;
    }
    private String getStringFromBitmap(Bitmap bitmapPicture) {
        /*
         * This functions converts Bitmap picture to a string which can be
         * JSONified.
         * */
        final int COMPRESSION_QUALITY = 100;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }
    private void updateUserProfile() {

        if (Util.isNetworkAvailable()) {
            showProgress();
            String usergender;
            if (gender == 0) {
                usergender = "male";
            } else {
                usergender = "female";
            }
            String profileImage = "";
            if (bundleImageBitmap!=null)
            {
                profileImage=encodeTobase64(bundleImageBitmap);
                Log.d("Vikis","bitmapToString"+profileImage);
//                try {
//                    profileImage= String.valueOf(compress(profileImage));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
            cinemaTimeAPI.updateProfile(sharedPreferences.getInt(Constants.Userid, 0), edit_text_label_email.getText().toString(),
                    usergender, edit_text_label_mobile_number.getText().toString(),profileImage).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                    hideProgress();
                    if (response.body() != null) {
                        JSONObject object = null;

                        try {
                            object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("Status").equals("Success")) {
                                JSONObject resultObject = object.getJSONObject("Results");
                                editor.putInt(Constants.LOGINSTATUS, 1);
                                editor.putInt(Constants.Userid, resultObject.getInt("id"));
                                editor.putString(Constants.USERNAME, resultObject.getString("user_fname"));
                                editor.putString(Constants.EMAIL, resultObject.getString("email"));
                                editor.putString(Constants.GENDER, resultObject.getString("gender"));
                                editor.putString(Constants.MOBILENUMBER, resultObject.getString("phone"));
                                editor.putString(Constants.DOB, resultObject.getString("dob"));
                                editor.putString(Constants.PROFILEPHOTO,resultObject.getString("user_profile_img"));
                                //editor.putString(Constants.MOBILENUMBER,model.getResults().getPhone());
                                editor.commit();
                                Toast.makeText(getActivity(), "Profile Updated", Toast.LENGTH_SHORT).show();
                                getActivity().onBackPressed();

                            } else {
                                Toast.makeText(getActivity(), object.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getActivity(), "no internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void getImageFiles() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_REQUEST_CODE_GALLERY);
            } else {
                Intent intent = new Intent(
                        Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                intent.setType("image/*");
                startActivityForResult(
                        Intent.createChooser(intent,"Select File"),
                        SELECT_IMAGE_FROM_GALLERY);
            }
        } else {

            Intent intent = new Intent(
                    Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.setType("image/*");
            startActivityForResult(
                    Intent.createChooser(intent,"Select File"),
                    SELECT_IMAGE_FROM_GALLERY);

        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_IMAGE_FROM_GALLERY) {
                //Crop.of(data.getData(), outputUri).asSquare().start(getActivity());
                onSelectFromGalleryResult(data);

            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            galleryUri = data.getData();
            try {
                bundleImageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), galleryUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            compressInputImage(data, getActivity(), profileImage);
        }
//        if (galleryUri != null)
//            selectedImage = System.currentTimeMillis() + ".jpg";
    }







}
