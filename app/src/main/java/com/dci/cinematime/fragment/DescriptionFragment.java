package com.dci.cinematime.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.dci.cinematime.Adapter.MovieCastAdapter;
import com.dci.cinematime.Adapter.MovieCrewAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.model.MovieDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 7/25/2018.
 */

public class DescriptionFragment extends Fragment {

    TextView description;
    RecyclerView castrecyle, crewrecyle;
    MovieCastAdapter movieCastAdapter;
    MovieCrewAdapter movieCrewAdapter;
    List<MovieDetail> movieDetailList;
    Boolean isCheck= true;
    TextView castTextview,crewTextview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //    changeTabsFont();
        View view = inflater.inflate(R.layout.fragment_description, container, false);
        description = (TextView)view.findViewById(R.id.description);
        castrecyle = view.findViewById(R.id.castrecyle);
        crewrecyle = view.findViewById(R.id.crewrecyle);
        castTextview=(TextView)view.findViewById(R.id.castTextview);
        crewTextview=(TextView)view.findViewById(R.id.crewTextview);
        movieDetailList = new ArrayList<>();
        //description.setText(MovieDetailActivity.description);
        if ( MovieDetailActivity.moviedetailsCast.size()==0)
        {
            castTextview.setVisibility(View.GONE);
        }
        if ( MovieDetailActivity.moviedetailsCrew.size()==0)
        {
            crewTextview.setVisibility(View.GONE);
        }
        movieCastAdapter = new MovieCastAdapter(getActivity(), MovieDetailActivity.moviedetailsCast,MovieDetailActivity.castBaseUrl);
        castrecyle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        castrecyle.setItemAnimator(new DefaultItemAnimator());
        castrecyle.setNestedScrollingEnabled(false);
        castrecyle.setAdapter(movieCastAdapter);

        movieCrewAdapter = new MovieCrewAdapter(getActivity(), MovieDetailActivity.moviedetailsCrew);
        crewrecyle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        crewrecyle.setItemAnimator(new DefaultItemAnimator());
        crewrecyle.setAdapter(movieCrewAdapter);
        crewrecyle.setNestedScrollingEnabled(false);
        description.setText(MovieDetailActivity.description);

        makeTextViewResizable(description, 2, "View More", true);
//        description.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isCheck) {
//                    description.setMaxLines(25);
//                    description.setText(Html.fromHtml(MovieDetailActivity.description+"<font color='red'> <u>View Less</u></font>"));
//                    isCheck = false;
//                } else {
//                    description.setMaxLines(5);
//                    description.setText(Html.fromHtml(MovieDetailActivity.description+"<font color='red'> <u>View More</u></font>"));
//                    isCheck = true;
//                }
//            }
//        });



        return view;
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(
                        addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE);
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        makeTextViewResizable(tv, 2, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

}
