package com.dci.cinematime.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.SearchListAdpater;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.MoviesItem;
import com.dci.cinematime.model.SearchResponse;
import com.dci.cinematime.model.SearchResults;
import com.dci.cinematime.model.TheatresItem;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends BaseFragment {

    ListView listviewSearch;
    SearchView searchview;
    boolean issearchLoading=true;
    ArrayList<SearchResults> searchResults;
    ArrayList<MoviesItem> moviesItemssearchResults;
    ArrayList<TheatresItem> threatresItemssearchResults;
    SearchResponse searchResponse;
    SearchListAdpater searchListAdpater;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    TextView text_no_comm;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_search, container, false);
        ((HomeActivity)getActivity()).hidetoolbar();//this calling method is show the tool
        CinemaTimeApplication.getContext().getComponent().inject(this);
        listviewSearch=(ListView)view.findViewById(R.id.listview_search);
        text_no_comm=(TextView)view.findViewById(R.id.text_no_comm);
        searchview=(SearchView)view.findViewById(R.id.search_movietheatre);
        searchResults=new ArrayList<SearchResults>();
        moviesItemssearchResults=new ArrayList<MoviesItem>();
        threatresItemssearchResults=new ArrayList<TheatresItem>();
        searchListAdpater = new SearchListAdpater(moviesItemssearchResults, getActivity());
        listviewSearch.setAdapter(searchListAdpater);
        listviewSearch.setDivider(null);
        searchview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchview.setIconified(false);
            }
        });
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.toString().length()>=2)
                {
                    getdata(newText);
                }
                return false;
            }
        });
        listviewSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(listviewSearch.getWindowToken(), 0);
                if (moviesItemssearchResults.get(position).getCategory()==1)
                {
                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                    i.putExtra("id", moviesItemssearchResults.get(position).getId());
                    i.putExtra("releaseType", moviesItemssearchResults.get(position).getReleaseType());
                    startActivity(i);
                }
                else
                {
                    Intent i = new Intent(getActivity(), TheatreDetailsActivity.class);
                    i.putExtra("theatreid", moviesItemssearchResults.get(position).getId());
                    i.putExtra("theatrename",moviesItemssearchResults.get(position).getName());
                    startActivity(i);
                    getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getdata(String language) {
        if (Util.isNetworkAvailable()) {
            //  if (isRefresh) {
            issearchLoading=false;
            text_no_comm.setVisibility(View.GONE);
            listviewSearch.setVisibility(View.VISIBLE);
            //showProgress();
            // }
            //  swip_lay.setRefreshing(true);

            cinemaTimeAPI.getmoviesearch(language).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    issearchLoading=true;
                    moviesItemssearchResults.clear();
                    if (response.body() != null) {
                        try {

                            JSONObject object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("Status").equals("Success"))
                            {
                            Object searchresult;

                            searchresult = object.get("Results");
                            boolean check = false;
                            if (searchresult instanceof JSONArray) {
                                // It's an array
                                check=true;
                            }
                            else if (searchresult instanceof JSONObject) {
                                // It's an object
                                check=false;
                            }
                            if (!check) {
                                JSONObject jsonArray = object.getJSONObject("Results");


                                for (int i = 0; i < 1; i++) {


                                    JSONArray moviesArray = jsonArray.getJSONArray("movies");
                                    if (moviesArray.length() != 0) {
                                        for (int j = 0; j < moviesArray.length(); j++) {
                                            JSONObject moviesListObject = moviesArray.getJSONObject(j);
                                            MoviesItem moviesItem = new MoviesItem();
                                            moviesItem.setId(moviesListObject.getInt("id"));
                                            moviesItem.setName(moviesListObject.getString("name"));
                                            moviesItem.setMovie_original_title(moviesListObject.getString("movie_original_title"));
                                            moviesItem.setCategory(1);
                                            moviesItem.setReleaseType(moviesListObject.getInt("releaseType"));
                                            JSONArray castArray = moviesListObject.getJSONArray("cast");
                                            ArrayList<String> moviecast = new ArrayList<String>();
                                            String moviecaststring = null;
                                            moviecast.add("Cast");
                                            for (int l = 0; l < castArray.length(); l++) {
                                                JSONObject castListObject = castArray.getJSONObject(l);
                                                moviecaststring = castListObject.getString("crew_name");
                                                moviecast.add(moviecaststring);

                                            }
                                            StringBuilder builder = new StringBuilder();
                                            for (String value : moviecast) {
                                                builder.append(value);
                                                builder.append(",");
                                            }
                                            String text = builder.toString();
                                            moviesItem.setCrew_name(text);
                                            moviesItemssearchResults.add(moviesItem);


                                        }


                                    }
                                    JSONArray theatresArray = jsonArray.getJSONArray("theatres");
                                    if (theatresArray.length() != 0) {

                                        for (int k = 0; k < theatresArray.length(); k++) {
                                            JSONObject moviesListObject = theatresArray.getJSONObject(k);
                                            MoviesItem moviesItem = new MoviesItem();
                                            moviesItem.setId(moviesListObject.getInt("id"));
                                            moviesItem.setName(moviesListObject.getString("name"));
                                            moviesItem.setCategory(2);
                                            JSONObject cityObject=moviesListObject.getJSONObject("city");
                                            moviesItem.setCity(cityObject.getString("Cityname"));
                                            JSONObject stateObject=moviesListObject.getJSONObject("state");
                                            moviesItem.setState(stateObject.getString("Statename"));
                                            JSONObject countryObject=moviesListObject.getJSONObject("country");
                                            moviesItem.setCountry(countryObject.getString("Countryname"));
                                            moviesItemssearchResults.add(moviesItem);

                                        }
                                    }


                                    searchListAdpater.notifyDataSetChanged();


                                }
                            }

                            }
                            else
                            {
                                searchListAdpater.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    issearchLoading=true;
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

}
