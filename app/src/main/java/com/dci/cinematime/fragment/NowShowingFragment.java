package com.dci.cinematime.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.Adapter.NowShowingAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.BaseActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.NowShowingResponse;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NowShowingFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private List<NowShowingResponse.Result> showing;
    private RecyclerView recyclerView;
    private NowShowingAdapter mAdapter;
    boolean isRefresh = false;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swip_lay;
    HomeActivity homeActivity;
    ImageView imageNoList;
    TextView textNoList;
    LinearLayoutManager linearLayoutManager;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public NowShowingFragment() {
        // Required empty public constructor
    }
    BaseActivity baseActivity;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    int movieid;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nowshowing, container, false);
        homeActivity=(HomeActivity)getActivity();
        CinemaTimeApplication.getContext().getComponent().inject(this);
        baseActivity = (BaseActivity) getActivity();


//        homeFilterPopupActivity=(HomeFilterPopupActivity)getActivity();
        recyclerView = view.findViewById(R.id.recycle_now_showing);
        swip_lay = view.findViewById(R.id.swip_lay);
        imageNoList=(ImageView)view.findViewById(R.id.image_no_comments);
        textNoList=(TextView)view.findViewById(R.id.text_no_comm);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        showing = new ArrayList<>();
        mAdapter = new NowShowingAdapter(showing, getActivity(),baseActivity,homeActivity);
        recyclerView.setAdapter(mAdapter);
        editor = sharedPreferences.edit();


        swip_lay.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swip_lay.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {

                            showing.clear();
                            mAdapter.notifyDataSetChanged();
                            hideProgress();
                            HomeFilterPopupActivity.sortby=1;
                            HomeFilterPopupActivity.selectedlanguage="";
                            getNowshowingdata(HomeFilterPopupActivity.sortby, HomeFilterPopupActivity.selectedlanguage);
                            swip_lay.setRefreshing(false);
                        }
                        }

                }, 000);
            }
        });



        return view;


    }

    @Override
    public void onResume() {
        super.onResume();

       // getdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);



    }

    private void getNowshowingdata(int sortby,String language) {
        if (Util.isNetworkAvailable()) {
          //  if (isRefresh) {
            imageNoList.setVisibility(View.GONE);
            //imageNoList.setImageResource();
            textNoList.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            showProgress();
           // }
          //  swip_lay.setRefreshing(true);

             cinemaTimeAPI.nowshow(String.valueOf(sortby), language,sharedPreferences.getInt(Constants.Userid,0)).enqueue(new Callback<NowShowingResponse>() {
                @Override
                public void onResponse(Call<NowShowingResponse> call, Response<NowShowingResponse> response) {
                    hideProgress();
                    hiderefresh();

                    showing.clear();
                    if (response.body() != null) {

                        NowShowingResponse model = response.body();
                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(model);
                        if (model.getResults()!=null) {


                            showing.addAll(model.getResults());

                            if (showing.size() != 0) {


                                mAdapter.notifyDataSetChanged();
                                int resId = R.anim.layout_animation_fall_down;
                                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
                                recyclerView.setLayoutAnimation(animation);
                                //recyclerView.setItemAnimator(new DefaultItemAnimator());

                                mAdapter.setOnClickListen(new NowShowingAdapter.AddTouchListen() {

                                    @Override
                                    public void onTouchClick(int position) {
                                        movieid = showing.get(position).getId();


                                        Log.d("TAG", "The interstitial wasn't loaded yet.");
                                        Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                        i.putExtra("id", showing.get(position).getId());
                                        i.putExtra("showtime", true);
                                        startActivity(i);

//                                    Intent i = new Intent(getActivity(), MovieDetailActivity.class);
//                                    i.putExtra("id", showing.get(position).getId());
//                                    i.putExtra("showtime",true);
//                                    startActivity(i);
                                    }
                                });

                                hideProgress();


                            } else {
                                if (isAdded()) {
                                    imageNoList.setVisibility(View.VISIBLE);
                                    //imageNoList.setImageResource();
                                    textNoList.setVisibility(View.VISIBLE);
                                    textNoList.setText(model.getMessage());
                                    recyclerView.setVisibility(View.GONE);

                                }


                            }
                        }
                        else
                        {
                            if (isAdded()) {
                                imageNoList.setVisibility(View.VISIBLE);
                                //imageNoList.setImageResource();
                                textNoList.setVisibility(View.VISIBLE);
                                textNoList.setText(model.getMessage());
                                recyclerView.setVisibility(View.GONE);

                            }
                        }
                    } else {
                        if (isAdded()) {
                            imageNoList.setVisibility(View.VISIBLE);
                            //imageNoList.setImageResource();
                            textNoList.setVisibility(View.VISIBLE);
                            textNoList.setText(R.string.No_movie);
                            recyclerView.setVisibility(View.GONE);

                        }
                    }
                }

                @Override
                public void onFailure(Call<NowShowingResponse> call, Throwable t) {
                    hiderefresh();
                    hideProgress();
                    if (isAdded()) {
                        imageNoList.setVisibility(View.VISIBLE);
                        //imageNoList.setImageResource();
                        textNoList.setVisibility(View.VISIBLE);
                        textNoList.setText(R.string.No_movie);
                        recyclerView.setVisibility(View.GONE);

                    }


                }
            });

        } else {
            if (isAdded()) {
                imageNoList.setVisibility(View.VISIBLE);
                //imageNoList.setImageResource();
                textNoList.setVisibility(View.VISIBLE);
                textNoList.setText(R.string.no_network);
                recyclerView.setVisibility(View.GONE);

            }
        }
    }



   /* private void nowshowing(){
        NowShowing_cl nw1 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw1);

        NowShowing_cl nw2 = new NowShowing_cl("Ocean 14","English-Arabic","U/A","95%","98,000 views");

        showing.add(nw2);

        NowShowing_cl nw3 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw3);

        NowShowing_cl nw4 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw4);

        NowShowing_cl nw5 = new NowShowing_cl("Ocean 14","English-Arabic","A","95%","98,000 views");

        showing.add(nw5);
    }*/
   @Override
   public void onStart() {
       super.onStart();
       isStarted = true;
       if (isVisible) {
           homeActivity.filteroption=0;
           homeActivity.image_filter.setVisibility(View.VISIBLE);
           if (HomeFilterPopupActivity.nowshowingFilter)
           {
               HomeFilterPopupActivity.nowshowingFilter=false;
               getNowshowingdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);
           }
           else if (showing.size()<=0)
           {
               HomeFilterPopupActivity.nowshowingFilter=false;
           getNowshowingdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);
           }


       }


   }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            homeActivity.filteroption=0;
            homeActivity.image_filter.setVisibility(View.VISIBLE);
            if (HomeFilterPopupActivity.nowshowingFilter)
            {
                HomeFilterPopupActivity.nowshowingFilter=false;
                getNowshowingdata(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);
            }
            else  if (showing.size()<=0) {
                HomeFilterPopupActivity.nowshowingFilter=false;
                getNowshowingdata(HomeFilterPopupActivity.sortby, HomeFilterPopupActivity.selectedlanguage);
            }

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }


}
