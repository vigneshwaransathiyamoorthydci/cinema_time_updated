package com.dci.cinematime.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.ExclusiveAdapter;
import com.dci.cinematime.Adapter.ExclusiveShowingAdapter;
import com.dci.cinematime.Adapter.TheatersAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.AudienceTypeFilterActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.ExclusiveResponse;
import com.dci.cinematime.model.MovieDetailsWithThearteDetails;
import com.dci.cinematime.model.MovieDetailsWithThearteDetailsTheatreList;
import com.dci.cinematime.model.NowShowingResponse;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.dci.cinematime.model.TheatersList;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.ExclusiveDetailsResponse;
import com.dci.cinematime.utils.LocationService;
import com.dci.cinematime.utils.Results;
import com.dci.cinematime.utils.ShoppingApplication;
import com.dci.cinematime.utils.Theatre;
import com.dci.cinematime.utils.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExclusiveFragment extends BaseFragment{
    private List<Results> exclusiveResult;
    private List<ExclusiveResponse.Results.Datum> exclu = new ArrayList<>();
    HashMap exclusivetheatreListHashmap = new HashMap();
    HashMap exclusivetheatreTimeListHashmap = new HashMap();
    private ArrayList<Theatre> exclusivetheatreList;
    public  ArrayList<ShowTimeMovieTiminginTheater> showTimeMovieTiminginTheater;
    private RecyclerView recyclerView;
    private ExclusiveAdapter mAdapter;
    boolean isRefresh = false;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swip_lay;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public ExclusiveFragment() {
        // Required empty public constructor
    }
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    ImageView imageNoList;
    TextView textNoList;

    List<TheatersList.ResultsItem> theaterList;
    TheatersAdapter theatersAdapter;
    LinearLayoutManager linearLayoutManager;
    private ArrayList<MovieDetailsWithThearteDetails> movieDetailsWithThearteDetails;
    private ArrayList<MovieDetailsWithThearteDetailsTheatreList> movieDetailsWithThearteDetailsArrayList;
    ExclusiveShowingAdapter exclusiveShowingAdapter;
    HashMap theatrelist = new HashMap();
    HomeActivity homeActivity;
    IntentFilter filter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exclusive, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        homeActivity=(HomeActivity)getActivity();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_exclusive);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayoutManager);
        imageNoList=(ImageView)view.findViewById(R.id.image_no_comments);
        textNoList=(TextView)view.findViewById(R.id.text_no_comm);
        movieDetailsWithThearteDetails=new ArrayList<MovieDetailsWithThearteDetails>();
        exclusiveResult=new ArrayList<>();
        exclusivetheatreList=new ArrayList<>();
        showTimeMovieTiminginTheater=new ArrayList<>();
         filter = new IntentFilter("Hello World");

       // swip_lay = view.findViewById(R.id.swip_lay);

         //exclusive();
//        swip_lay.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                swip_lay.setRefreshing(true);
//                (new Handler()).postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        getTheatreMovieList(0.0,0.0);
//
//                        swip_lay.setRefreshing(false);
//                    }
//
//                }, 000);
//            }
//        });

        return view;
    }


    private BroadcastReceiver smsBroadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            hideProgress();
            if (isVisible && isStarted) {
                //getTheatreMovieList(intent.getDoubleExtra("Latitude", 0), intent.getDoubleExtra("Longitude", 0));

            }
        }
    };
    private void getTheatreMovieList(final Double latitude, final Double longitude) {
        if (Util.isNetworkAvailable()) {

            //  swip_lay.setRefreshing(true);
            showProgress();
            cinemaTimeAPI.getAllmovieandTheatrelistbyModel().enqueue(new Callback<ExclusiveDetailsResponse>() {
                @Override
                public void onResponse(Call<ExclusiveDetailsResponse> call, Response<ExclusiveDetailsResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        exclusiveResult.clear();
                        exclusivetheatreListHashmap.clear();
                        ExclusiveDetailsResponse model = response.body();
                        Gson gson = new GsonBuilder().create();
                        String json = gson.toJson(model);
                        if (model.getResults() != null) {
                            exclusiveResult.addAll(model.getResults());

                            for (int i=0;i<model.getResults().size();i++)
                            {
                                exclusivetheatreListHashmap.put(model.getResults().get(i).getId(),model.getResults().get(i).getTheatre());

                                }




                        }
                        exclusiveShowingAdapter=new ExclusiveShowingAdapter(exclusiveResult,getContext(),exclusivetheatreListHashmap,exclusivetheatreTimeListHashmap,latitude,longitude);
                        recyclerView.setAdapter(exclusiveShowingAdapter);

                    }

                }

                @Override
                public void onFailure(Call<ExclusiveDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                }
            });
//            cinemaTimeAPI.getAllmovieandTheatrelist().enqueue(new Callback<JsonElement>() {
//                @Override
//                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
//                    hideProgress();
//
//
//                    if (response.body() != null) {
//                        try {
//                            JSONObject object = new JSONObject(String.valueOf(response.body()));
//                            if (object.getString("Status").equals("Success")) {
//                                JSONArray resultsArray = object.getJSONArray("Results");
//                                for (int i = 0; i < resultsArray.length(); i++) {
//                                    JSONObject movieArrayObject = resultsArray.getJSONObject(i);
//                                    JSONObject theatrejsonObject=movieArrayObject.getJSONObject("theatre");
//                                    movieDetailsWithThearteDetailsArrayList=new ArrayList<MovieDetailsWithThearteDetailsTheatreList>();
//                                    for (int y=0;y<theatrejsonObject.length();y++)
//                                    {
//                                        movieDetailsWithThearteDetailsArrayList.add(new MovieDetailsWithThearteDetailsTheatreList(theatrejsonObject.getInt("id"),
//                                                theatrejsonObject.getString("Theatrename"),theatrejsonObject.getString("Description"),
//                                                theatrejsonObject.getString("picture"),theatrejsonObject.getString("Latitude"),
//                                                theatrejsonObject.getString("Longitude")));
//                                    }
//                                    theatrelist.put(movieArrayObject.getInt("id"),movieDetailsWithThearteDetailsArrayList);
//                                    movieDetailsWithThearteDetails.add(new MovieDetailsWithThearteDetails(movieArrayObject.getInt("id"),
//                                            movieArrayObject.getString("name"),movieArrayObject.getString("movie_original_title"),
//                                            movieArrayObject.getString("movie_language"),movieArrayObject.getString("movie_posterimage"),
//                                            movieArrayObject.getString("movie_backgroundimg"),movieArrayObject.getString("movie_trailer"),
//                                            movieDetailsWithThearteDetailsArrayList,theatrelist));
//                                }
//
//                                exclusiveShowingAdapter.notifyDataSetChanged();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    else
//                    {
//                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<JsonElement> call, Throwable t) {
//                    hiderefresh();
//                    hideProgress();
//                    //  swip_lay.setRefreshing(false);
//                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
//
//
//                }
//            });

        } else {
            Toast.makeText(getActivity(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible) {
            getActivity().registerReceiver(smsBroadcastReceiver, filter);
            homeActivity.image_filter.setVisibility(View.INVISIBLE);
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                //gps.hideSettingAlert();
                showProgress();
                getActivity().startService(new Intent(getActivity(), LocationService.class));
//
            }else {
//
//
                showSettingsAlert();
//
            }




        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            isStarted = true;
            homeActivity.image_filter.setVisibility(View.INVISIBLE);
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){

                showProgress();
            getActivity().startService(new Intent(getActivity(), LocationService.class));

        } else {

                showSettingsAlert();
        }



    }
        else if (isVisible)
        {
           getActivity().stopService(new Intent(getActivity(),LocationService.class));
        }
    }
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getActivity().startActivity(intent);
                dialog.cancel();
            }
        });

        // On pressing the cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                getTheatreMovieList(0.0,0.0);
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;

        }

        //getActivity().stopService(new Intent(getActivity(),LocationService.class));


    @Override
    public void onDestroy() {
        super.onDestroy();

//            getActivity().unregisterReceiver(smsBroadcastReceiver);

        //getActivity().stopService(new Intent(getActivity(),LocationService.class));
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}

