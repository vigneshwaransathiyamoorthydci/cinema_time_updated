package com.dci.cinematime.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.CategoryListAdpater;
import com.dci.cinematime.Adapter.MovieCastAdapter;
import com.dci.cinematime.Adapter.MovieCrewAdapter;
import com.dci.cinematime.Adapter.MovieReviewAdapter;
import com.dci.cinematime.Adapter.RecyclerViewAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.LoginActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.CastItem;
import com.dci.cinematime.model.CategoryItem;
import com.dci.cinematime.model.CrewItem;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailReviewItem;
import com.dci.cinematime.model.MovieDetailsReviewResponse;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 7/25/2018.
 */

public class ReviewFragment extends BaseFragment {
    EditText review;
    RecyclerView recyclerView;
    MovieReviewAdapter movieReviewAdapter;
    List<MovieDetail> movieDetailList;
    List<MovieDetailReviewItem> movieDetailReviewList;
    MovieDetailsReviewResponse movieDetailsReviewResponse;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    LinearLayoutManager linearLayoutManager;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public  static  boolean lastEnd=true;
    public boolean isLoading = false;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    ImageView imageNoReview;
    TextView textNoReview;
    static
    ArrayList<String> Test=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //    changeTabsFont();
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        recyclerView = view.findViewById(R.id.review_recycle);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayoutManager);
        review = view.findViewById(R.id.review);
        imageNoReview=(ImageView)view.findViewById(R.id.image_no_comments);
        textNoReview=(TextView)view.findViewById(R.id.text_no_comm);
        movieDetailList = new ArrayList<>();
        movieDetailReviewList=new ArrayList<MovieDetailReviewItem>();
        editor = sharedPreferences.edit();
        movieReviewAdapter = new MovieReviewAdapter(getActivity(), movieDetailReviewList,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(movieReviewAdapter);
//        recyclerViewAdapter=new RecyclerViewAdapter(getActivity(),Test);
//        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 15) {
                        if (!lastEnd) {
                            currentPage = currentPage + 1;
                            getMovieReview();
                        }
                    }
                }
            }

        });
        review.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (sharedPreferences.getInt(Constants.Userid,0)!=0)
                    {
                        if (review.getText().toString().length()>0)
                        {
                            addmoviewReview();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "Enter Review", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Login to continue", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    return true;
                }
                return false;
            }
        });
        return view;
    }

    private void addmoviewReview()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();

            cinemaTimeAPI.addmovieReview(MovieDetailActivity.movieId,sharedPreferences.getInt(Constants.Userid,0),review.getText().toString()).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    review.setText("");
                    try {
                        JSONObject object = new JSONObject(String.valueOf(response.body()));
                        if (object.getString("Status").equals("Success"))
                        {
                            Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                            getMovieReview();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "Try Later", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), "Try Later", Toast.LENGTH_SHORT).show();

                }
            });
        }
        else

        {
            Toast.makeText(getActivity(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }
    public void getMovieReview() {
        if (Util.isNetworkAvailable()) {
            currentPage = currentPage + 1;
            //  swip_lay.setRefreshing(true);
            if (currentPage==1) {
                movieDetailReviewList.clear();
            }
            showProgress();
            if (isAdded())
            {
                recyclerView.setVisibility(View.VISIBLE);
                imageNoReview.setVisibility(View.GONE);
                textNoReview.setVisibility(View.GONE);
            }

            cinemaTimeAPI.getMovieReview(MovieDetailActivity.movieId,currentPage).enqueue(new Callback<MovieDetailsReviewResponse>() {
                @Override
                public void onResponse(Call<MovieDetailsReviewResponse> call, Response<MovieDetailsReviewResponse> response) {
                    hideProgress();
                    movieDetailsReviewResponse=response.body();
                    if (response.body()!=null && movieDetailsReviewResponse.getStatus().equals("Success"))
                    {
                        //Toast.makeText(getActivity(), "sucuess", Toast.LENGTH_SHORT).show();

                        for (int y=0;y<movieDetailsReviewResponse.getResults().getData().size();y++)
                        {

                            if (movieDetailsReviewResponse.getResults().getData().get(y).getUser_details()!=null) {
                                Test.add( movieDetailsReviewResponse.getResults().getData().get(y).getUser_details().getUser_fname());
                                movieDetailReviewList.add(new MovieDetailReviewItem(movieDetailsReviewResponse.getResults().getData().get(y).getReviews(),
                                        movieDetailsReviewResponse.getResults().getData().get(y).getUser_details().getUser_fname(),
                                        movieDetailsReviewResponse.getResults().getData().get(y).getReview_status(),
                                        movieDetailsReviewResponse.getResults().getData().get(y).getUser_id(),
                                        movieDetailsReviewResponse.getResults().getData().get(y).getId(),
                                        movieDetailsReviewResponse.getResults().getData().get(y).getCreated_at()));
                            }
                        }
                       movieReviewAdapter.notifyDataSetChanged();

                        //recyclerViewAdapter.notifyDataSetChanged();
                    }


                    else
                    {
                        if (Test.size()<=0) {
                            if (isAdded()) {
                                recyclerView.setVisibility(View.GONE);
                                imageNoReview.setVisibility(View.VISIBLE);
                                textNoReview.setVisibility(View.VISIBLE);
                                textNoReview.setText(getString(R.string.noreview_found));
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<MovieDetailsReviewResponse> call, Throwable t) {
                    hideProgress();

                    if (Test.size()<=0) {
                        if (isAdded()) {
                            recyclerView.setVisibility(View.GONE);
                            imageNoReview.setVisibility(View.VISIBLE);
                            textNoReview.setVisibility(View.VISIBLE);
                            textNoReview.setText(getString(R.string.noreview_found));
                        }
                    }
                }
            });
        } else {
            //Toast.makeText(getActivity(), "Please check your Network", Toast.LENGTH_SHORT).show();
            if (isAdded())
            {
                recyclerView.setVisibility(View.GONE);
                imageNoReview.setVisibility(View.VISIBLE);
                textNoReview.setVisibility(View.VISIBLE);
                textNoReview.setText(getString(R.string.no_network));
            }
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            getMovieReview();


        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            getMovieReview();

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

}
