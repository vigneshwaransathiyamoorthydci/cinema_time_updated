package com.dci.cinematime.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.AudienceListAdpater;
import com.dci.cinematime.Adapter.MovieReviewAdapter;
import com.dci.cinematime.Adapter.MovieTheaterAdapter;
import com.dci.cinematime.Adapter.MovieTimeListAdpater;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.AudienceTypeFilterActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.AudienceType;
import com.dci.cinematime.model.AudienceTypeResultItem;
import com.dci.cinematime.model.MovieDetail;
import com.dci.cinematime.model.MovieDetailReviewItem;
import com.dci.cinematime.model.MovieDetailsTheatresList;
import com.dci.cinematime.model.ShowTimeMovieTiminginTheater;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;
import com.github.jhonnyx2012.horizontalpicker.DatePickerListener;
import com.github.jhonnyx2012.horizontalpicker.HorizontalPicker;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.inject.Inject;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 7/25/2018.
 */

public class ShowtimeFragment extends BaseFragment  implements DatePickerDialog.OnDateSetListener {
    List<MovieDetail> movieDetailList;
    RecyclerView theaterrecycle;
    MovieTheaterAdapter movieTheaterAdapter;
    HorizontalPicker picker;
    LinearLayout dateselect;
    LinearLayout timeselect;
    HorizontalCalendar horizontalCalendar;
    RelativeLayout calendar_view;
    String flag = "0";
    private static int selectedDate = 1;
    private Calendar currentD;
    private Calendar calendardate;
    private SimpleDateFormat simpleDateFormat, simpleDateFormat2;
    private Calendar calendarTime;
    private Calendar currentT;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public ArrayList<String> showtimeList;
    public ArrayList<Integer> audienceTypeList;
    Spinner showtime_spinner;
    Spinner audiencetype_spinner;
    MovieTimeListAdpater timeAdapter;
    AudienceListAdpater audienceListAdpater;
    String formattedDate;
    List<MovieDetailsTheatresList> movieDetailTheatresList;
    public  ArrayList<ShowTimeMovieTiminginTheater> showTimeMovieTiminginTheater;
    ArrayList<String> movietimeinTheatres;
    boolean apiservice=false;
    TextView selecteddate;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    HashMap Showtimingmap = new HashMap();
    AudienceType audienceTypeResponse;
    ArrayList<AudienceTypeResultItem> audienceTypeResultItems;
    Field popup;
    ImageView imageNothearte;
    TextView textNotheatre;
    String showtime;
    int audiencetype;
    boolean getTheratrelist=false;
    HomeActivity homeActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //    changeTabsFont();
        View view = inflater.inflate(R.layout.fragment_showtime, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        imageNothearte=(ImageView)view.findViewById(R.id.image_no_theatre);
        textNotheatre=(TextView)view.findViewById(R.id.text_no_thearte);
        picker = (HorizontalPicker) view.findViewById(R.id.datePicker);
        //homeActivity=(HomeActivity)getActivity();
        theaterrecycle = view.findViewById(R.id.recycle);
        dateselect = view.findViewById(R.id.date_select);
        timeselect = (LinearLayout) view.findViewById(R.id.time_select);
        calendar_view = view.findViewById(R.id.calendar_view);
        showtime_spinner=(Spinner)view.findViewById(R.id.spinner_showtime);
        audiencetype_spinner=(Spinner)view.findViewById(R.id.spinner_audiencetype);
        selecteddate=(TextView)view.findViewById(R.id.selected_date);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        formattedDate = df.format(c);
        movieDetailTheatresList=new ArrayList<MovieDetailsTheatresList>();
        movietimeinTheatres=new ArrayList<String>();
        audienceTypeList=new ArrayList<>();
        showtimeList=new ArrayList<String>();
        showTimeMovieTiminginTheater=new ArrayList<ShowTimeMovieTiminginTheater>();
        dateselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog();
            }
        });
        timeselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog();
            }
        });
        timeAdapter = new MovieTimeListAdpater(showtimeList,getContext());

        audienceTypeResultItems=new ArrayList<AudienceTypeResultItem>();
        showtime_spinner.setAdapter(timeAdapter);
        //showtime_spinner.setPrompt("Select Showtime");
        audienceListAdpater=new AudienceListAdpater(audienceTypeResultItems,getContext());
        audiencetype_spinner.setAdapter(audienceListAdpater);

        movieDetailList = new ArrayList<>();

        movieTheaterAdapter = new MovieTheaterAdapter(getActivity(), movieDetailTheatresList);
        theaterrecycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        theaterrecycle.setItemAnimator(new DefaultItemAnimator());
        theaterrecycle.setAdapter(movieTheaterAdapter);

        try {
            popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);
            ListPopupWindow popupWindow = (ListPopupWindow) popup.get(showtime_spinner);
            popupWindow.setHeight(300);
            popupWindow.setWidth(100);
            ListPopupWindow popupWindow1 = (ListPopupWindow) popup.get(audiencetype_spinner);
            popupWindow1.setHeight(200);
            popupWindow1.setWidth(300);

        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
        showtime_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                showtime=showtimeList.get(position);
                if (!getTheratrelist) {

                        getMovieThreatelist(formattedDate, showtime, audiencetype);

                }
                //getAudienceType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        audiencetype_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                audiencetype=audienceTypeResultItems.get(position).getId();
                if (!getTheratrelist) {

                        getMovieThreatelist(formattedDate, showtime, audienceTypeResultItems.get(position).getId());

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        homeActivity.image_filter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), HomeFilterPopupActivity.class));
//                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//            }
//        });


        return view;
    }

    private void datePickerDialog() {
        if (selectedDate == 1) currentD = Calendar.getInstance();
        else currentD = calendardate;
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance((DatePickerDialog.OnDateSetListener) this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setAccentColor(getResources().getColor(R.color.red));
        datePickerDialog.setMinDate(currentD);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }




    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
        String donateDate=simpleDateFormat.format(calendardate.getTime());
        Date dob = null;
        try {
            dob = simpleDateFormat.parse(donateDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        formattedDate= simpleDateFormat.format(dob);
        selecteddate.setTextSize(13);

        selecteddate.setText(parseDateToddMMyyyy(formattedDate));
        if (!getTheratrelist) {

                getMovieThreatelist(formattedDate, showtime, audiencetype);

        }



    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "MMM-dd-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void getMovieThreatelist(String date,String time,int audiencetype) {
        if (Util.isNetworkAvailable()) {
            movieDetailTheatresList.clear();
            showTimeMovieTiminginTheater.clear();
            showProgress();
            getTheratrelist=true;
            if (isAdded())
            {
                theaterrecycle.setVisibility(View.VISIBLE);
                imageNothearte.setVisibility(View.GONE);
                textNotheatre.setVisibility(View.GONE);
            }
             cinemaTimeAPI.getMovieThreate(MovieDetailActivity.movieId,date,time,audiencetype).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    getTheratrelist=false;
                    hideProgress();
                    if (response.body()!=null) {
                        JSONObject object = null;
                        try {
                            object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("Status").equals("Success")) {
                                JSONObject ResultsjsonArray = object.getJSONObject("Results");
                                JSONArray theatresjsonArray = ResultsjsonArray.getJSONArray("theatres");
                                for (int i = 0; i < theatresjsonArray.length(); i++) {
                                    JSONObject theatresjsonArrayObject = theatresjsonArray.getJSONObject(i);
                                    JSONArray showtimesjsonArray = theatresjsonArrayObject.getJSONArray("showtimes");
                                    showTimeMovieTiminginTheater = new ArrayList<ShowTimeMovieTiminginTheater>();
                                    for (int k = 0; k < showtimesjsonArray.length(); k++) {
                                        JSONObject showtimesjsonjsonArrayObject = showtimesjsonArray.getJSONObject(k);
                                        //movietimeinTheatres.add(showtimesjsonjsonArrayObject.getString("time"));
                                        if (audienceTypeResultItems.get(audiencetype_spinner.getSelectedItemPosition()).getId() == 0) {
                                            showTimeMovieTiminginTheater.add(new ShowTimeMovieTiminginTheater(showtimesjsonjsonArrayObject.getString("time"),
                                                    showtimesjsonjsonArrayObject.getString("type"), theatresjsonArrayObject.getInt("id"), theatresjsonArrayObject.getString("name")));
                                        } else if (audienceTypeResultItems.get(audiencetype_spinner.getSelectedItemPosition()).getId() == 1) {
                                            if (showtimesjsonjsonArrayObject.getString("type").equals("Male")) {
                                                showTimeMovieTiminginTheater.add(new ShowTimeMovieTiminginTheater(showtimesjsonjsonArrayObject.getString("time"),
                                                        showtimesjsonjsonArrayObject.getString("type"), theatresjsonArrayObject.getInt("id"), theatresjsonArrayObject.getString("name")));
                                            }

                                        } else if (audienceTypeResultItems.get(audiencetype_spinner.getSelectedItemPosition()).getId() == 2) {
                                            if (showtimesjsonjsonArrayObject.getString("type").equals("Family")) {
                                                showTimeMovieTiminginTheater.add(new ShowTimeMovieTiminginTheater(showtimesjsonjsonArrayObject.getString("time"),
                                                        showtimesjsonjsonArrayObject.getString("type")
                                                        , theatresjsonArrayObject.getInt("id"), theatresjsonArrayObject.getString("name")));
                                            }
                                        }


                                    }
                                    Showtimingmap.put(i, showTimeMovieTiminginTheater);
                                    movieDetailTheatresList.add(new MovieDetailsTheatresList(theatresjsonArrayObject.getString("name"),
                                            theatresjsonArrayObject.getString("Latitude"), theatresjsonArrayObject.getString("Longitude"),
                                            theatresjsonArrayObject.getString("Email"), theatresjsonArrayObject.getString("NoofScreens")
                                            , Showtimingmap, theatresjsonArrayObject.getString("city"),
                                            theatresjsonArrayObject.getInt("id"),theatresjsonArrayObject.getString("profileImg")));

                                }


                                movieTheaterAdapter.notifyDataSetChanged();
                                //getAudienceType();
                            }
                            else
                            {
                                if (isAdded()) {
                                    theaterrecycle.setVisibility(View.GONE);
                                    imageNothearte.setVisibility(View.VISIBLE);
                                    textNotheatre.setVisibility(View.VISIBLE);
                                    textNotheatre.setText(object.getString("Message"));
                                }
                            }
                            } catch(JSONException e){
                                hideProgress();
                                if (isAdded()) {
                                    theaterrecycle.setVisibility(View.GONE);
                                    imageNothearte.setVisibility(View.VISIBLE);
                                    textNotheatre.setVisibility(View.VISIBLE);
                                    textNotheatre.setText(getString(R.string.notheatre_found));
                                }
                                //Toast.makeText(getContext(), "No Theatre Found", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    getTheratrelist=false;
                    if (isAdded())
                    {
                        theaterrecycle.setVisibility(View.GONE);
                        imageNothearte.setVisibility(View.VISIBLE);
                        textNotheatre.setVisibility(View.VISIBLE);
                        textNotheatre.setText(getString(R.string.notheatre_found));
                    }
                   // Toast.makeText(getContext(), "No Theatre Found", Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            //Toast.makeText(getContext(), "Please check your Network", Toast.LENGTH_SHORT).show();
            if (isAdded())
            {
                theaterrecycle.setVisibility(View.GONE);
                imageNothearte.setVisibility(View.VISIBLE);
                textNotheatre.setVisibility(View.VISIBLE);
                textNotheatre.setText(getString(R.string.no_network));
            }
        }
    }

    private void getAudienceType()
    {
        if (Util.isNetworkAvailable())
        {
            audienceTypeList.clear();
            audienceTypeResultItems.clear();
            audienceTypeResultItems.add(new AudienceTypeResultItem(0,getString(R.string.all_audience),1));
            cinemaTimeAPI.getAudiencetype().enqueue(new Callback<AudienceType>() {
                @Override
                public void onResponse(Call<AudienceType> call, Response<AudienceType> response) {

                    audienceTypeResponse=response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        for (int u=0;u<audienceTypeResponse.getResults().size();u++)
                        {
                            audienceTypeResultItems.add(new AudienceTypeResultItem(audienceTypeResponse.getResults().get(u).getId(),
                                    audienceTypeResponse.getResults().get(u).getAudience(),
                                    audienceTypeResponse.getResults().get(u).getStatus()));
                            //audienceTypeList.add(audienceTypeResponse.getResults().get(u).getId());

                        }
                        audienceListAdpater.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(getContext(), audienceTypeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<AudienceType> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), "On FAilure", Toast.LENGTH_SHORT).show();

                }
            });
        }
        else
        {
            Toast.makeText(getContext(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMovieShowtime()
    {
        if (Util.isNetworkAvailable()) {

             showProgress();
                showtimeList.clear();
            cinemaTimeAPI.getMovieShowtime(MovieDetailActivity.movieId).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                hideProgress();
                if (response.body()!=null)
                {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(String.valueOf(response.body()));
                        if (object.getString("Status").equals("Success")) {
                            JSONArray jsonArray = object.getJSONArray("Results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject showtimeObject = jsonArray.getJSONObject(i);
                                showtimeList.add(showtimeObject.getString("showtimes"));


                            }
                            timeAdapter.notifyDataSetChanged();
                            getAudienceType();
                        }
                        else
                        {
                            Toast.makeText(getContext(),object.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                        //getMovieThreatelist(formattedDate,showtimeList.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideProgress();
                        Toast.makeText(getContext(),"No List found",Toast.LENGTH_SHORT).show();
                    }

                }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),"No List found",Toast.LENGTH_SHORT).show();

                }
            });

        }
        else
        {
            Toast.makeText(getContext(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {

            //getMovieThreatelist(formattedDate,currentTime);

                getMovieShowtime();



        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;

                getMovieShowtime();



        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
}
