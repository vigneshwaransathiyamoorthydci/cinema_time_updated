package com.dci.cinematime.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.BaseActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.LoginActivity;
import com.dci.cinematime.activity.PurchaseActivity;
import com.dci.cinematime.activity.SettingsscreenActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.utils.AppPreference;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.LanguageHelper;
import com.dci.cinematime.utils.Util;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    ConstraintLayout constraint_inside_three;


    public SettingFragment() {
        // Required empty public constructor
    }
    TextView text_label_logout_setting_fragment;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    BaseActivity baseActivity;
    HomeActivity homeActivity;
    TextView text_label_email_name;
    TextView text_label_email_id;
    View view_setting_fragment;
    FrameLayout purchaseLinearlayout;
    FrameLayout editProfileLinearlayout;
    FrameLayout linearappsetting;
    FrameLayout linearappnotification;
    FrameLayout linearappabout;
    FrameLayout linearapprateourapp;
    FrameLayout linearappprivacypolicy;
    FrameLayout linearapphelpandfeedback;
    FrameLayout linearapplogout;
    View editprofileview;
    View logoutrview;
    View purchaseview;
    Bitmap bundleImageBitmap;
    CircularImageView profileImage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        homeActivity=(HomeActivity)getActivity();
        profileImage = (CircularImageView) view.findViewById(R.id.image_setting);
        text_label_email_name=(TextView)view.findViewById(R.id.text_label_email_name);
        text_label_email_id=(TextView)view.findViewById(R.id.text_label_email_id);
        purchaseLinearlayout=(FrameLayout)view.findViewById(R.id.lineareditpurchase);
        editProfileLinearlayout=(FrameLayout)view.findViewById(R.id.lineareditprofile);
        linearappsetting=(FrameLayout)view.findViewById(R.id.linearappsetting);
        linearappnotification=(FrameLayout)view.findViewById(R.id.linearappnotification);
        linearappabout=(FrameLayout)view.findViewById(R.id.linearappabout);
        linearapprateourapp=(FrameLayout)view.findViewById(R.id.linearapprateourapp);
        linearappprivacypolicy=(FrameLayout)view.findViewById(R.id.linearappprivacypolicy);
        linearapphelpandfeedback=(FrameLayout)view.findViewById(R.id.linearapphelpandfeedback);
        linearapplogout=(FrameLayout)view.findViewById(R.id.linearapplogout);
        editprofileview=(View)view.findViewById(R.id.editprofileview);
        logoutrview=(View)view.findViewById(R.id.logoutrview);
        purchaseview=(View)view.findViewById(R.id.editpurchaseview);
        homeActivity.hidetoolbar();
        purchaseLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_logout = new Intent(getActivity(), PurchaseActivity.class);
                startActivity(intent_logout);

            }
        });
        linearapplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                dialog.dismiss();

                            }
                        })
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing

                                sharedPreferences.edit().clear().commit();
                                AppPreference.setLoggedIn(getContext(), false);
                                AppPreference.setSkip(getContext(), false);
                                Intent intent_logout = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent_logout);
                                getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                                getActivity().finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();






            }
        });
          editProfileLinearlayout.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  Intent intent = new Intent(getActivity(), SettingsscreenActivity.class);
                  intent.putExtra("name", "userprofile");
                  getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                  startActivity(intent);
              }
          });

        linearappsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingsscreenActivity.class);
                intent.putExtra("name", "language");
                startActivity(intent);
            }
        });
       linearappnotification.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(getActivity(), SettingsscreenActivity.class);
               intent.putExtra("name", "notification");
               startActivity(intent);
           }
       });

        linearapprateourapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Under Development",Toast.LENGTH_SHORT).show();
            }
        });
       linearappprivacypolicy.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(getActivity(), SettingsscreenActivity.class);
               intent.putExtra("name", "policy");
               startActivity(intent);
           }
       });
        linearappabout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingsscreenActivity.class);
                intent.putExtra("name", "about");
                startActivity(intent);
            }
        });
       linearapphelpandfeedback.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(getActivity(), SettingsscreenActivity.class);
               intent.putExtra("name", "help");
               startActivity(intent);
           }
       });

        if (sharedPreferences.getInt(Constants.Userid,0)<=0) {
            editProfileLinearlayout.setVisibility(View.GONE);
            editprofileview.setVisibility(View.GONE);
            linearapplogout.setVisibility(View.GONE);
            logoutrview.setVisibility(View.GONE);
            purchaseLinearlayout.setVisibility(View.GONE);
            purchaseview.setVisibility(View.GONE);
        }
        else
        {
            if (sharedPreferences.getString(Constants.PROFILEPHOTO,null)!=null) {
               // bundleImageBitmap = Util.convert(sharedPreferences.getString(Constants.PROFILEPHOTO, null));
//                byte[] decodedString = Base64.decode(sharedPreferences.getString(Constants.PROFILEPHOTO, null), Base64.DEFAULT);
//                bundleImageBitmap = getBitmapFromString(sharedPreferences.getString(Constants.PROFILEPHOTO, null));
//                if (bundleImageBitmap != null) {
//                    profileImage.setImageBitmap(bundleImageBitmap);
//                }

            Picasso.with(getContext()).
                    load(getContext().getString(R.string.coming_soon_image_base_url)+Util.USERPROFILEIMAGE
                            + sharedPreferences.getString(Constants.PROFILEPHOTO,null)).placeholder(R.mipmap.placeholder_icon).
                    into(profileImage);
            }
            text_label_email_name.setText(sharedPreferences.getString(Constants.USERNAME,null));
            text_label_email_id.setText(sharedPreferences.getString(Constants.EMAIL,null));
            editProfileLinearlayout.setVisibility(View.VISIBLE);
            editprofileview.setVisibility(View.VISIBLE);
            linearapplogout.setVisibility(View.VISIBLE);
            logoutrview.setVisibility(View.VISIBLE);
        }

        return view;

    }
    private Bitmap getBitmapFromString(String jsonString) {
        /*
         * This Function converts the String back to Bitmap
         * */
        byte[] decodedString = Base64.decode(jsonString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
    public static String decompress(byte[] compressed) throws IOException {
        final int BUFFER_SIZE = 32;
        ByteArrayInputStream is = new ByteArrayInputStream(compressed);
        GZIPInputStream gis = new GZIPInputStream(is, BUFFER_SIZE);
        StringBuilder string = new StringBuilder();
        byte[] data = new byte[BUFFER_SIZE];
        int bytesRead;
        while ((bytesRead = gis.read(data)) != -1) {
            string.append(new String(data, 0, bytesRead));
        }
        gis.close();
        is.close();
        return string.toString();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (sharedPreferences.getInt(Constants.Userid,0)>0) {
            if (sharedPreferences.getString(Constants.PROFILEPHOTO,null)!=null) {
                // bundleImageBitmap = Util.convert(sharedPreferences.getString(Constants.PROFILEPHOTO, null));

                Picasso.with(getContext()).
                        load(getContext().getString(R.string.coming_soon_image_base_url)+Util.USERPROFILEIMAGE
                                + sharedPreferences.getString(Constants.PROFILEPHOTO,null)).placeholder(R.mipmap.placeholder_icon).
                        into(profileImage);

            }
        }
    }
}
