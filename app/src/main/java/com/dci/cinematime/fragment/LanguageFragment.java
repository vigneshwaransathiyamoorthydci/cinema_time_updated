package com.dci.cinematime.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.dci.cinematime.R;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.LanguageHelper;

import javax.inject.Inject;

import butterknife.BindView;

public class LanguageFragment extends BaseFragment {



    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    ImageView imagebackarrow;
    RadioButton radioButtonEnglish;
    RadioButton radioButtonTamil;
    RadioGroup radioLanguage;
    Button saveButton;
    String language = "en";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_language, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        imagebackarrow=(ImageView) view.findViewById(R.id.userprofile_arrow);
        radioLanguage=(RadioGroup)view.findViewById(R.id.radio_language);
        radioButtonEnglish=(RadioButton)view.findViewById(R.id.radio_button_english);
        radioButtonTamil=(RadioButton)view.findViewById(R.id.radio_button_tamil);
        saveButton=(Button)view.findViewById(R.id.language_save);
        imagebackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        radioLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.radio_button_english:
                        editor.putInt(Constants.SELECTEDLANGUAGES, 0);
                        language = "en";

                        break;
                    case R.id.radio_button_tamil:
                        editor.putInt(Constants.SELECTEDLANGUAGES, 1);
                        language = "ar";
                        break;

                }
            }
        });
        setLangauge(sharedPreferences.getInt(Constants.SELECTEDLANGUAGES, 0));
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.commit();
                LanguageHelper.setLanguage(getContext(), language);
                Intent intent_homeacvity = new Intent(getContext(), HomeActivity.class);
                intent_homeacvity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_homeacvity);
            }
        });
        //text_label_log_in = view.findViewById(R.id.text_label_log_in);
        return view;
    }

    private void setLangauge(int selectedLanguage) {
        switch (selectedLanguage) {
            case 0:
                radioButtonEnglish.setChecked(true);
                break;
            case 1:

                radioButtonTamil.setChecked(true);
                break;
        }

    }
}
