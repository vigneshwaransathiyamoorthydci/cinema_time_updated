package com.dci.cinematime.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.TheatersAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.AudienceTypeFilterActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.TheatreDetailsActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.TheatersList;
import com.dci.cinematime.retrofit.ApiClient;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.GPSTracker;
import com.dci.cinematime.utils.ShoppingApplication;
import com.dci.cinematime.utils.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class TheatresFragment extends BaseFragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final char MY_LOCATION_REQUEST_CODE = 0;
    private GoogleMap mMap;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    GoogleApiClient mGoogleApiClient;
    RecyclerView recyclerView;
    TheatersAdapter theatersAdapter;
    List<TheatersList.ResultsItem> theaterList;
    List<TheatersList.ResultsItem> searchtheaterList;
    LocationRequest mLocationRequest;
    boolean isRefresh = false;
    private TheatresFragment mapFrag;
    LinearLayoutManager linearLayoutManager;
    ImageView theatresearchImage;
    public void Oncreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public TheatresFragment() {
        // Required empty public constructor
    }
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    EditText headingTextview;
    private FusedLocationProviderClient mFusedLocationClient;
    GPSTracker gps;
    double latitude = 0;
    double longitude = 0;
    boolean locationUpdateServiceCall=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_theatres, container, false);
        ((HomeActivity) getActivity()).hidetoolbar();//this calling method is show the tool
        CinemaTimeApplication.getContext().getComponent().inject(this);
        recyclerView = view.findViewById(R.id.recycle_theatre_fragment);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(linearLayoutManager);
        theatresearchImage=(ImageView)view.findViewById(R.id.image_theatre_fragment_filter_icon);
        headingTextview=(EditText)view.findViewById(R.id.text_label_theatre_fragment_heading_name);
        searchtheaterList=new ArrayList<TheatersList.ResultsItem>();
        //SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        //fragment.getMapAsync(this);
        gps = new GPSTracker(getContext(),getActivity());
        getCurrentLocation();


        theatresearchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (headingTextview.getHint().toString().equals("Theatres"))
                {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                    headingTextview.setFocusable(true);
                    headingTextview.setEnabled(true);
                    headingTextview.requestFocus();
                    headingTextview.setHint("");
                }
                else if (headingTextview.getText().toString().length()<=0)
                {
                    Toast.makeText(getActivity(), "Enter something to search", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(headingTextview.getWindowToken(), 0);
                    getSearchData(headingTextview.getText().toString(),true);

                }


            }
        });

        headingTextview.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (headingTextview.getText().toString().length()>0)
                    {
                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(headingTextview.getWindowToken(), 0);
                        getSearchData(headingTextview.getText().toString(),true);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Enter something to search", Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }

                return false;
            }
        });
        headingTextview.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length()==0)
                {

                    getSearchData("",false);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) {


            }
        });





        return view;

    }
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getContext().startActivity(intent);
            }
        });

        // On pressing the cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    private void getCurrentLocation() {


        // check if GPS enabled
        if (gps.canGetLocation()) {

            double latitudes = gps.getLatitude();
            double longitudes = gps.getLongitude();
            Log.d("VIKIs","LOCATION"+latitudes+" "+longitude);
            //showProgress();
            // Do What you want to do
           // getTheatreMovieList(latitude,longitude);
            showProgress();
        }
        else
        {
            getTheatreMovieList(latitude,longitude);
        }

    }
    public void onViewCreated(View view, Bundle savedInstanceState) {
        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);
        View locationButton = ((View) fragment.getView().findViewById(Integer.parseInt("1"))
                .getParent()).findViewById(Integer.parseInt("2"));
        locationButton.setVisibility(View.GONE);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        try {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);





            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("", "" + e);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("Your Location");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
//        mCurrLocationMarker = mMap.addMarker(markerOptions);
//
//        //move map camera
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));





                 latitude = mLastLocation.getLatitude();
                 longitude = mLastLocation.getLongitude();
                 //hideProgress();
                    hideProgress();
                if(!locationUpdateServiceCall) {
                    locationUpdateServiceCall=true;
                    getTheatreMovieList(latitude, longitude);
                }









    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
            } else {
                // Permission was denied. Display an error message.
            }
        }
    }

    private void getSearchData(String searchstring,boolean search)
    {
        if (search)
        {
        searchtheaterList.clear();
        if (theaterList!=null) {
            if (theaterList.size() != 0) {
                for (int i = 0; i < theaterList.size(); i++) {
                    if (theaterList.get(i).getTheatrename().toString().toLowerCase().contains(searchstring.toLowerCase())) {
                        searchtheaterList.add(theaterList.get(i));
                    }

                }

                theatersAdapter = new TheatersAdapter(searchtheaterList, getActivity(), latitude, longitude);
                recyclerView.setAdapter(theatersAdapter);
                searchsetMapdata();
                theatersAdapter.setOnClickListen(new TheatersAdapter.AddTouchListen() {

                    @Override
                    public void onTouchClick(int position) {
                        Log.d(ShoppingApplication.TAG, "onTouchClick: " + theaterList.get(position).getId());
                        Intent i = new Intent(getActivity(), TheatreDetailsActivity.class);
                        i.putExtra("theatreid", searchtheaterList.get(position).getId());
                        i.putExtra("theatrename", searchtheaterList.get(position).getTheatrename());
                        startActivity(i);
                        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                    }
                });
            }
        }
        else
        {
            Toast.makeText(getActivity(), "No Theater Found", Toast.LENGTH_SHORT).show();
        }

        }
        else
        {
            theatersAdapter = new TheatersAdapter(theaterList, getActivity(),latitude,longitude);
            recyclerView.setAdapter(theatersAdapter);
            setMapdata(true);
            theatersAdapter.setOnClickListen(new TheatersAdapter.AddTouchListen() {

                @Override
                public void onTouchClick(int position) {

                    Intent i = new Intent(getActivity(), TheatreDetailsActivity.class);
                    i.putExtra("theatreid", theaterList.get(position).getId());
                    i.putExtra("theatrename",theaterList.get(position).getTheatrename());
                    startActivity(i);
                    getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

                }
            });
        }



    }

    private void getTheatreMovieList(final Double latitude, final Double longitude) {
        if (Util.isNetworkAvailable()) {

            //  swip_lay.setRefreshing(true);
            showProgress();
            cinemaTimeAPI.getTheaterList().enqueue(new Callback<TheatersList>() {
                @Override
                public void onResponse(Call<TheatersList> call, Response<TheatersList> response) {
                    hideProgress();
                    theaterList = new ArrayList<>();
                    theaterList.clear();
                    Gson gson = new GsonBuilder().create();
                    String json = gson.toJson(response.body());
                    Log.e("list", "theater: " + json);
                    if (response.body() != null) {

                        TheatersList model = response.body();


                        if (model.getStatus().equals("Success")) {
                            theaterList.addAll(model.getResults());

                            if (theaterList.size() != 0) {


                                theatersAdapter = new TheatersAdapter(theaterList, getActivity(), latitude, longitude);
                                recyclerView.setAdapter(theatersAdapter);
                                setMapdata(true);
                                theatersAdapter.setOnClickListen(new TheatersAdapter.AddTouchListen() {

                                    @Override
                                    public void onTouchClick(int position) {

                                        AudienceTypeFilterActivity.Audiencetype = 0;
                                        Intent i = new Intent(getActivity(), TheatreDetailsActivity.class);
                                        i.putExtra("theatreid", theaterList.get(position).getId());
                                        i.putExtra("theatrename", theaterList.get(position).getTheatrename());
                                        startActivity(i);
                                        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                                    }
                                });
                                // swip_lay.setRefreshing(false);
                            } else {
                                Toast.makeText(getActivity(), model.getMessage(), Toast.LENGTH_SHORT).show();
                                //swip_lay.setRefreshing(false);
                            }
                        }
                        else {
                            Toast.makeText(getActivity(), model.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        hideProgress();
                      //  hiderefresh();
                    } else {
                        Toast.makeText(getActivity(), "Server error", Toast.LENGTH_SHORT).show();
                        hideProgress();
                      //  hiderefresh();
                        // swip_lay.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<TheatersList> call, Throwable t) {
                    hiderefresh();
                    hideProgress();
                    //  swip_lay.setRefreshing(false);
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            Toast.makeText(getActivity(), "Please check your Network", Toast.LENGTH_SHORT).show();
        }
    }

    private void searchsetMapdata()
    {
        if (searchtheaterList.size()>0)
        {
            mMap.clear();
        }
        for (int i = 0; i < searchtheaterList.size(); i++) {

            double latitude = Double.parseDouble(searchtheaterList.get(i).getLatitude());
            double longitude = Double.parseDouble(searchtheaterList.get(i).getLongitude());
            if (latitude!=0.0 && longitude!=0.0) {

                MarkerOptions marker = new MarkerOptions();
                marker = new MarkerOptions().position(
                        new LatLng(latitude, longitude)).title(searchtheaterList.get(i).getTheatrename());
                marker.snippet(searchtheaterList.get(i).getAddress());
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.mappin));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), (float) 7.0));
                mMap.addMarker(marker);
            }

        }
    }

    private void setMapdata(Boolean mapZoom)
    {

        for (int i = 0; i < theaterList.size(); i++) {

            double latitude = Double.parseDouble(theaterList.get(i).getLatitude());
            double longitude = Double.parseDouble(theaterList.get(i).getLongitude());
            if (latitude!=0.0 && longitude!=0.0) {
                MarkerOptions marker = new MarkerOptions();
                marker = new MarkerOptions().position(
                        new LatLng(latitude, longitude)).title(theaterList.get(i).getTheatrename());
                marker.snippet(theaterList.get(i).getAddress());
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.mappin));
                if(mapZoom)
                {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), (float) 2.2));
                }
                //mMap.animateCamera(CameraUpdateFactory.zoomTo(mMap.getCameraPosition().zoom - 0.5f));
                mMap.addMarker(marker);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }
}

