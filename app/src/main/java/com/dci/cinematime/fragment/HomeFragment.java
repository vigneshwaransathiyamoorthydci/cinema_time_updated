package com.dci.cinematime.fragment;


import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.cinematime.Adapter.Viewpager_adapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.AudienceTypeFilterActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    ViewPager viewPager1;
    TabLayout tabLayout;
    SmartTabLayout smartTabLayout;
    TabLayout tabsHome;
     LinearLayout lyTabs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //    changeTabsFont();
        View view = inflater.inflate(R.layout.fragment_fragment_home, container, false);
        viewPager1 = (ViewPager) view.findViewById(R.id.viewpager_home);
        setupViewpager(viewPager1);
        smartTabLayout=(SmartTabLayout) view.findViewById(R.id.tab_home);

          lyTabs = (LinearLayout) smartTabLayout.getChildAt(0);
        tabsHome=(TabLayout)view.findViewById(R.id.tabs_home);
       smartTabLayout.setViewPager(viewPager1);
        tabsHome.setupWithViewPager(viewPager1, false);

        viewPager1.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                HomeFilterPopupActivity.sortby=1;
                HomeFilterPopupActivity.selectedlanguage="";
                AudienceTypeFilterActivity.Audiencetype=0;

            }
            @Override
            public void onPageSelected(int position) {
                HomeFilterPopupActivity.sortby=1;
                HomeFilterPopupActivity.selectedlanguage="";
                AudienceTypeFilterActivity.Audiencetype=0;
                //changeTabsTitleTypeFace(lyTabs, position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                HomeFilterPopupActivity.sortby=1;
                HomeFilterPopupActivity.selectedlanguage="";
                AudienceTypeFilterActivity.Audiencetype=0;
            }
        });

       smartTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
           @Override
           public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               HomeFilterPopupActivity.sortby=1;
               HomeFilterPopupActivity.selectedlanguage="";
               AudienceTypeFilterActivity.Audiencetype=0;
           }

           @Override
           public void onPageSelected(int position) {
               HomeFilterPopupActivity.sortby=1;
               HomeFilterPopupActivity.selectedlanguage="";
               AudienceTypeFilterActivity.Audiencetype=0;

           }

           @Override
           public void onPageScrollStateChanged(int state) {
               HomeFilterPopupActivity.sortby=1;
               HomeFilterPopupActivity.selectedlanguage="";
               AudienceTypeFilterActivity.Audiencetype=0;

           }
       });
        smartTabLayout.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                LayoutInflater inflater = LayoutInflater.from(container.getContext());
                Resources res = container.getContext().getResources();
                View tab = inflater.inflate(R.layout.tab_text, container, false);
                TextView customText = (TextView) tab.findViewById(R.id.text_tab_heading);
                switch (position) {
                    case 0:

                        customText.setText(adapter.getPageTitle(position));
                        break;
                    case 1:

                        customText.setText(adapter.getPageTitle(position));
                        break;
                    case 2:

                        customText.setText(adapter.getPageTitle(position));
                        break;
                    default:
                        throw new IllegalStateException("Invalid position: " + position);
                }
                return tab;
            }
            });

        //tabsHome.setupWithViewPager(viewPager1, false);
        setupTabIcons();

       // smartTabLayout.setupWithViewPager(viewPager1);


        /*tabLayout = (tabLayout) view.findViewById(R.id.tab_home);
        tabLayout.setupWithViewPager(viewPager1);*/
       // tabLayout.setBackground(getContext().getDrawable(R.drawable.background_notifycolour));
        viewPager1.setOffscreenPageLimit(3);

        ((HomeActivity)getActivity()).showtoolbar();//this calling method is show the tool bar



        return view;
    }
    public void changeTabsTitleTypeFace(LinearLayout ly, int position) {
        for (int j = 0; j < ly.getChildCount(); j++) {
            TextView tvTabTitle = (TextView) ly.getChildAt(j);
            tvTabTitle.setTypeface(null, Typeface.NORMAL);
            if (j == position) tvTabTitle.setTypeface(null, Typeface.BOLD);
        }
    }
    void setupViewpager(ViewPager viewPager) {
        Viewpager_adapter adapter = new
                Viewpager_adapter(getChildFragmentManager());
        adapter.addFragment(new NowShowingFragment(), "Now Showing");
        adapter.addFragment(new ComingSoonFragment(), "Upcoming");
        adapter.addFragment(new ExclusiveFragment(), "Exclusive");

        viewPager.setAdapter(adapter);

    }


    private void setupTabIcons() {
        TextView textHome = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.tab_text, null);
        textHome.setText("Now Showing");
       // textHome.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart, 0, 0, 0);
        tabsHome.getTabAt(0).setCustomView(textHome);
        TextView textNews = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.tab_text, null);
        textNews.setText("UpComing");
       // textNews.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart, 0, 0, 0);
        tabsHome.getTabAt(1).setCustomView(textNews);
        TextView textEvents = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.tab_text, null);
        //textEvents.setText("UpComing");
       //textEvents.setCompoundDrawablesWithIntrinsicBounds(R.drawable.heart/, 0, 0, 0);
        tabsHome.getTabAt(2).setCustomView(textEvents);
        textEvents.setText("Exclusive");



    }


}


  /*  private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Font.getInstance().getTypeFace(), Typeface.NORMAL);
                }
            }
        }
    }

}*/
