package com.dci.cinematime.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.cinematime.R;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Util;

import javax.inject.Inject;

public class PrivacyPolicyFragment extends BaseFragment {



    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    ImageView imagebackarrow;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_privacypolicy, container, false);
        CinemaTimeApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        imagebackarrow=(ImageView) view.findViewById(R.id.userprofile_arrow);
        imagebackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        //text_label_log_in = view.findViewById(R.id.text_label_log_in);
        return view;
    }


}
