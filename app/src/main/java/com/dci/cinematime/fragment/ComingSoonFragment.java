package com.dci.cinematime.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.cinematime.Adapter.ComingSoonAdapter;
import com.dci.cinematime.R;
import com.dci.cinematime.activity.AudienceTypeFilterActivity;
import com.dci.cinematime.activity.BaseActivity;
import com.dci.cinematime.activity.HomeActivity;
import com.dci.cinematime.activity.HomeFilterPopupActivity;
import com.dci.cinematime.activity.MovieDetailActivity;
import com.dci.cinematime.app.CinemaTimeApplication;
import com.dci.cinematime.model.ComingSoonResponse;
import com.dci.cinematime.model.NotifyMe_cl;
import com.dci.cinematime.retrofit.CinemaTimeAPI;
import com.dci.cinematime.utils.Constants;
import com.dci.cinematime.utils.Util;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.cinematime.utils.ShoppingApplication.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComingSoonFragment extends BaseFragment {

    private List<ComingSoonResponse.Result> coming;
    private RecyclerView recyclerView;
    private ComingSoonAdapter cominsoonAdapter;
    SwipeRefreshLayout swip_lay;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    HomeActivity homeActivity;

    @Inject
    CinemaTimeAPI cinemaTimeAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    BaseActivity baseActivity;
    ImageView imageNoList;
    TextView textNoList;
    ComingSoonResponse comingSoonResponse;
    public ArrayList<ComingSoonResponse.Result> resultArrayList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coming_soon, container, false);
        homeActivity=(HomeActivity)getActivity();
        baseActivity = (BaseActivity) getActivity();
        CinemaTimeApplication.getContext().getComponent().inject(this);
        resultArrayList=new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_coming_soon);
        swip_lay = view.findViewById(R.id.swip_lay);

        imageNoList=(ImageView)view.findViewById(R.id.image_no_comments);
        textNoList=(TextView)view.findViewById(R.id.text_no_comm);
        coming = new ArrayList<>();
        editor = sharedPreferences.edit();
        swip_lay.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swip_lay.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideProgress();
                        coming.clear();
                        getComingsoonList(HomeFilterPopupActivity.sortby,HomeFilterPopupActivity.selectedlanguage);
                        swip_lay.setRefreshing(false);
                    }

                }, 000);
            }
        });



        return view;


    }


    private void getComingsoonList(int sortby,String language) {

        if (Util.isNetworkAvailable()) {
            imageNoList.setVisibility(View.GONE);
            //imageNoList.setImageResource();
            textNoList.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            resultArrayList.clear();
            showProgress();
            coming.clear();
            cinemaTimeAPI.getcomingSoonMovie(sharedPreferences.getInt(Constants.Userid, 0), String.valueOf(sortby), language).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    if (response.body() != null) {
                        JSONObject object = null;
                        try {
                            object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("Status").equals("Success")) {

                                JSONArray jsonArray = object.getJSONArray("Results");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject resultarrayobject = jsonArray.getJSONObject(i);
                                    ComingSoonResponse.Result result = new ComingSoonResponse.Result();
                                    result.setId(resultarrayobject.getInt("id"));
                                    result.setName(resultarrayobject.getString("name"));
                                    result.setMovie_language(resultarrayobject.getString("movie_language"));
                                    result.setMovie_original_title(resultarrayobject.getString("movie_original_title"));
                                    result.setMovie_backgroundimg(resultarrayobject.getString("movie_backgroundimg"));
                                    result.setMovie_posterimage(resultarrayobject.getString("movie_posterimage"));
                                    result.setNotify_status(resultarrayobject.getInt("notify_status"));

                                    JSONObject movie_certificate = resultarrayobject.optJSONObject("movie_certificate");
                                    if (movie_certificate != null) {

                                        result.setMovie_certi(movie_certificate.getString("movie_certificate"));
                                    }
                                    resultArrayList.add(result);

                                }
                                cominsoonAdapter = new ComingSoonAdapter(resultArrayList, getActivity(), baseActivity,homeActivity);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(mLayoutManager);
                                //recyclerView.setItemAnimator(new DefaultItemAnimator());
                                int resId = R.anim.layout_animation_fall_down;
                                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
                                recyclerView.setLayoutAnimation(animation);
                                recyclerView.setAdapter(cominsoonAdapter);
                                cominsoonAdapter.setOnClickListen(new ComingSoonAdapter.AddTouchListen() {

                                    @Override
                                    public void onTouchClick(int position) {

                                        Intent i = new Intent(getActivity(), MovieDetailActivity.class);
                                        i.putExtra("id", resultArrayList.get(position).getId());
                                        i.putExtra("releaseType", 2);
                                        startActivity(i);
                                    }
                                });


                            } else {
                                if (isAdded()) {
                                    imageNoList.setVisibility(View.VISIBLE);
                                    //imageNoList.setImageResource();
                                    textNoList.setVisibility(View.VISIBLE);
                                    textNoList.setText(object.getString("Message"));
                                    recyclerView.setVisibility(View.GONE);

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } else {
                        {
                            if (isAdded()) {
                                imageNoList.setVisibility(View.VISIBLE);
                                //imageNoList.setImageResource();
                                textNoList.setVisibility(View.VISIBLE);
                                textNoList.setText(R.string.No_movie);
                                recyclerView.setVisibility(View.GONE);

                            }
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    {
                        if (isAdded()) {
                            imageNoList.setVisibility(View.VISIBLE);
                            //imageNoList.setImageResource();
                            textNoList.setVisibility(View.VISIBLE);
                            textNoList.setText(R.string.No_movie);
                            recyclerView.setVisibility(View.GONE);

                        }

                    }

                }

            });
        }
        else
        {
            if (isAdded()) {
                imageNoList.setVisibility(View.VISIBLE);
                //imageNoList.setImageResource();
                textNoList.setVisibility(View.VISIBLE);
                textNoList.setText(R.string.no_network);
                recyclerView.setVisibility(View.GONE);

            }

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            homeActivity.filteroption=0;
            homeActivity.image_filter.setVisibility(View.VISIBLE);
            if (HomeFilterPopupActivity.nowshowingFilter)
            {
                HomeFilterPopupActivity.nowshowingFilter=false;
                getComingsoonList(HomeFilterPopupActivity.sortby, HomeFilterPopupActivity.selectedlanguage);
            }
            else  if (resultArrayList.size()<=0) {
                HomeFilterPopupActivity.nowshowingFilter=false;
                getComingsoonList(HomeFilterPopupActivity.sortby, HomeFilterPopupActivity.selectedlanguage);
            }


        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            homeActivity.filteroption=0;
            homeActivity.image_filter.setVisibility(View.VISIBLE);
            if (HomeFilterPopupActivity.nowshowingFilter)
            {
                HomeFilterPopupActivity.nowshowingFilter=false;
                getComingsoonList(HomeFilterPopupActivity.sortby, HomeFilterPopupActivity.selectedlanguage);
            }
            else if (resultArrayList.size()<=0) {
                HomeFilterPopupActivity.nowshowingFilter=false;
                getComingsoonList(HomeFilterPopupActivity.sortby, HomeFilterPopupActivity.selectedlanguage);
            }

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }


}













