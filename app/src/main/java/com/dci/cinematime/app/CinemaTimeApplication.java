package com.dci.cinematime.app;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;


import com.dci.cinematime.BuildConfig;
import com.dci.cinematime.dagger.AppModule;
import com.dci.cinematime.dagger.ApplicationComponent;
import com.dci.cinematime.dagger.DaggerApplicationComponent;
import com.dci.cinematime.retrofit.RetrofitModule;
import com.dci.cinematime.utils.AppPreference;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import javax.inject.Inject;

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;

public class CinemaTimeApplication extends MultiDexApplication {
    @Inject
    public SharedPreferences mPrefs;
    private static CinemaTimeApplication mInstance;
    int countMinites=120000;
    int countDown=1000;
    public static CinemaTimeApplication getContext() {
        return mInstance;
    }

    private ApplicationComponent mComponent;
    private InterstitialAd mInterstitialAd;
    CountDownTimer AdcountTimer;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(BuildConfig.CINEMA_TIME_BASE_URL))
                .build();
        mComponent.inject(this);
        MobileAds.initialize(getContext(),
                "ca-app-pub-3940256099942544~3347511713");
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                //mInterstitialAd.loadAd(new AdRequest.Builder().build());

                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                AdcountTimer.start();



            }

        });


        AdcountTimer=new CountDownTimer(countMinites, countDown) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                if (mInterstitialAd.isLoaded()) {
                    if (foregrounded()) {
                        if (AppPreference.isSkipIn(getContext()))
                        {
                            mInterstitialAd.show();
                        }
                        else
                        {
                            mInterstitialAd.loadAd(new AdRequest.Builder().build());
                            AdcountTimer.start();
                        }
                    }
                    else
                    {
                        mInterstitialAd.loadAd(new AdRequest.Builder().build());
                        AdcountTimer.start();

                    }
                }
            }
        }.start();


//        // UNIVERSAL IMAGE LOADER SETUP
//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
//                .cacheOnDisk(true).cacheInMemory(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .displayer(new FadeInBitmapDisplayer(300)).build();
//
//        ImageLoaderConfiguration imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(
//                getApplicationContext())
//                .defaultDisplayImageOptions(defaultOptions)
//                .memoryCache(new WeakMemoryCache())
//                .diskCacheSize(100 * 1024 * 1024).build();
//
//        ImageLoader.getInstance().init(imageLoaderConfiguration);


    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    public static CinemaTimeApplication from(@NonNull Context context) {
        return (CinemaTimeApplication) context.getApplicationContext();
    }

    public boolean foregrounded() {
        ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == IMPORTANCE_FOREGROUND || appProcessInfo.importance == IMPORTANCE_VISIBLE);
    }
}
